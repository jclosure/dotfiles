# Bootstrapped Linux, Unix, and MacOS setup from Emacs

## Recommended packages
1. autossh
2. sshfs

## MacOS setup
In order to use the distributed clipboard features with zsh and emacs,
it is necessary to setup the MacOS machine as central mediator.  Follow
the following setup for this:

https://gist.github.com/jclosure/19698429dda1105b8a93b0832c07ebc7

## Instructions
1. Install zsh
2. Install oh-my-zsh
- Add custom theme that indicates OS type in prompt
```
cp ./dotfiles/zsh/oh-my-zsh/themes/mytheme.zsh-theme ~/.oh-my-zsh/themes/
```
3. Install oh-my-tmux
4. Install emacs
5. Install the dotfiles
Clone this repo somewhere and then run it's setup.zsh
```bash
cd ~/projects
git clone git@gitlab.com:jclosure/dotfiles.git
./dotfiles/install.zsh
```
6. Add the following to ~/.zshrc
Add this right under the default theme specification of oh-my-zsh
```
# load theme with custom prompt
export ZSH="$HOME/.oh-my-zsh"
```
Add this to the bottom
```
# dotfiles stuff
TAG=👁  # or whatever tag you prefer
source ~/projects/dotfiles/zsh/.zshrc-includes
source ~/.zshrc-local
source ~/projects/dotfiles/zsh/.zshrc-iterm

## MUST BE LAST IN FILE!!!
## AUTOMATICALLY CREATE OR ATTACH TO TMUX IF SSH SESSION AND NOT ALREADY ATTACHED
export DEFAULT_IDEMUX_SESSION=development
idemux
```
7. Start emacs and allow install to proceed

## SSH Setup
The following will setup resilient ssh sessions and do reverse forwarding
to support tunnelled ssh clipboards/pastboards for Mac/Linux. See ssh
directory for config files.

### Considerations
Reverse forwarding
```
Host *
  ServerAliveInterval 120
  ServerAliveCountMax 30
  ConnectTimeout 30
  SendEnv TERM_PROGRAM
  LogLevel ERROR
  # ExitOnForwardFailure yes
  RemoteForward 8377 localhost:8377
  RemoteForward 2224 localhost:2224
  RemoteForward 2225 localhost:2225
  # This one is for sending random shit with netcat
  RemoteForward 2226 localhost:2226
```

HostKey check suppression can be done either inline
```
-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no
```
or in the `~/.ssh/config`
```
Host 10.*                            # use your own pattern here, eg. *.example.com, example.*.com
  StrictHostKeyChecking   no         # turn off the HostKey check
  LogLevel                ERROR      # keep warnings from printing to STDOUT
  UserKnownHostsFile      /dev/null  # (optional) add the host automatically to a black hole, otherwise it would be added to ~/.ssh/known_hosts and show you a warning/message at the top of your session. You may want it added to known_hosts if your shell uses `ssh` autocompletion, such as fish.
```

## Iterm2
Import the Development.json profile into iterm2 and set it as default

## Tips
If you are using a Linux system where the user db is not local in
/etc/passwd, such as centralized in ldap, you cannot change your shell
with chsh.  To utilize zsh as your default shell, you can add the
following to your .bashrc.
```sh
# this goes always at bottom... !!!!!
# Switch to the zsh shell. If something goes wrong, fall back to bash.
zsh_path=$(which zsh)
if [[ $? == 0 ]]; then
   $zsh_path && exit
fi
```

## Resources
Elisp Programming
(elisp cookbook)[https://www.emacswiki.org/emacs/ElispCookbook]

## TODO
- Make all shell scripts conformant to Google's Shell Style Guide
-// Break out clipboard fusion as a stand-alone emacs package
/q- Break out better movement and deleting words as a stand-alone package
