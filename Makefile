


install:
	./install.zsh
	./install.zsh

clean:
	unlink ~/.emacs.d
	rm -rf ./.emacs.d/el-get
	rm -rf ./.emacs.d/elpa
	rm -rf ./.emacs.d/var
