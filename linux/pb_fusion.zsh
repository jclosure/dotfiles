#  TEACH ZSH TO USE LOCAL CLIPBOARD
# Ref: https://coderwall.com/p/a035gq/zsh-copy-paste-system-wide-for-os-x-like-in-emacs
# TODO: make versions of these functions that tee and drive netcat to allow remote zsh sessions to also participate

# ADAPTED FOR LINUX WITH PASTEBOARD OF REVERSE FORWARDED TUNNELS BACK TO ORCHESTRATOR HOST

pb-kill-line () {
  emulate -L zsh
  zle kill-line
  echo -n "$CUTBUFFER" | nc localhost 2224
}

pb-kill-whole-line () {
  emulate -L zsh
  zle kill-whole-line
  echo -n "$CUTBUFFER" | nc localhost 2224
}

pb-backward-kill-word () {
  emulate -L zsh
  zle backward-kill-word
  echo -n "$CUTBUFFER" | nc localhost 2224
}

pb-kill-word () {
  emulate -L zsh
  zle kill-word
  echo -n "$CUTBUFFER" | nc localhost 2224
}

pb-kill-buffer () {
  emulate -L zsh
  zle kill-buffer
  killring=("$CUTBUFFER" "${(@)killring[1,-2]}")
  echo -n "$CUTBUFFER" | nc localhost 2224
}

pb-copy-region-as-kill-deactivate-mark () {
  emulate -L zsh
  zle copy-region-as-kill
  zle set-mark-command -n -1
  killring=("$CUTBUFFER" "${(@)killring[1,-2]}")
  echo "$CUTBUFFER" | nc localhost 2224
}

pb-kill-region-as-kill-deactivate-mark () {
  emulate -L zsh
  zle kill-region
  zle set-mark-command -n -1
  killring=("$CUTBUFFER" "${(@)killring[1,-2]}")
  echo "$CUTBUFFER" | nc localhost 2224
}

pb-yank () {
  emulate -L zsh
  killring=("$CUTBUFFER" "${(@)killring[1,-2]}")
  CUTBUFFER=$(nc localhost 2225 < $TTY)
  zle yank
}

zle -N pb-kill-line
zle -N pb-kill-whole-line
zle -N pb-backward-kill-word
zle -N pb-kill-word
zle -N pb-kill-buffer
zle -N pb-copy-region-as-kill-deactivate-mark
zle -N pb-kill-region-as-kill-deactivate-mark
zle -N pb-yank

bindkey '^K'   pb-kill-line
bindkey '^U'   pb-kill-whole-line
bindkey '\e^?' pb-backward-kill-word
bindkey '\e^H' pb-backward-kill-word
bindkey '^W'   pb-backward-kill-word
bindkey '\ed'  pb-kill-word
bindkey '\eD'  pb-kill-word
bindkey '^X^K' pb-kill-buffer
bindkey '\ew'  pb-copy-region-as-kill-deactivate-mark
bindkey '\eW'  pb-copy-region-as-kill-deactivate-mark
bindkey '^Y'   pb-yank



# fix zle cut/copy keys to be exactly like emacs
# copy
bindkey "\ew" pb-copy-region-as-kill-deactivate-mark
# cut
bindkey "\C-w" pb-kill-region-as-kill-deactivate-mark
# redo
bindkey "\e_" redo
# undo is already bound to \C--

# don't put alt-<backdelete> chars in killring, just delete them
bindkey "^[^?" backward-delete-word
