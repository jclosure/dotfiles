# Iterm development profile

## Install
Import Development.json into iterm2 profiles

NOTE: Development.itermkeymap.json contains a copy if you just want
to use the keys.  However the Development.json also sets the color
schema and user.tag variables, do that's recommened to get emoji
badges and nice colors.

## Notables
- user.tag badge that corresponds to $TAG from .zshrc
- keybindings to forward keys to emacs

## extra tools
Install Key Codes
```
brew info key-codes
```
Use Key Codes to get the Unicode key codes nec to forward

![Example](iterm_extended_key_bindings.png)

## Example
```elisp
;; NOTE: in iTerm requires:
(global-set-key  (kbd "C-.") 'tabbar-forward) ; [46;5u
(global-set-key  (kbd "C-,") 'tabbar-backward) ; [44;5u
```

## Codes
Control: `5u`
Shift: `2u`
