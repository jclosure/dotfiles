#!/bin/zsh

############################################################
# Composable functions predicates with any and all semantics
############################################################

beginswith() { case "$2" in "$1"*) true;; *) false;; esac; }
endswith() { case "$2" in *"$1") true;; *) false;; esac; }
contains() { case "$2" in *"$1"*) true;; *) false;; esac; }
checkresult() { if [ $? = 0 ]; then echo TRUE; else echo FALSE; fi; }

# examples:
# all "beginswith x" x xy xyz ; checkresult # TRUE
# all "beginswith x" x xy axy ; checkresult # FALSE
# all "endswith x" x ax abx ; checkresult # TRUE
# all "endswith x" x ax abc ; checkresult # FALSE
# all "contains x" x ax axc ; checkresult # TRUE
# all "contains x" x ax abc ; checkresult # FALSE
all() {
    local test="$1"; shift
    for i in "$@"; do
        eval "$test $i" || return
    done
}

# examples:
# any "beginswith x" a ab xbc ; checkresult # TRUE
# any "beginswith x" a ab abc ; checkresult # FALSE
# any "endswith x" a ab abx ; checkresult # TRUE
# any "endswith x" a ab abc ; checkresult # FALSE
# any "contains x" a ab axc ; checkresult # TRUE
# any "contains x" a ab abc ; checkresult # FALSE
any() {
    local test="$1"; shift
    for i in "$@"; do
        eval "$test $i" && return
    done
}

filter() {
    local test="$1"; shift
    local matched=()
    for i in "$@"; do
        eval "$test $i" && matched+=("$i")
    done
    echo -e "${matched[*]}"
}

########################################################3

#######################################
# Print path variable with 1 line per path part.
# Fallsback to $PATH.  Use also for $PYTHONPATH
# Arguments:
# path to print
#######################################
function print_path() {
    local _path=$1
    if [[ -z $_path ]]; then
      echo -e ${PATH//:/\\n}
    else
      echo -e ${_path//:/\\n}
    fi
}

function docker_bridge_ip() {
    docker network inspect bridge --format '{{range .IPAM.Config }}{{.Gateway}}{{end}}'
}

# HGREP
function hgrep {
    history | grep -i $1
}

# For working with protobufs to test well-formedness and spec match
function cproto() {
    protoc $1 --cpp_out=/tmp/ --proto_path=.
    IFS='.' read -r file_name proto <<< "$1"
}

function pproto() {
    protoc $1 --python_out=/tmp/ --proto_path=.
    IFS='.' read -r file_name proto <<< "$1"
}

function get_vmware_vm_ip() {
    # https://superuser.com/questions/373192/how-do-i-find-the-ip-address-of-a-virtual-machine-using-vmware-fusion
    local vm_name=$1
    /Applications/VMware\ Fusion.app/Contents/Library/vmrun getGuestIPAddress "$(HOME)/Documents/Virtual Machines.localized/$(vm_name).vmwarevm/$(vm_name).vmx"
}

# WORKSTATION
function EPHEMERAL_PORT() {
    netstat -aln | awk '
    $6 == "LISTEN" {
      if ($4 ~ "[.:][0-9]+$") {
        split($4, a, /[:.]/);
        port = a[length(a)];
        p[port] = 1
      }
    }
    END {
      for (i = 11000; i < 65000 && p[i]; i++){};
      if (i == 65000) {exit 1};
      print i
    }
  '
}

# TODO: consider using -M $(EPHEMERAL_PORT)
# example: alias w1ssh="autossh_connect myhost.fqdn.com"
function autossh_connect() {
    local remote_host=$1
    autossh -t -M 0 -o 'ServerAliveInterval=30' -o 'ServerAliveCountMax=10000' -o 'SendEnv=TERM_PROGRAM' \
            -o 'ExitOnForwardFailure=no' -o 'TCPKeepAlive=yes' \
            -L $(EPHEMERAL_PORT):localhost:$(EPHEMERAL_PORT) $remote_host 2> >(sed -e 's/setsockopt TCP_NODELAY: Invalid argument//' >&2)
    # NOTE: filtering stdout in order to supress this particular err with nc powered clipboard transfer solution
}

# TODO: consider using -M $(EPHEMERAL_PORT)
# example: alias w1sshfs="autosshfs_mount myhost.fqdn.com"
function autosshfs_mount() {
    local remote_host=$1
    local mount_dir="$HOME/mnt/$remote_host"
    mkdir -p "$mount_dir"
    sshfs $remote_host: "$mount_dir" \
          -o auto_cache,defer_permissions,negative_vncache,noappledouble,volname=$remote_host,reconnect,compression=yes,transform_symlinks,ServerAliveInterval=45,ServerAliveCountMax=10000,ssh_command='autossh -q -M 0'
    echo "Mounted home directory from remote host: $remote_host to: $mount_dir"
}

# echo to stderr for testing redirects: cmd 2> >(sed -e 's/foo/bar/' >&2)
# example: echoerr kungfoo 2> >(sed -e 's/oo/u/' >&2)
echoerr() { echo "$@" 1>&2; }

# kafka-topics
kt() {
    kafka-topics --create --topic $1 --partitions 1 --replication-factor 1 --if-not-exists --zookeeper $(docker_bridge_ip):2181
}

generate_password() {
    echo $(pwgen -Bsy $1 1)
}

#######################################
# Get a list of listening ports and the assoc pids
# Requirements:
# lsof installed and avail on the $PATH
#######################################
function ports() {
    sudo lsof -Pn -i4
}

#######################################
# Kill a process listening on a particular port by port number
# Requirements:
# lsof installed and avail on the $PATH
#######################################
portslay () {
    kill -9 `lsof -i tcp:$1 | tail -1 | awk '{ print $2;}'`
}

# to edit binary apple plist files:
function pledit {
    plutil -convert xml1 ${1}
    emacs ${1}
    plutil -convert binary1 ${1}
}

#######################################
# Serve the current directory as a static webserver
# Requirements:
# python2 or python3
#######################################
function serve_dir {
    local port=$1
    open http://localhost:$port 2>/dev/null
    python_version=$(python -V)
    if [[ "$python_version" =~ "^Python 3.*" ]]; then
      echo "starting with python3"
      python -m http.server $port
    else
      echo "starting with python2"
      python -m SimpleHTTPServer $port
    fi
}

function clean_build_dirs {
    find . -type d -path '*/_build/*' -exec rm -rvf {} \;
}

function clean_deps_dirs {
    find . -type d -path '*/deps/*' -exec rm -rvf {} \;
}

# cli countdown
function countdown(){
    date1=$((`gdate +%s` + $1));
    while [ "$date1" -ge `gdate +%s` ]; do
        echo -ne "$(gdate -u --date @$(($date1 - `gdate +%s`)) +%H:%M:%S)\r";
        sleep 0.1
    done
}

# cli stopwatch
function stopwatch(){
    date1=`gdate +%s`;
    while true; do
        echo -ne "$(gdate -u --date @$((`gdate +%s` - $date1)) +%H:%M:%S)\r";
        sleep 0.1
    done
}

# alias to lookup the correct submodule ver for a given branch of parent
function git_submodule_branch_version {
    local PATH_TO_SUBMODULE=$1
    BRANCH=$(git branch | grep \* | cut -d ' ' -f2-)
    git ls-tree $BRANCH $PATH_TO_SUBMODULE
}

# alias to completely remove a submodule
function git_remove_submodule {
    [[ -z "$1" ]] && { echo "You must provide the path to from the project root to the submodule!!!" ; exit 1; }
    local SUBMODULE_PATH=$(realpath -L --relative-base . $1)
    echo "removing submodule at: "$SUBMODULE_PATH
    git submodule deinit -f -- $SUBMODULE_PATH
    rm -rf .git/modules/$SUBMODULE_PATH
    git rm -f $SUBMODULE_PATH
}

# reset from when ssh session times out and leaves terminal in a wack state
# TODO: determine if plain old `reset' works for this
function reset_terminal() {
    printf "\e[?2004l" # clear altscreen
    printf "\e[?1000l" # disable mouse reporting
    printf "\e[?2004l" # temporarily turning bracketed paste
}

## harvest certs from all environments
function harvest_all_kube_certs {
    local DOWNLOAD_DIR=/tmp/certs
    mkdir -p $DOWNLOAD_DIR
    local PRIOR_CONTEXT=$(kubectl config current-context)
    for CONTEXT in `kubectl config get-contexts | awk 'NR>1 {print $2}'`; do
        kubectl config use-context $CONTEXT
        kubectl get secrets grpc-client -o yaml | grep .pem | while read key value; do echo $value | base64 -D > $DOWNLOAD_DIR/$CONTEXT-client-${key%":"}; done
        kubectl get secrets grpc-server -o yaml | grep .pem | while read key value; do echo $value | base64 -D > $DOWNLOAD_DIR/$CONTEXT-server-${key%":"}; done
    done
    kubectl config use-context $PRIOR_CONTEXT
    echo "Your certs can be found in: $DOWNLOAD_DIR"
}

function deserialize_proto_bin() {
    # USAGE EXAMPLE:
    # deserialize_proto_bin ./my_type.pb  ./protobufs/my_type.proto my_namespace.MyTypeProto | less
    local PB_FILE=$1
    local PROTOBUF_SCHEMA_FILE=$2
    local PROTOBUF_TYPE=$3
    local PROTOBUFS_SCHEMA_DIR="`realpath $(dirname $PROTOBUF_SCHEMA_FILE)`"
    protoc -I . -I $PROTOBUFS_SCHEMA_DIR --decode $PROTOBUF_TYPE $PROTOBUFS_SCHEMA_FILE < $PB_FILE
}


# TODO: Follow https://google.github.io/styleguide/shellguide.html

#######################################
# Pronounced: "idem-mux"
# Provides an idempotent emacs session by name, by either starting or opening an existing session of tmux
# Globals:
#   DEFAULT_TMUX_SESSION
#   TAG
# Arguments:
#   Name of the tmux session to open or create
#######################################
function idemux {
    local default_session_name=${DEFAULT_IDEMUX_SESSION:-default}
    local session_name=${1:-$default_session_name}
    local optional_command=$2

    [[ -z "$session_name" ]] && {
        echo "You must provide the name of the tmux session on \$1 or export \$DEFAULT_IDEMUX_SESSION!!!" ; return 1;
    }

    if [[ -e "`which tmux`" &&  "$PS1" != "" &&  "$TMUX" == "" && "${SSH_TTY:-x}" != x ]]; then
      # sleep 1
      if [[  $(tmux ls -F "#{session_name}: #{?session_attached,attached,not attached}") != "$SESSION_NAME: attached" ]]; then
        iterm2_set_user_var tag $TAG
        ( (tmux has-session -t $session_name 2>/dev/null && tmux attach-session -t $session_name) || (tmux new-session -s $session_name "export PATH=$PATH && export PYTHONPATH=$PYTHONPATH && '$optional_command'") ) # && exit 0
        if [[ "$?" != "0" ]]; then
          echo "tmux failed to start"
        fi
      fi
    fi
}
