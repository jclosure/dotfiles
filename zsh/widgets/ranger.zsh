
run_ranger () {
    echo
    ranger --choosedir=$(pwd) < $TTY
    zle reset-prompt
}

zle -N run_ranger
bindkey '^f' run_ranger
