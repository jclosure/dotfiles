
# TEACH ZSL TO USE LOCAL CLIPBOARD
# Ref: https://coderwall.com/p/a035gq/zsh-copy-paste-system-wide-for-os-x-like-in-emacs
# TODO: make versions of these functions that tee and drive netcat to allow remote zsh sessions to also participate

pb-kill-line () {
  zle kill-line
  echo -n $CUTBUFFER | pbcopy
}

pb-kill-whole-line () {
  zle kill-whole-line
  echo -n $CUTBUFFER | pbcopy
}

pb-backward-kill-word () {
  zle backward-kill-word
  echo -n $CUTBUFFER | pbcopy
}

pb-kill-word () {
  zle kill-word
  echo -n $CUTBUFFER | pbcopy
}

pb-kill-buffer () {
  zle kill-buffer
  echo -n $CUTBUFFER | pbcopy
}

pb-copy-region-as-kill-deactivate-mark () {
  zle copy-region-as-kill
  zle set-mark-command -n -1
  echo -n $CUTBUFFER | pbcopy
}

# TODO: test this one fully
pb-kill-region-as-kill-deactivate-mark () {
  zle kill-region
  zle set-mark-command -n -1
  echo -n $CUTBUFFER | pbcopy
}

pb-yank () {
  CUTBUFFER=$(pbpaste)
  zle yank
}

zle -N pb-kill-line
zle -N pb-kill-whole-line
zle -N pb-backward-kill-word
zle -N pb-kill-word
zle -N pb-kill-buffer
zle -N pb-copy-region-as-kill-deactivate-mark
zle -N pb-kill-region-as-kill-deactivate-mark
zle -N pb-yank

# TODO: tweak these and make them behave better
# bindkey '^K'   pb-kill-line
# bindkey '^U'   pb-kill-whole-line
# bindkey '\e^?' pb-backward-kill-word
# bindkey '\e^H' pb-backward-kill-word
# bindkey '^W'   pb-backward-kill-word
# bindkey '\ed'  pb-kill-word
# bindkey '\eD'  pb-kill-word
# bindkey '^X^K' pb-kill-buffer
# bindkey '\ew'  pb-copy-region-as-kill-deactivate-mark
# bindkey '\eW'  pb-copy-region-as-kill-deactivate-mark
# bindkey '^Y'   pb-yank

# bindkey by itself will give a list of everything bound

# find out which keys are bound and how to create widgets
# https://sgeb.io/posts/2014/04/zsh-zle-custom-widgets/

# fix zle cut/copy keys to be exactly like emacs
# https://gist.github.com/jclosure/d780d58e35c0b1beabfeae35bad65628

## SETUP ZSH TO MATCH MY EMACS PREFS

# copy
bindkey "\ew" pb-copy-region-as-kill-deactivate-mark
# cut
bindkey "\C-w" pb-kill-region-as-kill-deactivate-mark

#undo/redo
# undo is already bound to \C--
# redo
bindkey "\e_" redo

# don't put alt-<backdelete> chars in killring, just delete them
bindkey "^[^?" backward-delete-word
