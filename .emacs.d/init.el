;;; Commentary:
;;; --- Emacs portable single-file configuration: clean, fast-loading,
;;;     optimized for productivity in terminal
;;; -*- lexical-binding: t; -*-
;;
;; Copyright (c) 2016-2019 Joel Holder
;;
;; Author: Joel Holder <jclosure@gmail.com>
;; Keywords: convenience
;;
;; This file is not part of GNU Emacs.

;;; TODO:
;; - add comprehensive key chords section in documentation
;; - company-mode use TAB behavior only dwim
;; - cleanup and document hyrdras
;; - get forward and backward nav solution for cursor positions
					;  - pick between treemacs/neotree for long-term
;; - investigate centaur tabs - https://github.com/ema2159/centaur-tabs

;;; TODO: study and derive from:
;; https://emacs.nasy.moe/
;; https://lupan.pl/dotemacs/
;; https://ladicle.com/post/config/
;; https://sriramkswamy.github.io/dotemacs
;; https://github.com/angrybacon/dotemacs/blob/master/dotemacs.org
;; https://github.com/rougier/aerial-emacs/blob/master/.emacs#L118
;; https://github.com/bbatsov/emacs.d/blob/master/init.el
;; https://ivanmalison.github.io/dotfiles/#configurationofmyownpackages
;; https://michael.englehorn.com/config.html
;; https://github.com/seanfarley/dotfiles/tree/master/emacs

;;; INSTALLING EMACS FROM SCRATCH IN DEBIAN/UBUNTU

;; # install essential build tools
;; sudo apt-get install build-essential

;; # get all dependencies of a previous emacs version
;; sudo apt-get build-dep emacs24


;; # Download emacs dependencies:
;; wget https://ftp.gnu.org/gnu/emacs/emacs-27.1.tar.xz
;; tar xvf emacs-27.1.tar.xz
;; cd emacs-27.1

;; # install
;; ./configure \
;;  && make
;;  && make install

;; # regenerate icon cache
;; gtk-update-icon-cache -qtf /usr/share/icons/hicolor


;; RUNNING EMACS AS A DAEMON
;; https://www.emacswiki.org/emacs/EmacsAsDaemon


;;; ENV SETUP GUIDE

;; Colors set for dark background.  See FACE CUSTOMIZATION section.
;; the default colors here are optimized for: TERM=xterm-256color
;; please place 'export TERM=xterm-256color' in ~/.bashrc

;; SYSTEM SETUP RECOMMENDATIONS
;; Emacs aliases
;; alias ec-kill-emacs='emacsclient -e "(kill-emacs)"'
;; alias ec="emacsclient -t -a ''"

;; PREREQUISITES:

;; QUICK INSTALL DEBIAN/UBUNTU:
;; the following commands will do the extra program installs,
;; cut and paste the following, however each step should be read.
;; there are additional config items.

;; pip3 install --user virtualenv epc importmagic flake8 jedi rope pyflakes cpplint
;; emacs -nw
;; sudo apt-get install ack-grep
;; sudo apt-get install -y ripgrep
;; apt-get install -y silversearcher-ag
;; sudo apt-get install -y silversearcher-ag
;; sudo apt-get install tmux
;; sudo apt-get install xclip
;; sudo apt-get install xsel
;; sudo apt-get install jq
;; sudo apt-get install -y qrencode

;; 7. Install these:
;; pip3 install virtualenv epc importmagic flake8 jedi rope pyflakes cpplint
;; or at the user-level, however make sure the $PATH extension below is avail in gui mode
;; pip3 install --user virtualenv epc importmagic flake8 jedi rope pyflakes cpplint
;; make sure that this is in .zshrc:
;; export PATH="~/.local/bin:${PATH}"

;; Set the path to cpplint in your file_system below

;; 7.1. Install jedi in emacs

;; M-x jedi:install-server
;; M-x jedi:setup

;; 8. Install these

;; DEBIAN:
;; silversearcher: sudo apt-get install -y silversearcher-ag or https://github.com/ggreer/the_silver_searcher#installing
;; NOTE: silversearch must be ver > 0.31.0 for .elixir lang support.  best to always install latest ver. see src install.
;; ack: sudo apt-get install -y ack-grep
;; ripgrep: sudo apt-get install -y ripgrep or https://gist.githubusercontent.com/cyio/e101597bfbfff6369fa601c2b6456e81/raw/1b2f20784f040687c24a59958efb551394ffa0c5/install-ripgrep-on-ubuntu.sh

;; MACOS:
;; brew install ripgrep
;; brew install the_silver_searcher

;; 9. Add your ~/.agignore
;;/**/log/
;;/**/logs/
;;/**/images/
;;/**/javascripts/
;;/**/stylesheets/
;;/**/tmp/
;;/**/vendor/
;;/**/build/
;;/**/_build/
;;/**/deps/
;;/**/node_modules/
;;*.tags*
;;*.gemtags*
;;*.csv
;;*.tsv
;;*.tmp*
;;*.css
;;*.scss
;;*.old
;;*.html
;;*.plist
;;*.pdf
;;*.md
;;*.log
;;/apps/*/deps/*
;;/apps/*/*/deps/*
;;/apps/*/_build/*
;;/apps/*/*/_build/*
;;/*/node_modules/*

;; 10. Install the flake8 config
;; add this to ~/.config/flake8
;; [flake8]
;; ignore = E221,E501,E203,E202,E272,E251,E211,E222,E701
;; max-line-length = 160
;; exclude = tests/*
;; max-complexity = 10

;; 11. install tmux support binaries
;; on Linux, this requires xsel or xclip
;; on macOS, this requires installing reattach-to-user-namespace, see oh-my-tmux README.md

;; 12. install jq

;; 13. install qrencode
;; brew install qrencode
;; sudo apt-get install qrencode

;;; CONFIG  DOMUMENTATION

;; NOTE: for emacs in tty the terminal can block keys with C-*,
;; if not explicitly mapped. use the following to see what the keystrokes are
;; that need to be mapped via your terminal.
;; C-h l (or M-x view-lossage) to see if your key combinations make it into Emacs

;; ITERM ESCAPES CODES
;; https://emilyemorehouse.com/blog/001-iterm-keybindings/
;; NOTE: be sure to set ⌘ - keys shown ^

;; these are typically nec to map:
;; C-Up [1;5A
;; C-Down [1;5B
;; C-Left [1;5D
;; C-Right [1;5C
;; C-; [59;5u

;; cli-macos-like l/r nav
;; M-Right f
;; M-Left b

;; use "cat -v" or "vis -o" to see escape codes that keys pass to terminal
;; for MacOS, also there is the Key Codes app, which also provide hex values:
;; https://manytricks.com/keycodes/

;; EMACS KEY BINDINGS

;; open terminals: C-c t
;; scroll up in terminal: M-<up>
;; scroll down in terminal: M-<down>

;; helm prefix: C-x c
;; helm find-files: C-x c f
;; move up one directory in helm find files: C-l
;; helm find-files with grep: C-x c f C-s
;; helm find-files with grep recursive: Cx c f C-u C-s

;; jump to previous cursor position: C-u C-SPC
;; jump to next cursor position: C-x C-SPC

;; projectile prefix: C-c p
;; projectile switch projects: C-c p p
;; projectile find file: C-c p f
;; projectile helm: C-c p h
;; projectile search grep: C-c p s g
;; projectile search agg (silversearcher): C-c p s s

;; there are default keybindings for jumping to opening and closing delimiters
;; C-M b (move backward)
;; C-M f (move forward)

;; undo/redo
;; C-S-_ (undo)
;; M-S-_ (redo)
;; C-X u (undo-tree)

;; mark navigation
;; C-SPC C-SPC (set mark)
;; Cu C-SPC (pop mark)
;; C-h SPC (show buffer and global marks in helm)
;; NOTE: hold down control when navigating in helm to see preview of where marks are

;; get the current line and column number of point in the clipboard
;; M-l

;; imenu-mode (will require terminal to be configured with escape code for C-;)
;; C-;

;; hydra for multiple-cursors
;; M-u

;; dump jump
;; M-g j (jump to definition)
;; M-g b (jump back)

;; duplicate line
;; C-c d

;; predefined chords
;; (key-chord-define-global "uu" 'undo-tree-visualize)
;; (key-chord-define-global "xx" 'counsel-M-x)
;; (key-chord-define-global "yy" 'counsel-yank-pop)
;; (key-chord-define-global "ii" 'my-open-init-file)
;; (key-chord-define-global "zz" 'my-open-zshrc-file)

;; mouse support toggle (allows dragging of splitters in terminal mode)
;; C-c m (xterm-mouse-mode)
;; TODO: integrate killring with MacOS if iterm: http://iancmacdonald.com/macos/emacs/tmux/2017/01/15/macOS-tmux-emacs-copy-past.html


;; copying file path:linenumber of current buffer
;; M-l (copy file path and line number into system copy buffer and emacs killring)
;; NOTE: find-file is advised to jump to this line so can be pasted into counsel-find-file or helm-find-file with C-y

;; window management
;; C-M-s (hydra for window resizing h,j,k,l)

;; cursor control
;; M-u (hydra for multiple-cursors)
;; C-x m (after a region is selected invoke multiple-cursors edit-lines)

;; block selection of regions
;; C-x SPC

;; open terminal
;; C-c t

;;; Flows

;;; nuclear editing with counsel
;; M-x counsel-ag (do search)
;; C-c C-o (opens as buffer)
;; w (marks the buffer writable)
;; iedit (to pick symbol from list (multiple-cursors will also work for this))
;; C-c C-c (commit)

;;; nuclear editing with helm (easier maybe)
;; M-x helm-ag (do search)
;; C-c C-e (open to edit)
;; C-c C-c (commit) or C-c C-k (abort)

;;; testing regexes
;; C-M-r (Regexp I-search buffer)
;; C-M-s (Regexp I-search backward  in buffer)
;; NOTE: regexes can also easily be tested in a buffer w/ text using:
;;       Eval: (re-search-backward "[[:word:]]"):]]"
;; NOTE: for automatic escaping use
;;       (regexp-quote "^The cat$") => \\^The cat\\$
;; NOTE: for testing in buffer use
;;       visual-regexp (C-M-s)
;; NOTE: prefer jwiegly's regex-tool for serious regex dev
;; M-x regex-tool
;; C-c C-k to quit

;; CREATING A PROJECT SPECIFIC .dir.locals
;; https://endlessparentheses.com/a-quick-guide-to-directory-local-variables.html
;; use add-dir-local-variable in correct dir or just use projectile-edit-dir-locals

;;; CHANGING FACES AND ATTRUBUTES AND DEBUGGING
;; emacs colors: http://www.raebear.net/computers/emacs-colors/

;; list-colors-display - "show available 256 colors"
;; list-faces-display - "show defined faces"
;; describe-face - "show a face's attributes"

;; face attributes can be looked up and used for programmatic setting like this:
;; (face-background 'foo-face)
;; (face-foreground 'foo-face)

;; face attribute setting:
;; (set-face-attribute 'bar-face t :background "SkyBlue4")

;; combinting the 2 approaches:
;; (set-face-attribute 'bar-face t :background (face-foreground 'foo-face))


;; treemacs
;; F9 - "to toggle"
;; neotree
;; F8 - to 'go toggle'

;; SEARCHING
;; preferring helm because can operate across multiple buffers
;; M-s o helm-occur
;; C-s helm-swoop
;; isearch can be handed to helm with M-i inside an isearch session

;;; TIMERS AND IDLE TIMERS

;; timers are stored in  `timer-list' and `timer-idle-list'
;; `run-with-idle-timer' returns a timer object which you can pass to `cancel-timer'.
;; if you did not keep the timer object around, you can cancel by modifying `timer-idle-list' by hand.
;; cookbook: https://www.emacswiki.org/emacs/IdleTimers#toc1

;; SPECIFY CUSTOMIZATIONS SEPARATE FILE
(setq-default custom-file (expand-file-name ".custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load custom-file))

;; FRAME CONTROL (gui only)

(add-to-list 'initial-frame-alist '(fullscreen . maximized))
;; (add-to-list 'default-frame-alist '(fullscreen . fullheight))

;;; ERRATA

;; NOTE: on using ascii codes in iterm
;; we should generally prefer to set correct ascii codes via terminals for good play with zsh
;; e.g.
;; ^;     ^[[59;5u
;; The 59 is semicolon's decimal ASCII code, and the 5 is the code for Ctrl.
;; ^=    ^[[61;5u
;; The 61 is equal's decimal ASCII code, and the 5 is the code for Ctrl.

;; ascii code character table
;; https://theasciicode.com.ar/ascii-printable-characters/at-sign-atpersand-ascii-code-64.html

;; ///////////////////////////////////////////////////////////////////////
;; NOTE: THIS IS COOL .. BUT TOO DRACONIAN!!!
;; !!!! https://stackoverflow.com/questions/10660060/how-do-i-bind-c-in-emacs

;; currently used for tab switching with nswbuff
;; ^TAB    ^[[emacs-C-TAB
;; ^⇫TAB   ^[[emacs-C-S-TAB

(defun my/global-map-and-set-key (key command &optional prefix suffix)
   "`my/map-key' KEY then `global-set-key' KEY with COMMAND.
 PREFIX or SUFFIX can wrap the key when passing to `global-set-key'."
   (my/map-key key)
   (global-set-key (kbd (concat prefix key suffix)) command))

 (defun my/map-key (key)
   "Map KEY from escape sequence \"\e[emacs-KEY\."
   (define-key function-key-map (concat "\e[emacs-" key) (kbd key)))

;; (my/global-map-and-set-key "C-TAB" 'nswbuff-switch-to-previous-buffer)
;; ///////////////////////////////////////////////////////////////////////


;;; DEBUGGING

;; read this anytime you need to debug a function
;; https://endlessparentheses.com/debugging-emacs-lisp-part-1-earn-your-independence.html

;; turn on when doing dev on startup
(setq debug-on-error nil)

;; Use a hook so the message doesn't get clobbered by other messages.
(add-hook 'emacs-startup-hook
	  (lambda ()
	    (message "Emacs ready in %s with %d garbage collections."
		     (format "%.2f seconds"
			     (float-time
			      (time-subtract after-init-time before-init-time)))
		     gcs-done)))


;;; PACKAGE DOWNLOADER SETUP AND CONFIGURATION

;; ;;; setup package sources - straight-el
;; (progn ; `straight.el'
;; (let ((bootstrap-file (concat user-emacs-directory "straight/repos/straight.el/bootstrap.el"))
;;       (bootstrap-version 3))
;;   (unless (file-exists-p bootstrap-file)
;;     (with-current-buffer
;; (url-retrieve-synchronously
;;  "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
;;  'silent 'inhibit-cookies)
;;       (goto-char (point-max))
;;       (eval-print-last-sexp)))
;;   (load bootstrap-file nil 'nomessage))

;; ;;; in order to replace use-package, do the following:
;; ;;; 1. M-% :ensure nil, :straight nil, !
;; ;;; 2. uncomment the 2 lines below
;; ;; (setq straight-use-package-by-default t)
;; ;; (straight-use-package 'use-package)
;; ensure we can install from git sources
;; (use-package git)
;; )


;; NOTE: CREATE MIRROR OF PACKAGES WITH: https://github.com/redguardtoo/elpa-mirror

;; set up package sources - package.el
(progn ; `package.el'
  (require 'package)
  (setq package-enable-at-startup nil)
  ;; (add-to-list 'package-archives '("elpy" . "http://jorgenschaefer.github.io/packages/"))
  (add-to-list 'package-archives '("gnu" . "http://elpa.gnu.org/packages/"))
  (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/"))
  (add-to-list 'package-archives '("org" . "https://orgmode.org/elpa/") t)
  (package-initialize))

(progn ; `use-package'
  (setq use-package-always-ensure t)
  (setq use-package-always-defer t)
  (setq use-package-enable-imenu-support t)
  ;; (setq use-package-minimum-reported-time 0)

    ;;; enable to see loading stats
  ;; (setq use-package-verbose t)
  ;; (setq use-package-compute-statistics t)

  (unless (package-installed-p 'use-package)
    (package-refresh-contents)
    (package-install 'quelpa)
    (package-install 'use-package)
    (package-install 'quelpa-use-package))
  (require 'use-package)

  ;; don't update quelpa melpa on init (for speed)
  (defvar quelpa-update-melpa-p nil)
  ;; (setq use-package-ensure-function 'quelpa) ;; https://github.com/quelpa/quelpa-use-package
  (require 'quelpa-use-package)
  )

;; todo: use this - https://github.com/waymondo/use-package-ensure-system-package
(use-package use-package-ensure-system-package
  :demand t
  :ensure t)


;;----------------------------------------------------------------------------
;; Adjust garbage collection thresholds during startup, and thereafter
;;----------------------------------------------------------------------------
(let ((normal-gc-cons-threshold (* 20 1024 1024))
      (init-gc-cons-threshold (* 128 1024 1024)))
  (setq gc-cons-threshold init-gc-cons-threshold)
  (add-hook 'emacs-startup-hook
            (lambda () (setq gc-cons-threshold normal-gc-cons-threshold))))

;; BUG: WORKAROUND FOR EMACS 26.3 STARTUP LEXIXAL BINDING FOR ^
(setq normal-gc-cons-threshold (* 20 1024 1024))

;; ;; startup profiler
;; (use-package esup
;;   :ensure t)


;; REQUIRED EARLY KEY SETUP

;; TODO: When upgrade to emacs 2.7, remove this. early-init.el should run automatically before this file:

(require 'bind-key)

;; good key binding guidance: https://sam217pa.github.io/2016/09/23/keybindings-strategies-in-emacs/
(use-package general
  :demand t
  :init
  (defalias 'gsetq #'general-setq)
  (defalias 'gsetq-local #'general-setq-local)
  (defalias 'gsetq-default #'general-setq-default))

;; ;; DOOM INIT

;; ;; NOTE: stolen from doom, because they are amazing...
;; ;; TODO: ensure not causing mem leaks in gui
;; (progn
;;   (defvar doom-gc-cons-threshold 16777216 ; 16mb
;;         "The default value to use for `gc-cons-threshold'. If you experience freezing,
;;   decrease this. If you experience stuttering, increase this.")

;;   (defvar doom-gc-cons-upper-limit 268435456 ; 256mb
;;     "The temporary value for `gc-cons-threshold' to defer it.")


;;   (defvar doom--file-name-handler-alist file-name-handler-alist)

;;   (defun doom|restore-startup-optimizations ()
;;         "Resets garbage collection settings to reasonable defaults (a large
;;     `gc-cons-threshold' can cause random freezes otherwise) and resets
;;     `file-name-handler-alist'."
;; 	(setq file-name-handler-alist doom--file-name-handler-alist)
;; 	;; Do this on idle timer to defer a possible GC pause that could result; also
;; 	;; allows deferred packages to take advantage of these optimizations.
;; 	(run-with-idle-timer
;; 	 3 nil (lambda () (setq-default gc-cons-threshold doom-gc-cons-threshold))))

;;   (if (or after-init-time noninteractive)
;;       (setq gc-cons-threshold doom-gc-cons-threshold)
;;     ;; A big contributor to startup times is garbage collection. We up the gc
;;     ;; threshold to temporarily prevent it from running, then reset it later in
;;     ;; `doom|restore-startup-optimizations'.
;;     (setq gc-cons-threshold doom-gc-cons-upper-limit)
;;     ;; This is consulted on every `require', `load' and various path/io functions.
;;     ;; You get a minor speed up by nooping this.
;;     (setq file-name-handler-alist nil)
;;     ;; Not restoring these to their defaults will cause stuttering/freezes.
;;     (add-hook 'after-init-hook #'doom|restore-startup-optimizations)))


  ;; Ensure Doom is running out of this file's directory
  ;; (setq user-emacs-directory (file-name-directory load-file-name))

  ;; In noninteractive sessions, prioritize non-byte-compiled source files to
  ;; prevent stale, byte-compiled code from running. However, if you're getting
  ;; recursive load errors, it may help to set this to nil.
  (setq load-prefer-newer noninteractive)
  ;; instead of
  ;; (setq load-prefer-newer t)


;;; INIT PACKAGES


;; MODLINES

;; (use-package minions
;;   ;; :after (doom-modeline)
;;   :hook ((after-init . minions-mode))
;;   :init (setq
;;          minions-mode-line-lighter "◎"
;;          minions-direct '(flycheck-mode projectile-mode)))


;; start with modeline off in initial buffer so it doesn't flash during startup
(setq mode-line-format nil)


(use-package doom-modeline
  :hook (after-init . doom-modeline-mode)
  :init
  (setq
   ;; doom-modeline-height                   25
   ;; doom-modeline-bar-width                3
   doom-modeline-buffer-file-name-style   'relative-from-project
   doom-modeline-icon                     nil
   doom-modeline-major-mode-icon          nil
   doom-modeline-major-mode-color-icon    nil
   doom-modeline-buffer-state-icon        nil
   doom-modeline-buffer-modification-icon nil
   doom-modeline-minor-modes              nil ;; set t to use minions
   doom-modeline-enable-word-count        nil
   doom-modeline-checker-simple-format    nil
   doom-modeline-vcs-max-length           12
   doom-modeline-persp-name               t
   doom-modeline-lsp                      t
   doom-modeline-github                   t
   doom-modeline-github-interval          (* 30 60)

   doom-modeline-env-version       t
   doom-modeline-env-enable-python t
   doom-modeline-env-enable-ruby   t
   doom-modeline-env-enable-perl   t
   doom-modeline-env-enable-go     t
   doom-modeline-env-enable-elixir t
   doom-modeline-env-enable-rust   t

   doom-modeline-env-python-executable "python3"
   doom-modeline-env-ruby-executable   "ruby"
   doom-modeline-env-perl-executable   "perl"
   doom-modeline-env-go-executable     "go"
   doom-modeline-env-elixir-executable "iex"
   doom-modeline-env-rust-executable   "rustc"

   doom-modeline-mu4e        t
   doom-modeline-irc         t
   doom-modeline-irc-stylize 'identity)
  :config
  (progn
    ;; mode line settings
    (line-number-mode t)
    (column-number-mode t)))


;; THEMES

;; we explicitly set the background to dark here
;; (setq frame-background-mode 'dark)
;; (set-background-color "#000000")
;; (set-foreground-color "#ffffff")

(use-package hl-line
  :hook
  (after-init . global-hl-line-mode)
  :config
  (progn
    ;; highlight line enable and  color
    (global-hl-line-mode 1)
    ;; (set-face-background 'hl-line "#1c1c1c")
    ))

(use-package doom-themes
  :after (centaur-tabs)
  :disabled
  :init
  (load-theme 'doom-one t)
  :config
  (progn
    (interactive)
    ;; (doom-themes-neotree-config)
    (setq doom-neotree-line-spacing 0)
    (doom-themes-org-config)
    (set-background-color "#000000")
    (message "doom-themes configured")))


(use-package inkpot-theme)






(use-package atom-one-dark-theme
  :after (centaur-tabs)
  :init
  (load-theme 'atom-one-dark)
  :config
  (progn
    ;; tune colors for tty and gui mode slightly
    ;; HACK: reloading hl-line here just to ensur that I set color after its loaded
    (use-package hl-line
      :hook
      (after-init . global-hl-line-mode)
      :config
      (progn
        (if  (display-graphic-p)
            (progn
              (set-background-color "gray14")
              (set-face-background 'hl-line "gray11"))
          (progn
            (set-face-background 'hl-line "#1c1c1c")))
        ;; highlight line enable and  color
        (global-hl-line-mode 1)
        ;; (set-face-background 'hl-line "#1c1c1c")

        ;; style hack to ensure the gui colors are correct
        (when (display-graphic-p)
          (set-face-attribute 'helm-source-header nil
                              :background "color-17" :foreground "gray47" :height 1.1 :weight 'bold))

        ))


    (use-package auto-dim-other-buffers
      ;; :after (doom-themes)
      :custom-face
      (auto-dim-other-buffers-face ((t (:background new-auto-dim-background-color))))
      :hook (after-init . auto-dim-other-buffers-mode)
      :config
      (progn
        ;; inferring background color early is causing errors, due to race conditions at init.
        ;; KLUDGE: here we variance betweeen console and gui
        (if (display-graphic-p)
            (setq percent-to-lighten 3.0
                  current-background-color "gray14")
          (setq percent-to-lighten 20
              current-background-color "gray14"))

        (setq new-auto-dim-background-color (color-lighten-name current-background-color percent-to-lighten))
        (set-face-background 'auto-dim-other-buffers-face new-auto-dim-background-color))
      )

    ))

;; toggle between a light and dark theme
;; (use-package heaven-and-hell
;;   :ensure t
;;   :init
;;   (setq heaven-and-hell-theme-type 'dark) ;; Omit to use light by default
;;   (setq heaven-and-hell-themes
;;         '((light . tsdh-light)
;;           (dark . tsdh-dark))) ;; Themes can be the list: (dark . (tsdh-dark wombat))
;;   ;; Optionall, load themes without asking for confirmation.
;;   (setq heaven-and-hell-load-theme-no-confirm t)
;;   :hook (after-init . heaven-and-hell-init-hook)
;;   :bind (("C-c <f6>" . heaven-and-hell-load-default-theme)
;;          ("<f6>" . heaven-and-hell-toggle-theme)))


;; centaur-tabs -- Custom tabline for display buffer list
(use-package centaur-tabs
  :ensure t
  :demand
  :init
  (setq
	centaur-tabs-height 26
	centaur-tabs-set-icons t
	centaur-tabs-set-bar 'left
	centaur-tabs-cycle-scope 'tabs

  ;; generate super/sub scripts: https://lingojam.com/SuperscriptGenerator
  ;; dot-like unicode chars: https://stackoverflow.com/a/36743430
  centaur-tabs-set-modified-marker t
	;; centaur-tabs-modified-marker " ● " ; small
	centaur-tabs-modified-marker " ● " ; small superscript
	;; centaur-tabs-modified-marker " ⚫ " ; medium
  ;; centaur-tabs-modified-marker " ⚫ " ; medium superscript
	;; centaur-tabs-modified-marker " ⬤  " ; large
	;; centaur-tabs-modified-marker " ⬤  " ; large superscript
  ;; centaur-tabs-modified-marker " * "

	centaur-tabs-set-close-button t
  ;; centaur-tabs-close-button " x"
  centaur-tabs-close-button " ˣ" ;; superscript

  centaur-tabs-style "bar"
  ;; centaur-tabs-stylec "wave"
	;; centaur-tabs-set-bar 'under
  )
  :config
  (progn

    ;;; ---------------------------------------------------------------------------
    ;;; NOTE: BUFFER MODIFICATION STATE CHANGE EVENTS TAKEN FROM TABBAR-RULER
    ;;; ---------------------------------------------------------------------------

    (defun my-ignore-buffer-p ()
      (not (buffer-file-name)))

    ;; ref: https://wilkesley.org/~ian/xah/emacs/elisp_hash_table.html
    (setq my-buffer-hashes (make-hash-table :test 'equal))
    (defun my-set-buffer-hash ()
      (puthash (current-buffer) (buffer-hash) my-buffer-hashes))
    (defun my-get-buffer-hash ()
      (gethash (current-buffer) my-buffer-hashes))
    (defun my-buffer-hash-changed-p ()
      (not (string= (buffer-hash) (my-get-buffer-hash))))

    ;; Hook save and change events to show modified buffers in tabbar
    (defun on-saving-buffer ()
      (centaur-tabs-set-template centaur-tabs-current-tabset nil)
      (centaur-tabs-display-update))
    (defun on-modifying-buffer ()
      (unless (my-ignore-buffer-p)
        (my-set-buffer-hash)
        (set-buffer-modified-p (buffer-modified-p))
        (centaur-tabs-set-template centaur-tabs-current-tabset nil)
        (centaur-tabs-display-update)))
    (defun after-modifying-buffer (begin end length)
      (unless (my-ignore-buffer-p)
        (set-buffer-modified-p (my-buffer-hash-changed-p))
        (centaur-tabs-set-template centaur-tabs-current-tabset nil)
        (centaur-tabs-display-update)))

    (add-hook 'after-save-hook 'on-saving-buffer)
    (add-hook 'first-change-hook 'on-modifying-buffer)
    (add-hook 'after-change-functions 'after-modifying-buffer)

    (centaur-tabs-mode t)
    (centaur-tabs-headline-match)
    ;; (centaur-tabs-inherit-tabbar-faces) ; doom-themes compatibility
    (centaur-tabs-group-by-projectile-project) ; projectile compatibility
    (centaur-tabs-change-fonts "arial" 160))
  :bind
  ("C-." . centaur-tabs-forward) ; [46;5u
  ("C-," . centaur-tabs-backward) ; [44;5u
  ;; (:map evil-normal-state-map
	;;       ("g t" . centaur-tabs-forward)
	;;       ("g T" . centaur-tabs-backward))
  :hook
  (dired-mode . centaur-tabs-local-mode))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BEGIN TABBAR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; ;;TABBAR (must be early)
;; ;; TODO: add advice to lighten-darken button faces when active/inactive window

;; ;; tabbar-ruler causes display issues for hydra-powered transient states :-(
;; (use-package tabbar-ruler
;;   :hook (after-init . tabbar-mode)
;;   :defer 0
;;   :init
;;   (setq tabbar-ruler-global-tabbar t
;;         tabbar-ruler-use-mode-icons nil))


;; (use-package tabbar
;;   :after (tabbar-ruler)
;;   :hook (after-init . tabbar-mode)
;;   :demand t
;;   :init
;;   (setq tabbar-cycle-scope 'tabs)
;;   :config
;;   (progn


;; ;;     (defhydra hydra-tabbar (:color red :hint nil)
;; ;;          "
;; ;; tabbar
;; ;; _n_ext
;; ;; _p_revious
;; ;; _q_uit"
;; ;;          ("n" tabbar-forward)
;; ;;          ("p" tabbar-backward)
;; ;;          ("q" nil "cancel" :color blue))

;;     ;; collides with region selection with ctrl held down
;;     ;; (define-key global-map [C-right] 'tabbar-forward)
;;     ;; (define-key global-map [C-left] 'tabbar-backward)


;;     ;; hide the scroll buttons on the left, put some padding to push out a bit
;;     (customize-set-variable 'tabbar-scroll-right-button '((" ") " "))
;;     (customize-set-variable 'tabbar-scroll-left-button '((" ") " "))
;;     (customize-set-variable 'tabbar-buffer-home-button '(("  ") "  "))


;;     (progn
;;       ;; do styling
;;       ;; setup all the tabbar face attribs with some small alignment hacks

;;       ;; background color of tabbar strip
;;       (setq tabbar-background-color "#3A3A3A")

;;       (set-face-attribute
;;        'tabbar-default nil
;;        :background "black"
;;        :foreground "gray20"
;;        :box '(:style nil))

;;       (set-face-attribute
;;        'tabbar-unselected nil
;;        :background "gray30"
;;        :foreground "white"
;;        :underline nil
;;        :box '(:style nil))

;;       (set-face-attribute
;;        'tabbar-modified nil
;;        :background "gray30"
;;        :foreground "red"
;;        :underline nil
;;        :box '(:style nil))

;;       (set-face-attribute
;;        'tabbar-selected nil
;;        :background "white"
;;        :foreground "black"
;;        :underline nil
;;        :box '(:style nil))

;;       ;; highlight is hover behavior
;;       (set-face-attribute
;;        'tabbar-highlight nil
;;        :background "DarkCyan"
;;        :foreground "green"
;;        :underline nil
;;        :box '(:color "DarkCyan" :style nil))

;;       ;; defaults for button
;;       (set-face-attribute
;;        'tabbar-button nil
;;        :underline nil
;;        :box '(:line-width 1 :color "white" :style nil))

;;       (set-face-attribute
;;        'tabbar-unselected nil
;;        :background "gray34"
;;        :foreground "white"
;;        :box '(:style released-button))

;;       (set-face-attribute
;;        'tabbar-modified nil
;;        :background "gray34"
;;        :foreground "pink"
;;        :inherit 'tabbar-unselected
;;        :box '(:style nil))

;;       (set-face-attribute
;;        'tabbar-selected-modified  nil
;;        :foreground "MediumOrchid2"
;;        :inherit 'tabbar-selected
;;        :weight 'bold
;;        :box '(:line-width 1 :color "white" :style released-button))

;;       (set-face-attribute
;;        'tabbar-unselected-modified  nil
;;        :background "gray34"
;;        :foreground "pink"
;;        :inherit 'tabbar-modified
;;        :box '(:style nil))

;;       (set-face-attribute
;;        'tabbar-selected nil
;;        :background "#bcbcbc"
;;        :foreground "black"
;;        :box nil)

;;       (set-face-attribute
;;        'tabbar-button nil
;;        :box '(:line-width 1 :color "gray72" :style released-button))


;;       (set-face-attribute
;;        'tabbar-separator nil
;;        :height 0.7)

;;       ;; set big fonts
;;       (set-face-attribute
;;        'tabbar-button nil
;;        :inherit 'tabbar-default-face
;;        :box '(:line-width 1 :color "gray30"))

;;       (set-face-attribute 'tabbar-default  nil
;;                           ;;:family "Courier"
;;                           :height 1.1)

;;       (set-face-attribute
;;        'tabbar-selected nil
;;        :inherit 'tabbar-default-face
;;        :foreground "deep sky blue"
;;        :background (face-attribute 'default :background)
;;        :box '(:line-width 1 :color "DarkGoldenrod")
;;        :weight 'bold)

;;       (set-face-attribute
;;        'tabbar-unselected nil
;;        :inherit 'tabbar-default-face
;;        :box '(:line-width 1 :color "gray70"))
;;       ;;end set big fonts

;;       )


;;     ;; change padding of the tabs
;;     ;; we also need to set separator to avoid overlapping tabs by highlighted tabs
;;     (custom-set-variables
;;      '(tabbar-separator (quote (0.5))))


;;     (defun my-cached (func)
;;       "Turn a function into a cache dict."
;;       (lexical-let ((table (make-hash-table :test 'equal))
;;                     (f func))
;;         (lambda (key)
;;           (let ((value (gethash key table)))
;;             (if value
;;                 value
;;               (puthash key (funcall f) table))))))


;;     ;;; ---------------------------------------------------------------------------
;;     ;;; override tabbar-buffer-groups in order to specify custom groups and buffer assignments
;;     ;;; NOTE: from https://github.com/unthingable/dotfiles/blob/master/.emacs.d/init.el#L424
;;     ;;; ---------------------------------------------------------------------------

;;     (defun my-tabbar-buffer-groups ()
;;       "Return the list of group names the current buffer belongs to.
;;      Return a list of one element based on major mode."
;; 	    (list
;; 	     (cond
;;         ((memq major-mode '(org-mode org-agenda-mode diary-mode))
;;          "OrgMode"
;;          )
;; 	      ((or (get-buffer-process (current-buffer))
;;              ;; Check if the major mode derives from `comint-mode' or
;;              ;; `compilation-mode'.
;;              (tabbar-buffer-mode-derived-p
;;               major-mode '(comint-mode compilation-mode)))
;; 	       "Process"
;; 	       )
;; 	      ((member (buffer-name)
;;                  '("*scratch*" "*copy-log*" "*Messages*" "*dashboard*" "TAGS"))
;; 	       "Common"
;; 	       )
;; 	      ((eq major-mode 'dired-mode)
;; 	       "Dired"
;; 	       )
;; 	      ((memq major-mode
;;                '(help-mode apropos-mode Info-mode Man-mode))
;; 	       "Help"
;; 	       )
;; 	      ((memq major-mode
;;                '(rmail-mode
;;                  rmail-edit-mode vm-summary-mode vm-mode mail-mode
;;                  mh-letter-mode mh-show-mode mh-folder-mode
;;                  gnus-summary-mode message-mode gnus-group-mode
;;                  gnus-article-mode score-mode gnus-browse-killed-mode))
;; 	       "Mail"
;; 	       )
;; 	      ((memq (current-buffer)
;;                ;; Group tabs by projectile projects
;;                (condition-case nil
;;                    (projectile-buffers-with-file-or-process (projectile-project-buffers))
;;                  (error nil)))
;; 	       (projectile-project-name))
;; 	      (t
;; 	       ;; Return `mode-name' if not blank, `major-mode' otherwise.
;; 	       (if (and (stringp mode-name)
;;                   ;; Take care of preserving the match-data because this
;;                   ;; function is called when updating the header line.
;;                   (save-match-data (string-match "[^ ]" mode-name)))
;;              mode-name
;;            (symbol-name major-mode))
;; 	       )
;;         )))


;;     ;; evaluate again to clear cache
;;     (setq cached-ppn (my-cached 'my-tabbar-buffer-groups))

;;     (defun my-tabbar-groups-by-project ()
;;       (funcall cached-ppn (buffer-name)))

;;     (setq tabbar-buffer-groups-function 'my-tabbar-groups-by-project)

;;     (defun my-toggle-group-by-project ()
;;       (interactive)
;;       (setq my-group-by-project (not my-group-by-project))
;;       (message "Grouping by project alone: %s"
;;                (if my-group-by-project "enabled" "disabled"))
;;       (setq cached-ppn (my-cached 'my-tabbar-buffer-groups)))


;;     ;; ;; uncached version
;;     ;; (setq tabbar-buffer-groups-function 'my-tabbar-buffer-groups)

;;     ;;; ---------------------------------------------------------------------------



;;     ;; override tabbar-buffer-tab-label in order to add spaces to label begin & end,
;;     ;; so that label names to balance them in middle of tab
;;     (defun tabbar-buffer-tab-label (tab)
;;             "Return a label for TAB.
;;          That is, a string used to represent it on the tab bar."
;; 	    (let ((label  (if tabbar--buffer-show-groups
;; 			      (format " [%s] " (tabbar-tab-tabset tab))
;; 			    (format " %s " (tabbar-tab-value tab)))))
;; 	      ;; Unless the tab bar auto scrolls to keep the selected tab
;; 	      ;; visible, shorten the tab label to keep as many tabs as possible
;; 	      ;; in the visible area of the tab bar.
;; 	      (if tabbar-auto-scroll-flag
;; 		  label
;; 		(tabbar-shorten
;; 		 label (max 1 (/ (window-width)
;; 				 (length (tabbar-view
;;                   (tabbar-current-tabset)))))))))



;;     ;;; ---------------------------------------------------------------------------
;;     ;;; NOTE: SPECIFY BUFFERS TO IGNORE
;;     ;;; ---------------------------------------------------------------------------

;;     ;; ;; set any buffers we don't want tabs for
;;     ;; ;; example: nswbuff is a fundamental buffer, we dont' want tabbar to pick it up
;;     ;; ;; because causes a tab bar instance in the nswbuff status window to appear
;;     ;; (setq *tabbar-ignore-buffers* '(" *nswbuff*"))
;;     ;; (setq tabbar-buffer-list-function
;;     ;;       (lambda ()
;;     ;;         (remove-if
;;     ;;          (lambda (buffer)
;;     ;;            (loop for name in *tabbar-ignore-buffers* ;remove buffer name in this list.
;;     ;;                       thereis (string-equal (buffer-name buffer) name)))
;;     ;;          (buffer-list))))


;;     ;; hide some buffers


;;     (defvar buffer-show-whitelist-regexps
;;       (concat "^"
;;               (regexp-opt
;;                '("*ansi-term*"
;;                  ;; "*compilation*"
;;                  "*scratch*"
;;                  "*copy-log"
;;                  "*Messages"
;;                  "*shell*"
;;                  "*terminal"
;;                  "*run"
;;                  "*gud"
;;                  "*eshell"
;;                  "*Python"
;;                  "*inferior-lisp"
;;                  "*eww"
;;                  "*xkcd"
;;                  "*slime-repl")))
;;       "Regexps that allow hidden buffers to still be shown in tabbar.")

;;     (setq tabbar-buffer-list-function
;;           (lambda ()
;;             (remove-if
;;              (lambda (buffer)
;;                (and
;;                 (or
;;                  ;; Hide buffers starting with " " or "*"
;;                  (find (aref (buffer-name buffer) 0) " *")
;;                  ;; Hide TAGS
;;                  (string-equal (buffer-name buffer) "TAGS"))
;;                 (not
;;                  ;; Show buffers on the whitelist
;;                  (string-match buffer-show-whitelist-regexps (buffer-name buffer)))))
;;              (buffer-list))))



;;     ;;; ---------------------------------------------------------------------------
;;     ;;; NOTE: BUFFER MODIFICATION STATE CHANGE EVENTS TAKEN FROM TABBAR-RULER
;;     ;;; ---------------------------------------------------------------------------

;;     (defun my-ignore-buffer-p ()
;;       (not (buffer-file-name)))

;;     ;; ref: https://wilkesley.org/~ian/xah/emacs/elisp_hash_table.html
;;     (setq my-buffer-hashes (make-hash-table :test 'equal))
;;     (defun my-set-buffer-hash ()
;;       (puthash (current-buffer) (buffer-hash) my-buffer-hashes))
;;     (defun my-get-buffer-hash ()
;;       (gethash (current-buffer) my-buffer-hashes))
;;     (defun my-buffer-hash-changed-p ()
;;       (not (string= (buffer-hash) (my-get-buffer-hash))))

;;     ;; Hooks based on yswzing's hooks, but modified for this function state.
;;     ;; called each time the modification state of the buffer changed
;;     (defun tabbar-ruler-modification-state-change ()
;;       (tabbar-set-template tabbar-current-tabset nil)
;;       (tabbar-display-update))

;;     ;; first-change-hook is called BEFORE the change is made
;;     (defun tabbar-ruler-on-buffer-modification ()
;;       (set-buffer-modified-p t)
;;       (tabbar-ruler-modification-state-change))
;;     (add-hook 'after-save-hook 'tabbar-ruler-modification-state-change)

;;     ;; Hook save and change events to show modified buffers in tabbar
;;     (defun on-saving-buffer ()
;;       (tabbar-set-template tabbar-current-tabset nil)
;;       (tabbar-display-update))
;;     (defun on-modifying-buffer ()
;;       (unless (my-ignore-buffer-p)
;;         (my-set-buffer-hash)
;;         (set-buffer-modified-p (buffer-modified-p))
;;         (tabbar-set-template tabbar-current-tabset nil)
;;         (tabbar-display-update)))
;;     (defun after-modifying-buffer (begin end length)
;;       (unless (my-ignore-buffer-p)
;;         (set-buffer-modified-p (my-buffer-hash-changed-p))
;;         (tabbar-set-template tabbar-current-tabset nil)
;;         (tabbar-display-update)))

;;     (add-hook 'after-save-hook 'on-saving-buffer)
;;     (add-hook 'first-change-hook 'on-modifying-buffer)
;;     (add-hook 'after-change-functions 'after-modifying-buffer)



;;     ;;; ---------------------------------------------------------------------------
;;     ;;; NOTE: SPECIFY TAB ORDER (add new buffers to the begging of tab list)
;;     ;;; https://emacs.stackexchange.com/questions/35363/tabbar-optimizing-sorting-of-tabs-in-alphabetical-order
;;     ;;; ---------------------------------------------------------------------------

;;     ;; redefine tabbar-add-tab so that it alphabetizes / sorts the tabs
;;     (defun tabbar-add-tab (tabset object &optional append)
;;       "Add to TABSET a tab with value OBJECT if there isn't one there yet.
;; If the tab is added, it is added at the beginning of the tab list,
;; unless the optional argument APPEND is non-nil, in which case it is
;; added at the end."
;;       (let ((tabs (tabbar-tabs tabset)))
;;         (if (tabbar-get-tab object tabset)
;;             tabs
;;           (let* ((tab (tabbar-make-tab object tabset))
;;                  (tentative-new-tabset
;;                   (if append
;;                       (append tabs (list tab))
;;                     (cons tab tabs)))
;;                  (new-tabset
;;                   (sort
;;                    tentative-new-tabset
;;                    #'(lambda (e1 e2)
;;                        (string-lessp
;;                         (format "%s" (car e1)) (format "%s" (car e2)))))))
;;             (tabbar-set-template tabset nil)
;;             (set tabset new-tabset)))))


;; ;;; MOVING TABS
;;     ;; ref: http://amitp.blogspot.com/2007/04/emacs-buffer-tabs.html
;;     ;; NOTE: there is a bug on multiple moves to the right
;;     (defun tabbar-move-tab (&optional right)
;;       "Move current tab to the left or to the right
;; if RIGHT is set."
;;       (let* ((ctabset nil)
;;              (ctabs nil)
;;              (ctab nil)
;;              (hd nil)
;;              (tl nil))
;;         (and
;;          (setq ctabset (tabbar-current-tabset 't))
;;          (setq ctabs (tabbar-tabs ctabset))
;;          (setq ctab (tabbar-selected-tab ctabset))
;;          (setq tl ctabs)
;;          (setq hd '())) ;; nil
;;         (while (and (cdr tl) (not (eq ctab (car tl))) (not (eq ctab (cadr tl))))
;;           (setq hd (append hd (list (car tl)))
;;                 tl (cdr tl)))
;;         (set ctabset
;;              (cond
;;               ((and (not right) (null hd) (eq ctab (car tl)))
;;                (append (cdr tl) (list (car tl))))
;;               ((not right)
;;                (append hd (list (cadr tl)) (list (car tl)) (cddr tl)))
;;               ((and right (not (cddr tl)))
;;                (append (list (cadr tl)) hd (list (car tl))))
;;               ((and right (eq ctab (car tl)))
;;                (append hd (list (cadr tl)) (list (car tl)) (cddr tl)))
;;               (right
;;                (append hd (list (car tl)) (list (caddr tl)) (list (cadr tl)) (cdddr tl)))
;;               ))
;;         (put ctabset 'template nil)
;;         (tabbar-display-update)))

;;     (defun tabbar-move-tab-left ()
;;       "Move tab left."
;;       (interactive)
;;       (tabbar-move-tab))

;;     (defun tabbar-move-tab-right ()
;;       "Move tab right."
;;       (interactive)
;;       (tabbar-move-tab t))

;;     ;; TODO: tune this as needed
;;     ;; NOTE: C-o is a valuable key
;;     (eval-after-load "hydra"
;;       '(defhydra hydra-tabbar
;;          (global-map "C-t")
;;          "tabbar control"
;;          ("h" tabbar-backward "prev tab" :color purple :column "Switch Tab")
;;          ("l" tabbar-forward "next tab" :color purple)

;;          ("<right>" tabbar-move-tab-right "→" :color blue :column "Move Tab")
;;          ("<left>" tabbar-move-tab-left "←")
;;          ;; ("j" tabbar-move-tab-left "move tab ←")
;;          ;; ("k" tabbar-move-tab-right "move tab →" :color blue :column "Move Tab")

;;          ("RET" nil "done" :color red :column nil)
;;          ("q" nil "close" :color red :column nil)))

;;     ;; basic bindings

;;     ;; NOTE: in iTerm requires:
;;     (global-set-key  (kbd "C-.") 'tabbar-forward) ; [46;5u
;;     (global-set-key  (kbd "C-,") 'tabbar-backward) ; [44;5u




;;     ))

;; (setq erc-header-line-uses-tabbar-p t)

;; ;; TODO: make a seperate autoloaded package for tabbar
;; ;; ala, https://gitlab.com/jgkamat/dotfiles/blob/13da6a341688e5fb1e74d6521dc9a3a941dd0cde/emacs/.emacs.d/lisp/tabbar-tweaks.el#L210

;; (general-define-key
;;  :keymaps 'tabbar-mode-map
;;  ;; Tabbar config
;;  "M-h" #'tabbar-backward-tab
;;  "M-l" #'tabbar-forward-tab
;;  "M-H" #'tabbar-backward-group
;;  "M-L" #'tabbar-forward-group)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; END TABBAR
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; THESE KEYBINDINGS SEEM MORE ERGONOMIC AND MORE POWERFUL

;; disabling M-k, normally bound to (kill-sentence &optional ARG)
;; it's a bit dangerous with the keys below
(global-unset-key "\M-k")


;;; CUSTOM THEME OVERRIDES
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-dim-other-buffers-face ((t (:background "#111"))))
 '(company-tooltip-selection ((t (:background "blue" :bold t))))
 '(font-lock-variable-name-face ((t (:foreground "violet"))))
 '(helm-source-header ((t (:background "color-17" :foreground "black" :weight bold :height 1.3 :family "Sans Serif"))))
 '(highlight-symbol-face ((t (:background "color-17" :foreground "black" :weight bold :height 1.3 :family "Sans Serif"))))
 '(iedit-occurrence-face ((t (:background "magenta" :foreground "black"))))
 '(magit-diff-added ((((type tty)) (:foreground "green"))))
 '(magit-diff-added-highlight ((((type tty)) (:foreground "brightgreen"))))
 '(magit-diff-context-highlight ((((type tty)) (:foreground "default"))))
 '(magit-diff-file-heading ((((type tty)) nil)))
 '(magit-diff-hunk-heading ((((type tty)) (:foreground "magenta"))))
 '(magit-diff-hunk-heading-highlight ((((type tty)) (:background "blue" :foreground "brightyellow"))))
 '(magit-diff-hunk-heading-selection ((((type tty)) (:background "blue" :foreground "magenta"))))
 '(magit-diff-removed ((((type tty)) (:background nil :foreground "red"))))
 '(magit-diff-removed-highlight ((((type tty)) (:background nil :foreground "brightred"))))
 '(magit-section-highlight ((((type tty)) nil))))


;; TODO: RENABLE
; ;;; SET DEFAULT FONTS (only applicable in gui mode)
; ;; NOTE: in gui mode, run M-x helm-select-xfont to see available fonts
; Test char and monospace:
; 0123456789abcdefghijklmnopqrstuvwxyz [] () :;,. !@#$^&*
; 0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ {} <> "'`  ~-_/|\?
(cond
 ((find-font (font-spec :name "DejaVu Sans Mono"))
  (set-frame-font "DejaVu Sans Mono-13"))
 ((find-font (font-spec :name "inconsolata"))
  (set-frame-font "inconsolata-13"))
 ((find-font (font-spec :name "Lucida Console"))
  (set-frame-font "Lucida Console-13"))
 ((find-font (font-spec :name "monaco"))
  (set-frame-font "monaco-13"))
 ((find-font (font-spec :name "courier"))
  (set-frame-font "courier-13")))


;;; ELGET SETUP

(use-package el-get
  :demand t
  :config
  (progn
    (add-to-list 'el-get-recipe-path "~/.emacs.d/el-get-user/recipes")

    ;; NOTE: currently using quelpa for emacswiki packages
    ;; do emacswiki init if this is the first time
    (unless (file-directory-p el-get-recipe-path-emacswiki)
      (progn
        (el-get-emacswiki-refresh el-get-recipe-path-emacswiki t)
        (el-get-emacswiki-build-local-recipes)))))


(use-package use-package-el-get
  :demand t)

;; add gitlab.com to known_hosts if not already done
;; TODO: find a better way to do this
(defun ensure-known-hosts (fqdn)
    (progn
      (let
	  ((marker-file  (format "%sssh-key-known" user-emacs-directory)))
	(when (not (file-exists-p marker-file))
	  (let
	      ((result (string-trim
			(shell-command-to-string
			 (format "ssh-keygen -l -F %s || echo " fqdn)))))
	    (when (string-empty-p result)
	      (shell-command-to-string
	       (format
		"ssh-keyscan -t rsa %s  >> ~/.ssh/known_hosts && touch %s" fqdn marker-file ))))))))

(ensure-known-hosts "gitlab.com")

;;; CUSTOM THEME OVERRIDES


;;; ELGET SETUP


;; (el-get-bundle dotfiles
;;   :url "https://gitlab.com/jclosure/dotfiles.git"
;;   :feature "init-dotfiles"
;;   :submodule nil
;;   ;;(load-file "~/.emacs.d/el-get/dotfiles/init-dotfiles.elc")
;;   )


;; ;; https://oli.me.uk/2015/03/03/making-el-get-sync-like-vim-plug/
;; ;; requires: ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
;; (el-get-bundle dotfiles
;;   :type git
;;   :url "git@gitlab.com:jclosure/dotfiles.git"
;;   :features (init-dotfiles dotfiles general)
;;   :compile ("*\\.el$" "helpers/")
;;   :load-path ("./site-lisp" "./site-lisp/helpers"))


;; #########################################################

;; BASIC PROGRAMMING SUPPORT

;; NOTE: cl has been deprecated as of Emacs27, replaced by cl-lib
(require 'cl-lib)

;; NOTE: one or more of the below depend upon 'cl
(use-package dash :config (require 'dash))                               ;; lists --> https://github.com/magnars/dash.el
(use-package dash-functional :demand t :config (dash-enable-font-lock))  ;; function list combinators
(use-package ht :demand t)                                               ;; hash-tables --> https://github.com/Wilfred/ht.el
(use-package s :demand t)                                                ;; strings --> https://github.com/magnars/s.el
(use-package a :demand t)                                                ;; association lists --> https://github.com/plexus/a.el
(use-package f :demand t)                                                ;; files and directories --> https://github.com/rejeep/f.el
(use-package loop :demand t)                                             ;; imperative loop structures --> https://github.com/Wilfred/loop.el
(use-package async :demand t)                                            ;; asynchronous processing --> https://github.com/jwiegley/emacs-async
(use-package deferred :demand t)                                         ;; facilities to manage asynchronous tasks --> https://github.com/kiwanami/emacs-deferred


;; SITE-LISP

;; extend load-path recursively
;; add any path to this list to be loaded from top-level down
(defconst site-lisp-path (f-join user-emacs-directory "site-lisp"))
(defconst load-paths-list (list
                      site-lisp-path))
(mapcar '(lambda(p)
           (add-to-list 'load-path p)
           (cd p) (normal-top-level-add-subdirs-to-load-path))
        load-paths-list)


;; HOW TO USE use-package to load a package from local dir (not already in load-path)
;; (require 'helpers)
(use-package helpers
  :ensure nil
  :demand t)

;; (require 'init-dotfiles)
(use-package init-dotfiles
  :ensure nil
  :demand t)

;; #########################################################

;; GERRIT-SUPPORT
;; TODO: get this working -> https://github.com/terranpro/magit-gerrit
;; https://emacs.stackexchange.com/questions/19672/magit-gerrit-push-to-other-branch
;; NOTE: default keys R-P collide with magit rename
;; (use-package magit-gerrit)
;; TODO: switch to gerrit management. integrate into flow. run: M-x magit-gerrit-dispatch
;; the melpa package is from the github repo above it uses magit-popup which is deprecated.
;; instead use -> https://github.com/darcylee/magit-gerrit which supports the new  transient
(el-get-bundle magit-gerrit :bundle-async t :type github :pkgname "darcylee/magit-gerrit")

;; TODO: when this becomes a melpa pkg, switch to use-package
(el-get-bundle helm-icons :bundle-async t :type github :pkgname "yyoncho/helm-icons")


;; POINT HISTORY: C-\

(use-package popwin :defer t)

;; TODO: try to get these so not require demand

;; manual fetch: (quelpa '(point-history :fetcher github :repo "blue0513/point-history"))
(use-package point-history
  :ensure nil
  :demand t
  :after (popwin)
  :quelpa (point-history :fetcher github :repo "blue0513/point-history")
  :init
  (setq point-history-save-timer 5
        point-history-ignore-buffer "^ \\*Minibuf\\|^ \\*point-history-show*"
        point-history-should-preview t)

  :config
  (progn

    ;; (add-hook 'after-init-hook (lambda ()
    (point-history-mode t)

    ;; global toggle
    ;; (global-set-key [(control ?\\)] 'point-history-show)

    ;; avoid tracking point in certain modes
    ;; (setq point-history-ignore-major-mode '(emacs-lisp-mode ruby-mode))

    ;; mode map key bindings
    (define-key point-history-show-mode-map (kbd "q") 'point-history-close)
    (define-key point-history-show-mode-map (kbd "n") 'point-history-next-line)
    (define-key point-history-show-mode-map (kbd "p") 'point-history-prev-line)
    ))


  ;; manual fetch: (quelpa '(ivy-point-history :fetcher github :repo "SuzumiyaAoba/ivy-point-history"))
  (use-package ivy-point-history
    :ensure nil
    :demand t
    :after (point-history)
    :quelpa (ivy-point-history :fetcher github :repo "SuzumiyaAoba/ivy-point-history")
    :config
    (progn
      ;; global toggle
      (global-set-key [(control ?\\)] 'ivy-point-history)
      ))



;; POINT-RING NAV
;; TODO: try idle-timer-based point-ring https://qiita.com/zk_phi/items/c145b7bd8077b8a0f537
;; XOR
;; use change-bashed point-ring w/ jump back/forward
;; navigate changes in buffer forward and backward
(use-package goto-chg
  :defer 3
  :config
  (progn
    (global-set-key (kbd "M--") 'goto-last-change)
    (global-set-key (kbd "M-=") 'goto-last-change-reverse)))


;;; CURSOR

(when (not (display-graphic-p))

  ;; my solution
  (load "terminal-cursor")
  ;; when in terminal switch in/out of vertical-bar cursor
  ;; (set-cursor-type 'vertical-bar)
  ;; switch it back when we exit
  (add-hook 'kill-emacs-hook (lambda () (set-cursor-type 'block)))


  ;; ;; NOTE: or use a seperate more flexible package "term-cursor.el"
  ;; ;; ;; try it with: printf '\033[n q', where n is one of the options above
  (el-get-bundle term-cursor-el :type github :pkgname "h0d/term-cursor.el")
  (global-term-cursor-mode)
  ;; (setq-default cursor-type 'box)
  (setq-default cursor-type 'bar)
  ;; (setq-default cursor-type 'hbar)
  ;; (setq-default cursor-type 'hollow)

  ;; cursor blinks when not in use
  ;; (blink-cursor-mode 1)
)

;; (defvar blink-cursor-colors (list  "#92c48f" "#6785c5" "#be369c" "#d9ca65")
;;    "On each blink the cursor will cycle to the next color in this list.")
;; (setq blink-cursor-count 3)
;; (defun blink-cursor-timer-function ()
;;   ;;   "Zarza wrote this cyberpunk variant of timer `blink-cursor-timer'.
;;   ;; Warning: overwrites original version in `frame.el'.

;;   ;; This one changes the cursor color on each blink. Define colors in `blink-cursor-colors'."
;;   (when (not (internal-show-cursor-p))
;;     (when (>= blink-cursor-count (length blink-cursor-colors))
;;       (setq blink-cursor-count 0))
;;     (set-cursor-color (nth blink-cursor-count blink-cursor-colors))
;;     (setq blink-cursor-count (+ 1 blink-cursor-count)))
;;   (internal-show-cursor nil (not (internal-show-cursor-p))))





;; show full current open file path in frame title
(setq frame-title-format
      (list (format "%s %%S: %%j " (system-name))
        '(buffer-file-name "%f" (dired-directory dired-directory "%b"))))


;; Local Variables:
;; byte-compile-warnings: (not free-vars)
;; End:
