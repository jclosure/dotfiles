;;; early-init.el --- early bird  -*- no-byte-compile: t -*-

;; ref: https://github.com/emacscollective/auto-compile

;; (setq load-prefer-newer t)


;; ;; Load recursively a directory
;; (defconst elisp-path '("~/.emacs.d/elisp"))
;; (mapcar '(lambda(p)
;;            (add-to-list 'load-path p)
;;            (cd p) (normal-top-level-add-subdirs-to-load-path)) elisp-path)

;; NOTE: This is problematic when compiling helm in elpa
;; (use-package auto-compile
;;   :demand t
;;   :config
;;   (auto-compile-on-load-mode)
;;   (auto-compile-on-save-mode)
;;   (setq auto-compile-display-buffer               nil)
;;   (setq auto-compile-mode-line-counter            t)
;;   (setq auto-compile-source-recreate-deletes-dest t)
;;   (setq auto-compile-toggle-deletes-nonlib-dest   t)
;;   (setq auto-compile-update-autoloads             t))


;;; early-init.el ends here
