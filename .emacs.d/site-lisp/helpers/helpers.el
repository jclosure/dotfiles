;;; package --- Summary
;;; Code:
;;; Commentary:
;;
;; Mostly lifted from: https://www.emacswiki.org/emacs/ElispCookbook


(defun edit-init-file ()
  "Open the init file."
  (interactive)
  ;;(find-file user-init-file)
  (find-file user-init-file))

(defun edit-dotfile ()
  "Open the init file."
  (interactive)
  ;;(find-file user-init-file)
  (find-file (f-join site-lisp-path  "init-dotfiles.el")))

(defun reload-config-files ()
  "Reload the init file."
  (interactive)
  (load-file user-init-file)
  (load-file (f-join site-lisp-path  "init-dotfiles.el")))

(defun edit-zsh-dotfile ()
  "Open the init file."
  (interactive)
  (find-file "~/.zshrc"))

;; TODO: consider changing the switch-to commands to open in other buffer

(defun switch-to-messages ()
  (interactive)
  (switch-to-buffer "*Messages*"))

(defun switch-to-scratch ()
  (interactive)
  (switch-to-buffer "*scratch*"))

(defun switch-to-dashboard ()
  (interactive)
  (switch-to-buffer "*dashboard*"))

(defun switch-to-copy-log ()
  (interactive)
  (switch-to-buffer "*copy-log*"))

;;  KEYBOARD AUTOMATION
(defun just-push-it (&rest keys)
  "Function to simulate pushing arbitrary keys
Examples:
(just-push-it \"C-x 3\" \"C-x o\" \"M-x ansi-term\" \"RET\" \"RET\")
(just-push-it \"C-g\")
"
  (setq unread-command-events
        (append (apply 'vconcat (mapcar 'kbd keys)) nil)))

(defun send-keyboard-quit ()
  (interactive)
  (just-push-it "C-g"))


;; HELPER FUNCTIONS
(defun advice-unadvise (sym)
  "Remove all advices from symbol SYM."
  (interactive "aFunction symbol: ")
  (advice-mapc (lambda (advice _props) (advice-remove sym advice)) sym))

(defun string/ends-with (s ending)
  "Return non-nil if string S ends with ENDING."
  (cond ((>= (length s) (length ending))
         (string= (substring s (- (length ending))) ending))
        (t nil)))
(defun string/starts-with (s begins)
  "Return non-nil if string S starts with BEGINS."
  (cond ((>= (length s) (length begins))
         (string-equal (substring s 0 (length begins)) begins))
        (t nil)))


(defun fill-buffer ()
  "Format existing text to 80 columns"
  (interactive)
  (save-excursion
    (save-restriction
      (widen)
      (fill-region (point-min) (point-max)))))


(provide 'helpers)

;;; helpers.el ends here
