;;; my-undo-hooks.el --- Undo hooks for user-defined functionality -*- lexical-binding: t -*-

;; ---------------------------------------------------------------------------
;; Undo Chain
;;
;; Usage:
;;
;; (require "undo-hooks")
;;
;; (defun my-before-undo-change-fn ()
;;   (interactive)
;;   (message "you entered an undo chain"))
;; (defun my-after-undo-change-fn ()
;;   (interactive)
;;   (message "you ended the undo chain"))
;;
;; (add-hook 'xxx-chain-begin-hook 'my-before-undo-change-fn)
;; (add-hook 'xxx-chain-begin-hook 'my-after-undo-change-fn)
;;
;; (xxx-chain-install)


(defcustom xxx-chain-begin-hook nil
  "Hook run before the first undo command is made."
  :type 'hook
  :group 'xxx)

(defcustom xxx-chain-end-hook nil
  "Hook run after the first non-undo command is made."
  :type 'hook
  :group 'xxx)

(defcustom xxx-chain-commands (list 'save-buffer 'undo)
  "Commands that wont break the undo chain."
  :type '(repeat symbol)
  :group 'xxx)

;; ------------------
;; Internal Variables

(defvar-local xxx-chain--is-enabled nil)
(defvar-local xxx-chain--dont-break-for-this-command nil)

;; ---------
;; Callbacks

(defun xxx-chain--pre-command ()
  "Utility to run before the command to check if undo is used."
  (setq xxx-chain--dont-break-for-this-command nil))

(defun xxx-chain--continue (old-function &rest args)
  "Utility to run on undo, so as not to break the chain."
  (setq xxx-chain--dont-break-for-this-command t)
  (prog1 (apply old-function args)))

(defun xxx-chain--after-change (_beg _end _len)
  "Checks if `xxx-chain--continue' ran, break the chain if it didn't."
  ;; Can be nil since removing the 'after-change' doesn't happen immediately.
  (when xxx-chain--is-enabled
    (unless xxx-chain--dont-break-for-this-command
      (xxx-chain--end))))

;; ---------------
;; Begin/End Chain

(defun xxx-chain--begin ()
  "Begin a chain of undo commands."
  ;; In case the lock was already enabled, re-enable.
  (when xxx-chain--is-enabled
    (xxx-chain--end))

  (setq xxx-chain--is-enabled t)

  (run-hooks 'xxx-chain-begin-hook)

  (dolist (elem xxx-chain-commands)
    (advice-add elem :around #'xxx-chain--continue '((name . "xxx-chain-continue"))))

  (add-hook 'pre-command-hook #'xxx-chain--pre-command :local)
  (add-hook 'after-change-functions #'xxx-chain--after-change :local)

  (message "Undo Lock Acquired"))

(defun xxx-chain--end ()
  "End a chain of undo commands."

  (dolist (elem xxx-chain-commands)
    (advice-remove elem "xxx-chain-continue"))

  (remove-hook 'pre-command-hook #'xxx-chain--pre-command :local)
  (remove-hook 'after-change-functions #'xxx-chain--after-change :local)

  ;; Clear to indicate we're not locked.
  (setq xxx-chain--is-enabled nil)

  (run-hooks 'xxx-chain-end-hook)

  (message "Undo Lock Released"))

(defun xxx-chain--ensure (&rest _)
  (unless xxx-chain--is-enabled
    (setq xxx-chain--dont-break-for-this-command t)
    (xxx-chain--begin)))

(defun xxx-chain-install (&rest args)
  "Add support for undo chain hooks."
  (advice-add 'undo :before #'xxx-chain--ensure '((name . "xxx-chain"))))
(defun xxx-chain-uninstall (&rest args)
  "Remove support for undo chain hooks."
  (advice-remove 'undo '((name . "xxx-chain"))))

(provide 'my-undo-hooks)
;;; my-undo-hooks.el ends here
