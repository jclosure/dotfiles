;;; package --- Summary
;;; Code:
;;; Commentary:


(setq-default indent-tabs-mode nil) ; use spaces for indentation

(defvar my-indentation-width 2
  "The number of spaces I prefer for line indentation.")

(defun my-enter ()
  "Inserts a newline character then indents the new line just
like the previous line"
  (interactive)
  (newline)
  (indent-relative-maybe))

(defun my-indent ()
  "When point is on leading white-space of a non-empty line, the
line is indented `my-indentation-width' spaces. If point is at
the beginning of an empty line, inserts `my-indentation-width'
spaces."
  (interactive)
  (insert (make-string my-indentation-width ? )))

(defun my-indentation-setup ()
  "Binds RETURN to the function `my-enter' and TAB to call
`my-indent'"
  (local-set-key "\r" 'my-enter)
  (setq indent-line-function 'my-indent))

(defun delete-trailing-whitespace-and-blank-lines ()
  "Deletes all whitespace at the end of a buffer (or, rather, a
buffer's accessible portion, see `Narrowing'), including blank
lines."
  (interactive)
  (let ((point (point)))
    (delete-trailing-whitespace)
    (goto-char (point-max))
    (delete-blank-lines)
    (goto-char (min point (point-max)))))

;; make sure trailing whitespace is removed every time a buffer is saved.
(add-hook 'before-save-hook 'delete-trailing-whitespace-and-blank-lines)

;; globally install my indentation setup
(global-set-key "\r" 'my-enter)
(setq indent-line-function 'my-indent)

;; also override key setting of major-modes, if any
(add-hook 'after-change-major-mode-hook 'my-indentation-setup)

;;; my-enter.el ends here
