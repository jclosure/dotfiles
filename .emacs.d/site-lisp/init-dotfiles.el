;;; PACKAGE --- Summary
;;; Code:
;;; Commentary:

;; load gpg files
(defun load-gpg-files ()
  (interactive)
  (require 'epa-file)
  ;; (custom-set-variables '(epg-gpg-program  "/usr/local/bin/gpg2"))
  (add-to-list 'exec-path "/usr/local/bin")
  (epa-file-enable)
  (when (eq 0 (shell-command "gpg --list-key 9F263813B9426874"))
    (load-library (format "%s.secrets/work.el.gpg" user-emacs-directory))))

(add-hook 'emacs-startup-hook 'load-gpg-files)

;; 1. detect system type
;; 2. run setup.sh for correct system type
;; 3. copy .zshrc into place
;; 4.

;;; USAGE
;; TODO: .... load buffer w/ key ....

;;; DETECT ENVIRONMENT

(progn
  ;; Documentation for `system-type':
  ;; Value is symbol indicating type of operating system you are using.
  ;; Special values:
  ;;   `gnu'         compiled for a GNU Hurd system.
  ;;   `gnu/linux'   compiled for a GNU/Linux system.
  ;;   `darwin'      compiled for Darwin (GNU-Darwin, Mac OS X, ...).
  ;;   `ms-dos'      compiled as an MS-DOS application.
  ;;   `windows-nt'  compiled as a native W32 application.
  ;;   `cygwin'      compiled using the Cygwin library.
  ;; Anything else indicates some sort of Unix system.

  (defconst *is-a-mac* (eq system-type 'darwin))
  (defconst *is-a-linux* (eq system-type 'gnu/linux))
  (defconst *is-in-tmux* (getenv "TMUX"))
  (defconst *is-in-ssh* (or (getenv "SSH_CLIENT") (getenv "SSH_TTY") (getenv "SSH_CONNECTION")))
  (defconst *is-in-terminal* (not (display-graphic-p))))

;;; PLATFORM INSTALLATION

;; TODO: come up with a better solution for doing this that does not use el-get
(setq *zshrc-includes-file* (format "%sel-get/dotfiles/zsh/.zshrc-includes" user-emacs-directory))
(setq *zshrc-iterm-file* (format "%sel-get/dotfiles/zsh/.zshrc-iterm" user-emacs-directory))

;; (setq *zshrc-file* (cond (*is-a-mac* (format "%sel-get/dotfiles/macos/.zshrc" user-emacs-directory))
;;       (*is-a-linux* (format "%sel-get/dotfiles/linux/dotfiles/.zshrc" user-emacs-directory))))

(when (not (file-exists-p "~/.zshrc-local"))
  (progn
    (shell-command "touch ~/.zshrc-local")
    (shell-command
     (format "echo 'source %s' >> ~/.zshrc" *zshrc-includes-file*))
    (shell-command
     (format "echo 'source ~/.zshrc-local' >> ~/.zshrc" *zshrc-includes-file*))
    (shell-command
     (format "echo 'source %s' >> ~/.zshrc" *zshrc-iterm-file*))))


;;; PLATFORM ADAPTATION

;; NOTE: this is only necessary when running a MacOS modified ver of Emacs
;; e.g. https://github.com/railwaycat/homebrew-emacsmacport
(when *is-a-mac*
  (progn
    ;; set keys for Apple keyboard, for emacs-mac, because they
    ;; default meta to ⌘
    (setq mac-option-modifier 'meta) ; make opt key do Super
    (setq mac-control-modifier 'control) ; make Control key do Control
    ))

(when (and *is-a-mac* *is-in-terminal*)
  (progn
    ;; make scroll up down work consistently when in terminal on osx
    ;; TODO: eval whether to apply in linux via ssh as well
    (global-set-key (kbd "<mouse-4>") (kbd "<wheel-up>"))
    (global-set-key (kbd "<mouse-5>") (kbd "<wheel-down>"))))

;;; CONFIGURE BASIC EMACS OPTIONS:

(progn
  (setq user-full-name "Joel Holder"
        user-mail-address "jclosure@gmail.com")

  ;; make pretty everywhere
  (global-prettify-symbols-mode 1)

  ;; Default Encoding
  (prefer-coding-system 'utf-8-unix)
  (set-locale-environment "en_US.UTF-8")
  (set-default-coding-systems 'utf-8-unix)
  (set-selection-coding-system 'utf-8-unix)
  (set-buffer-file-coding-system 'utf-8-unix)
  (set-clipboard-coding-system 'utf-8) ; included by set-selection-coding-system
  (set-keyboard-coding-system 'utf-8) ; configured by prefer-coding-system
  (set-terminal-coding-system 'utf-8) ; configured by prefer-coding-system
  (setq buffer-file-coding-system 'utf-8) ; utf-8-unix
  (setq save-buffer-coding-system 'utf-8-unix) ; nil
  (setq process-coding-system-alist
        (cons '("grep" utf-8 . utf-8) process-coding-system-alist))

  (setq set-mark-command-repeat-pop t)  ; Repeating C-SPC after popping mark pops it again
  (setq track-eol nil)			; Keep cursor at end of lines.
  (setq line-move-visual nil)		; To be required by track-eol
  ;; NOTE: turn this off if it get's annoying
  (setq-default kill-whole-line t)	; Kill line including '\n'
  (setq-default indent-tabs-mode nil)   ; use space

  ;; set to avoid problems with crontabs, etc.
  (setq require-final-newline t)

  ;; prevent extraneous tabs
  ;; (setq-default indent-tabs-mode nil)

  (setq inhibit-startup-message t)
  (setq initial-scratch-message "")


  ;; backup file control configuration
  (setq make-backup-files t               ; backup of a file the first time it is saved.
        backup-by-copying t               ; don't clobber symlinks
        version-control t                 ; version numbers for backup files
        delete-old-versions t             ; delete excess backup files silently
        delete-by-moving-to-trash t
        kept-old-versions 6               ; oldest versions to keep when a new numbered backup is made (default: 2)
        kept-new-versions 9               ; newest versions to keep when a new numbered backup is made (default: 2)
        auto-save-default t               ; auto-save every buffer that visits a file
        auto-save-timeout 20              ; number of seconds idle time before auto-save (default: 30)
        auto-save-interval 200            ; number of keystrokes between auto-saves (default: 300)
        )

  ;; disable the annoying bell ring
  (setq ring-bell-function 'ignore)

  ;; auto-revert any buffers that change on disk if not dirty
  (global-auto-revert-mode)

  ;; don't blink the cursor
  (blink-cursor-mode 0)



  ;; i find the capitalzing of the first char with this annoying
  (global-unset-key (kbd "M-c"))

  ;; no text wrapping
  (setq-default truncate-lines t)

  ;; remove $ glyph from the end of truncated lines
  ;; TODO: try to change it to: ➜ or ⤾
  ;; http://xahlee.info/comp/unicode_arrows.html
  ;; (set-display-table-slot standard-display-table 0 ?\⤾)
  (set-display-table-slot standard-display-table 0 ?\→)


  ;; set threshold for large files
  (setq large-file-warning-threshold (* 15 1024 1024))

  ;; gdb configuration
  (setq gdb-many-windows t
        gdb-show-main t)

  ;; save command history
  (savehist-mode)

;;; DOOM INJECT
  ;;
;;; General configuration

  (setq-default

   ;; disable lock files, e.g. .#init.el
   ;; create-lockfiles nil

   ;; follow symlinks to their actual location when opening files
   find-file-visit-truename t
   vc-follow-symlinks t

   ansi-color-for-comint-mode t
   bidi-display-reordering nil ; disable bidirectional text for tiny performance boost
   blink-matching-paren nil    ; don't blink--too distracting
   compilation-always-kill t        ; kill compilation process before starting another
   compilation-ask-about-save nil   ; save all buffers on `compile'
   compilation-scroll-output 'first-error
   confirm-nonexistent-file-or-buffer t

   cursor-in-non-selected-windows nil ; hide cursors in other windows

   display-line-numbers-width 3
   echo-keystrokes 0.02
   enable-recursive-minibuffers nil
   frame-inhibit-implied-resize t
   frame-title-format '("%b – Jclosure Emacs") ; simple name in frame title
   ;; remove continuation arrow on right fringe
   fringe-indicator-alist
   (delq (assq 'continuation fringe-indicator-alist)
         fringe-indicator-alist)
   highlight-nonselected-windows nil
   image-animate-loop t
   indicate-buffer-boundaries nil
   indicate-empty-lines nil
   max-mini-window-height 0.3
   mode-line-default-help-echo nil ; disable mode-line mouseovers
   mouse-yank-at-point t           ; middle-click paste at point, not at click
   resize-mini-windows 'grow-only  ; Minibuffer resizing
   show-help-function nil          ; hide :help-echo text
   use-dialog-box nil              ; always avoid GUI
   uniquify-buffer-name-style 'forward
   visible-cursor nil
   x-stretch-cursor nil
   ;; Favor vertical splits
   split-width-threshold 160
   split-height-threshold nil
   ;; `pos-tip' defaults
   pos-tip-internal-border-width 6
   pos-tip-border-width 1
   ;; no beeping or blinking please
   ring-bell-function #'ignore
   visible-bell nil
   ;; don't resize emacs in steps, it looks weird
   window-resize-pixelwise t
   frame-resize-pixelwise t)
  ;; y/n instead of yes/no
  (fset #'yes-or-no-p #'y-or-n-p)
  ;; Truly silence startup message
  (fset #'display-startup-echo-area-message #'ignore)
  ;; relegate tooltips to echo area only
  (if (bound-and-true-p tooltip-mode) (tooltip-mode -1))
  ;; enabled by default; no thanks, too distracting
  (blink-cursor-mode -1)
  ;; Prevent the glimpse of un-styled Emacs by setting these early.
  (add-to-list 'default-frame-alist '(tool-bar-lines . 0))
  (add-to-list 'default-frame-alist '(menu-bar-lines . 0))

  ;; by commenting this, we get vertical scrollbars in gui mode
  ;;(add-to-list 'default-frame-alist '(vertical-scroll-bars))

  ;; allow selected region to be replaced by typing or hitting backspace
  (delete-selection-mode 1)

  ;; indendation control
  (setq-default indent-tabs-mode nil ;; use space
                line-spacing 1
                tab-width 2
                c-basic-offset 2
                cursor-type 'bar)

  ;; The native border "consumes" a pixel of the fringe on righter-most splits,
  ;; `window-divider' does not. Available since Emacs 25.1.
  (setq-default window-divider-default-places t
                window-divider-default-bottom-width 1
                window-divider-default-right-width 1)
  (add-hook 'init-hook #'window-divider-mode)


  ;; small interface tweaks
  (setq inhibit-startup-message t)
  (menu-bar-mode -1)
  (tool-bar-mode -1)
  (fset 'yes-or-no-p 'y-or-n-p)

  ;; enable winner-mode
  (when (fboundp 'winner-mode)
    (winner-mode 1))

  ;; source control should follow symlinks
  (setq vc-follow-symlinks t)

  ;; configure parens
  ;; TODO: see smartparens section below for additional config
  (electric-pair-mode 1)
  (show-paren-mode 1)
  (setq show-paren-delay 0)
  ;; change highlighted paren face
  (set-face-attribute 'show-paren-match nil
                      :weight 'extra-bold
                      :background "blue"
                      ; ;; :background (face-background 'default)
                      :foreground "cyan"))


;;; SMALL HELPERS

(defun alias (new-name prev-name)
  "Set an alias for another function. Forward name from one to another"
  (setf (symbol-function new-name)
        (symbol-function prev-name)))

(defvar print-maybe-on  nil)
(defun print-maybe(msg return-value)
  (when print-maybe-on
    (print msg))
  return-value)

(cl-defun print-path (&optional (path (getenv "PATH")))
  "Print a path string one line at a time. Defaults to $PATH
   Use also by passing an optional arg, e.g. $PYTHONPATH"
  (print (replace-regexp-in-string ":" "\n" path)))

(defun buffer-mode (&optional buffer-or-name)
  "Returns the major mode associated with a buffer.
      If buffer-or-name is nil return current buffer's mode."
  (buffer-local-value 'major-mode
                      (if buffer-or-name
                          (get-buffer buffer-or-name)
                        (current-buffer))))

(defun prevent-kill-buffer (buff-name)
  (with-current-buffer buff-name
    (emacs-lock-mode 'kill)))

(defun rename-file-and-buffer (new-name)
  "Renames both current buffer and file it's visiting to NEW-NAME."
  (interactive "sNew name: ")
  (let ((name (buffer-name))
        (filename (buffer-file-name)))
    (if (not filename)
        (message "Buffer '%s' is not visiting a file!" name)
      (if (get-buffer new-name)
          (message "A buffer named '%s' already exists!" new-name)
        (progn
          (rename-file filename new-name 1)
          (rename-buffer new-name)
          (set-visited-file-name new-name)
          (set-buffer-modified-p nil))))))



(defun usage ()
  "Open the init file."
  (interactive)
  (find-file (f-join user-emacs-directory "usage.el")))


;; duplicate a line for playing with or changing it's syntax
(defun duplicate-line ()
  (interactive)
  (save-excursion
    (let ((line-text (buffer-substring-no-properties
                      (line-beginning-position)
                      (line-end-position))))
      (move-end-of-line 1)
      (newline)
      (insert line-text))))
(global-set-key (kbd "C-c d") 'duplicate-line)


(defun kill-all-buffers ()
  (interactive)
  (mapc 'kill-buffer (buffer-list)))

(defun face-exists-p (face)
    "check if font exists"
    (if (member face (face-list)) t nil))

(defun get-faces (pos)
  "Get the font faces at POS."
  (remq nil
        (list
         (get-char-property pos 'read-face-name)
         (get-char-property pos 'face)
         (plist-get (text-properties-at pos) 'face))))

(defun what-faces (pos)
    (interactive "d")
    	(let ((face (or (get-char-property (point) 'read-face-name)
    	    (get-faces pos))))
    (if face (message "Face: %s" face) (message "No face at %d" pos))))



;;; CUSTOM BEHAVIOR CHANGES

;; ensures that menu-bar-mode is enabled before showing it
(progn
  ;; cleanup whitespace at the end of lines before save
  (add-hook 'before-save-hook 'delete-trailing-whitespace)

  ;; enable menu-bar-mode if open menu items
  (defun my-enable-menu-bar-mode ()
    "Enable menu bar mode before opening a menu item"
    (interactive)
    (menu-bar-mode 1))

  (defadvice menu-bar-open (before menu-bar-open-before activate)
    (my-enable-menu-bar-mode)))



;; TODO: fix this before using
;; (defun my-mark-current-line (&optional arg)
;;   "Uses shift selection to select the current line.
;; When there is an existing shift selection, extends the selection
;; in the appropriate direction to include current line."
;;   (interactive "p")
;;   (let ((oldval (or (cdr-safe transient-mark-mode) transient-mark-mode))
;;         (backwards (and mark-active (> (mark) (point))))
;;         (beg (and mark-active (mark-marker))))
;;     (unless beg
;;       (if backwards (end-of-line) (beginning-of-line))
;;       (setq beg (point-marker)))
;;     (if backwards (end-of-line (- 1 arg)) (beginning-of-line (+ 1 arg)))
;;     (unless mark-active
;;       (push-mark beg nil t))
;;     (setq transient-mark-mode (cons 'only oldval))))


;; converts emacs to sane forward/backard deleting mechanism
;; stop at left/right margins on line when killing words
;; mimic other editors behavior with Alt-backspace and Alt-d
(progn

  (defun get-position-of-beginning-of-line ()
    (let ((orig-point (point)))
      (beginning-of-line)
      (let ((beg-line-point (point)))
        (goto-char orig-point)
        beg-line-point)))

  (defun get-position-of-end-of-line ()
    (let ((orig-point (point)))
      (end-of-line)
      (let ((end-line-point (point)))
        (goto-char orig-point)
        end-line-point)))

  (defun line-bounds-nonwhitespace ()
    (save-excursion
      (goto-char (point-at-bol))
      (skip-syntax-forward " " (point-at-eol))
      (let ((beg (point)))
        (goto-char (point-at-eol))
        (skip-syntax-backward " " (point-at-bol))
        (list beg (point)))))

  (defun at-end-of-line-text-p ()
    (= (point) (cadr (line-bounds-nonwhitespace))))

  (defun at-beginning-of-line-text-p ()
    (= (point) (car (line-bounds-nonwhitespace))))

  (defun my-delete-backward-word ()
    (interactive "*")
    (push-mark)
    (backward-word)
    (delete-region (point) (mark))
    )

  (defun my-delete-forward-word ()
    (interactive "*")
    (push-mark)
    (forward-word)
    (delete-region (point) (mark)))

   ;;; DELETING M-<delete>, M-d

  (defun my-backward-delete-word-on-this-line ()
    (interactive)
    (letrec ((orig-point (point))
             (beg-line-point (get-position-of-beginning-of-line))
             (line-string (buffer-substring-no-properties beg-line-point orig-point)))
      (backward-word)
      (let ((backward-word-point (point)))
        (goto-char orig-point)
        (cond
         ((= beg-line-point orig-point)
          ;; opt 1: delete-backward-word
          ;; (my-delete-backward-word)
          ;; opt 2: go to end of line above
          (delete-backward-char 1))
         ((string-match "^[\s\t]*$" line-string)
          (progn
            (push-mark)
            (goto-char beg-line-point)
            (delete-region (point) (mark))))
         ((> beg-line-point backward-word-point)
          (progn
            (push-mark)
            (goto-char (car (line-bounds-nonwhitespace)))
            (delete-region (point) (mark))))
         (t
          (my-delete-backward-word))))))

  (defun my-forward-delete-word-on-this-line ()
    (interactive)
    (let ((orig-point (point))
          (end-line-point (cadr (line-bounds-nonwhitespace))))
      (forward-word 1)
      (let ((forward-word-point (point)))
        (goto-char orig-point)
        (print (list orig-point end-line-point))
        (cond
         ((= orig-point end-line-point)
          (progn
            (push-mark)
            (goto-char (+ end-line-point 1))
            (goto-char (car (line-bounds-nonwhitespace)))
            (delete-region (point) (mark))
            (goto-char end-line-point)))
         ((< end-line-point forward-word-point)
          (progn
            (push-mark)
            (goto-char end-line-point)
            (delete-region (point) (mark))))
         (t
          (my-delete-forward-word))))))

  ;; don't put in kill ring and stop on line
  (define-key global-map [remap backward-kill-word] 'my-backward-delete-word-on-this-line)
  (define-key global-map [remap kill-word] 'my-forward-delete-word-on-this-line)

 ;;; MOVEMENT M-<left>, M-<right>

  ;; (defun EXAMPLE-HOW-TO-STOP-AT-THE-BEGINNING-OF-THE-VISUAL-LINE ()
  ;;   (interactive)
  ;;   (let ((orig-point (point))
  ;;         (beg-line-point (get-position-of-beginning-of-line)))
  ;;     (forward-word (- 1))
  ;;     (let ((backward-word-point (point)))
  ;;       (goto-char orig-point)
  ;;       (cond
  ;;        ((= beg-line-point orig-point)
  ;;         (backward-char 1))
  ;;        ((> beg-line-point backward-word-point)
  ;;         (goto-char beg-line-point))
  ;;        (t (goto-char backward-word-point))
  ;;        )
  ;;       )))

  (defun my-backward-word-on-this-line ()
    (interactive)
    (let ((orig-point (point))
          (beg-line-point (get-position-of-beginning-of-line)))
      (forward-word (- 1))
      (let ((backward-word-point (point))
            (line-string (buffer-substring-no-properties beg-line-point orig-point)))
        (goto-char orig-point)
        (cond
         ((string-match "^[\s\t]*$" line-string)
          (goto-char (- beg-line-point 1)))
         ((> beg-line-point backward-word-point)
          (goto-char (car (line-bounds-nonwhitespace))))
         (t (goto-char backward-word-point))))))


  (defun my-forward-word-on-this-line ()
    (interactive)
    (let ((orig-point (point))
          (end-line-point (cadr (line-bounds-nonwhitespace))))
      (forward-word 1)
      (let ((forward-word-point (point)))
        (goto-char orig-point)
        ;;(print (list orig-point end-line-point))
        (cond
         ((= orig-point end-line-point)
          (progn
            (goto-char (+ end-line-point 1))
            (goto-char (car (line-bounds-nonwhitespace)))))
         ((< end-line-point forward-word-point)
          (goto-char end-line-point))
         (t (goto-char forward-word-point))))))

  (define-key global-map [remap backward-word] 'my-backward-word-on-this-line)
  (define-key global-map [remap forward-word] 'my-forward-word-on-this-line)



  )





;; get a copy of where point is on file:linenumber in clipboard.  it also prints it.
(progn
  (defun copy-current-line-position-to-clipboard ()
    "Copy current line in file to clipboard as '</path/to/file>:<line-number>'"
    (interactive)
    (let ((path-with-line-number
           (concat (buffer-file-name) ":" (number-to-string (line-number-at-pos)))))
      (x-select-text path-with-line-number) ;; write to system copy buffer
      (kill-new  path-with-line-number) ;; write to emacs killring
      (message (concat path-with-line-number " copied to clipboard"))))

  ;; NOTE: may interfere with mc hydra's l
  (define-key global-map (kbd "M-RET") 'copy-current-line-position-to-clipboard))


;; handle opening files with line numbers directly.  ;; works with C-x C-f
;; note: because this is using advice on such a highly used fn, it might
;; conflict if other packages try to do the samething.
(progn
  (defun find-file--line-number (orig-fun filename &optional wildcards)
    "Turn files like file.cpp:14 into file.cpp and going to the 14-th line."
    (save-match-data
      (let* ((matched (string-match "^\\(.*\\):\\([0-9]+\\):?$" filename))
             (line-number (and matched
                               (match-string 2 filename)
                               (string-to-number (match-string 2 filename))))
             (filename (if matched (match-string 1 filename) filename)))
        (apply orig-fun (list filename wildcards))
        (when line-number
          ;; goto-line is for interactive use
          (goto-char (point-min))
          (forward-line (1- line-number))))))

  (advice-add 'find-file :around #'find-file--line-number))


;; smarter copy/cut -  current line or region if selected
(progn
  (defun xah-cut-line-or-region ()
    "Cut current line, or text selection.
When `universal-argument' is called first, cut whole buffer (respects `narrow-to-region').

URL `http://ergoemacs.org/emacs/emacs_copy_cut_current_line.html'
Version 2015-06-10"
    (interactive)
    (if current-prefix-arg
        (progn ; not using kill-region because we don't want to include previous kill
          (kill-new (buffer-string))
          (delete-region (point-min) (point-max)))
      (progn (if (use-region-p)
                 (kill-region (region-beginning) (region-end) t)
               (kill-region (line-beginning-position) (line-beginning-position 2))))))


  (defun xah-copy-line-or-region ()
    "Copy current line, or text selection.
When called repeatedly, append copy subsequent lines.
When `universal-argument' is called first, copy whole buffer (respects `narrow-to-region').

URL `http://ergoemacs.org/emacs/emacs_copy_cut_current_line.html'
Version 2018-09-10"
    (interactive)
    (if current-prefix-arg
        (progn
          (copy-region-as-kill (point-min) (point-max)))
      (if (use-region-p)
          (progn
            (copy-region-as-kill (region-beginning) (region-end)))
        (if (eq last-command this-command)
            (if (eobp)
                (progn )
              (progn
                (kill-append "\n" nil)
                (kill-append
                 (buffer-substring-no-properties (line-beginning-position) (line-end-position))
                 nil)
                (progn
                  (end-of-line)
                  (forward-char))))
          (if (eobp)
              (if (eq (char-before) 10 )
                  (progn )
                (progn
                  (copy-region-as-kill (line-beginning-position) (line-end-position))
                  (end-of-line)))
            (progn
              (copy-region-as-kill (line-beginning-position) (line-end-position))
              (end-of-line)
              (forward-char)))))))

  (global-set-key (kbd "M-w" ) 'xah-copy-line-or-region)
  (global-set-key (kbd "C-w" ) 'xah-cut-line-or-region))


;; ;; recenters on M-,
;; ;; NOTE: C-l by default will toggle between middle, top, bottom

;; solution 1: after advise the xref-pop-marker-stack fn
;; (defun my-center-in-window ()
;;   (let ((recenter-positions '(middle)))
;;         (recenter-top-bottom)))
;; (advice-add 'xref-pop-marker-stack :after #'my-center-in-window)

;; solution 2: use the builtin hook xref-after-return-hook
(defun my-center-in-window-hook ()
  (let ((recenter-positions '(middle)))
        (recenter-top-bottom)))
(add-hook 'xref-after-return-hook 'my-center-in-window-hook)



;;; EMACS SERVER
;; ;; enable server by default
;; (load "server")
;; (unless (server-running-p)
;;   (add-hook 'after-init-hook 'server-start t))


;; (use-package server
;;   :config (or (server-running-p) (server-mode)))


;;; SYSTEM  CLIPBOARD  CONFIG

;; prints copy/paste data into *Messages* buffer
(setq *debug-clipboard-io* t)

;; variables ref: https://with-emacs.com/posts/tutorials/almost-all-you-need-to-know-about-variables/
;; set this var upstream if you want to debug, it will override this default
(defvar *debug-clipboard-io* nil)

(defun write-to-clipboard-log-maybe (&rest args)
  (when *debug-clipboard-io*
    (apply #'write-to-clipboard-log args)))

(cl-defun write-to-clipboard-log (text &optional (header nil) &optional (footer nil))
  (interactive "sEnter the text: ")
  (let ((log-buffer-name "*copy-log*")
        (working-buffer (buffer-name)))
    (setq log-buffer (get-buffer-create log-buffer-name))
    (with-current-buffer log-buffer-name ; replace with the name of the buffer you want to append
      (goto-char (point-max))
      ;; maybe instead: (format-time-string "%Y-%m-%d %H:%M:%S")
      (let ((delimited (concat "--- " "%s" " in " working-buffer  ": "  (current-time-string)  " ---\n")))
        (when header
          (insert (propertize (format delimited header) 'face '(:foreground "purple" :weight bold))))
        (insert (format "%s\n" text))
        (when footer
          (insert (propertize (format delimited footer) 'face '(:inverse-video t))))))))

;; TODO: implement as a macro for better performance
(defun get-current-func-name ()
  "Get the symbol of the function this function is called from."
  ;; 5 is the magic number that makes us look
  ;; above this function
  (let* ((index 5)
         (frame (backtrace-frame index)))
    ;; from what I can tell, top level function call frames
    ;; start with t and the second value is the symbol of the function
    (while (not (equal t (first frame)))
      (setq frame (backtrace-frame (incf index))))
    (second frame)))

;; https://www.gnu.org/software/emacs/manual/html_node/emacs/Clipboard.html
;; (setq save-interprogram-paste-before-kill t)

(when (and *is-a-mac* *is-in-terminal*)
  (if *is-in-tmux*

      (progn

        "When we are running under macos in tmux reattach-to-user-namespace.
         be sure to: brew install reattach-to-user-namespace"
        (defun copy-from-osx ()
          "Use OSX clipboard to paste. PULL"
          (let ((text (shell-command-to-string "reattach-to-user-namespace pbpaste")))
            (write-to-clipboard-log-maybe text (get-current-func-name))
            text))

        (defun paste-to-osx (text &optional push)
          "Add kill ring entries (TEXT) to OSX clipboard.  PUSH."
          (let ((process-connection-type nil))
            (let ((proc (start-process "pbcopy" "*Messages*" "reattach-to-user-namespace" "pbcopy")))
              (write-to-clipboard-log-maybe text (get-current-func-name))
              (process-send-string proc text)
              (process-send-eof proc))))

        (setq interprogram-cut-function 'paste-to-osx)
        (setq interprogram-paste-function 'copy-from-osx))

    (progn

      ;; "When we are running in native macos"
      (defun copy-from-osx ()
        "Use OSX clipboard to paste. PULL"
        (let ((text (shell-command-to-string "pbpaste")))
          (write-to-clipboard-log-maybe text (get-current-func-name))
          text))

      (defun paste-to-osx (text &optional push)
        "Add kill ring entries (TEXT) to OSX clipboard.  PUSH."
        (let ((process-connection-type nil))
          (let ((proc (start-process "pbcopy" "*Messages*" "pbcopy")))
            (write-to-clipboard-log-maybe text (get-current-func-name))
            (process-send-string proc text)
            (process-send-eof proc))))

      (setq interprogram-cut-function 'paste-to-osx)
      (setq interprogram-paste-function 'copy-from-osx))))


;; NOTE: MAKE SURE THIS DOESN'T COLLIDE WITH ABOVE PBSOLUTION
;; Use macOS clipboard
;; NOTE: this assumes local osx not remotely run ssh, although does support tramp
;; NOTE: has issues with early init, thus deferring a long time
(use-package pbcopy
  :ensure t
  ;; :hook (after-init . (turn-on-pbcopy))
  :defer 5
  :if (eq system-type 'darwin)
  :config
  (turn-on-pbcopy))


;; NOTE: IMPORTANT!!! this assumes you are using ssh
;; with bi-directional clipboard port forwarding already
;; established. otherwise, you probably want to disable this.
;; a recipe for this on mac (and linux) can be found here:
;; https://gist.github.com/jclosure/19698429dda1105b8a93b0832c07ebc7
(when (and *is-a-linux* *is-in-terminal* *is-in-ssh*)

  (progn

    (defun is-port-listening (host port)
      "Tests if a tcp port is open"
      (interactive)
      (string-match-p "succeeded!"
                      (shell-command-to-string (format "nc -zv %s %d" host port))))

    (defun copy-from-remote ()
      "Use netcat clipboard to paste. PULL"
      (let ((text (shell-command-to-string "nc localhost 2225")))
        (write-to-clipboard-log-maybe text (get-current-func-name))
        text))

    (defun paste-to-remote (text &optional push)
      "Add kill ring entries (TEXT) to netcat clipboard.  PUSH."
      (let ((process-connection-type nil))
        (let ((proc (start-process "remote-clipboard-copy" "*Messages*" "nc" "localhost" "2224")))
          (write-to-clipboard-log-maybe text (get-current-func-name))
          (process-send-string proc text)
          (process-send-eof proc))))

    ;; if the reverse forarding is done we'll use the ports
    (if (is-port-listening "localhost" 2225)
        (progn
          (message "remote clipboard listening")
          (setq interprogram-cut-function 'paste-to-remote)
          (setq interprogram-paste-function 'copy-from-remote))
      (message "remote clipboard is not listening. skipping..."))))


;;; ESHELL CONFIG

(setq eshell-hist-ignoredups t)
(setq eshell-cmpl-cycle-completions nil)
(setq eshell-cmpl-ignore-case t)
(setq eshell-ask-to-save-history (quote always))
(setq eshell-prompt-regexp "^[^#$\n]*[#$] "
      eshell-prompt-function
      (lambda nil
        (concat
	 "[" (user-login-name) "@" (system-name) " "
	 (if (string= (eshell/pwd) (getenv "HOME"))
	     "~" (eshell/basename (eshell/pwd)))
	 "]"
	 (if (= (user-uid) 0) "# " "$ "))))
(add-hook 'eshell-mode-hook
          '(lambda ()
             (progn
               (define-key eshell-mode-map "\C-a" 'eshell-bol)
               (define-key eshell-mode-map "\C-r" 'counsel-esh-history)
               (define-key eshell-mode-map [up] 'previous-line)
               (define-key eshell-mode-map [down] 'next-line)
               )))




;;; EARLY PACKAGES


;; put this before any other emacs directories are defined or used
(use-package no-littering
  :demand t
  :config
  ;; don't leave autosaved files in same dir as file, keep here..
  (progn
    (setq auto-save-file-name-transforms
          `((".*" ,(no-littering-expand-var-file-name "auto-save/") t)))

    ;; save place in files - will use no-littering dirs
    (if (fboundp #'save-place-mode)
        (save-place-mode +1)
      (setq-default save-place t))))


(use-package recentf
  :ensure nil
  :init
  (progn
    ;; (setq recentf-auto-cleanup 300)
    ;; (setq recentf-exclude '("~$" "\\.log$"))
    (setq recentf-max-menu-items 50)
    (setq recentf-max-saved-items 50)
    (recentf-mode 1))
  :config
  (progn
    (add-to-list 'recentf-exclude no-littering-var-directory)
    (add-to-list 'recentf-exclude no-littering-etc-directory)
    (add-to-list 'recentf-exclude (format "%s/\\.emacs\\.d/elpa/.*" (getenv "HOME"))))
  (global-set-key "\C-x\ \C-r" 'recentf-open-files))


(use-package windmove
  :ensure nil
  :init
  (progn
    (defun ignore-error-wrapper (fn)
      "Funtion return new function that ignore errors.
   The function wraps a function with `ignore-errors'"
      (let ((fn fn))
        (lambda ()
          (interactive)
          (ignore-errors
            (funcall fn)))))

    (when (fboundp 'windmove-default-keybindings)
      (progn
        (windmove-default-keybindings)
        (global-set-key [s-left] (ignore-error-wrapper 'windmove-left))
        (global-set-key [s-right] (ignore-error-wrapper 'windmove-right))
        (global-set-key [s-up] (ignore-error-wrapper 'windmove-up))
        (global-set-key [s-down] (ignore-error-wrapper 'windmove-down))))


    ;; NOTE: windmove conflicts with org-mode's defaults for the following:
    ;; ref: https://orgmode.org/manual/Conflicts.html

    ;; org-mode-map <S-down> -> (org-shiftdown &optional ARG)
    ;; org-mode-map <S-up>   -> (org-shiftup &optional ARG)
    ;; org-mode-map <S-left> -> (org-shiftleft &optional ARG)
    ;; org-mode-map <S-right> -> (org-shiftright &optional ARG)

    ;; instead use the other bindings in org-mode for these which are:
    ;; org-mode-map C-c <down>
    ;; org-mode-map C-c <up>
    ;; org-mode-map C-c <left>
    ;; org-mode-map C-c <right>

    ;; Make windmove work in Org mode:
    (add-hook 'org-shiftup-final-hook 'windmove-up)
    (add-hook 'org-shiftleft-final-hook 'windmove-left)
    (add-hook 'org-shiftdown-final-hook 'windmove-down)
    (add-hook 'org-shiftright-final-hook 'windmove-right)

    ;;; setting org-mode's shift-select behavior below

    ;; "Everywhere except timestamps
    ;; (setq org-support-shift-select 'always)

    ;; "When outside special context"
    (setq org-support-shift-select t)

    ;; "Never"
    ;; (setq org-support-shift-select nil)
    ))

;; browse backup files
;; M-x backup-walker-start
;; p or n to prev next
(use-package backup-walker
  :commands backup-walker-start)


;; https://github.com/bbatsov/crux
(use-package crux
  :defer 2
  :bind ("C-k" . 'crux-smart-kill-line))

;; chords borrowed from prelude.
(use-package key-chord
  :defer 2
  :config
   (progn
     (setq key-chord-one-key-delay 0.5)
     (key-chord-mode 1)
     ;; (key-chord-define-global "bb" 'crux-switch-to-previous-buffer)
     (key-chord-define-global "uu" 'undo-tree-visualize)

     ;; copy/cut/paste, aka kill/copy-as-kill/yank
     (key-chord-define-global "xx" 'xah-cut-line-or-region)
     (key-chord-define-global "cc" 'xah-copy-line-or-region)
     (key-chord-define-global "vv" 'yank)
     (key-chord-define-global "yy" 'counsel-yank-pop)

   ;; disable key-chord-mode in the minibuffer
   (defun disable-key-chord-mode ()
     (set (make-local-variable 'input-method-function) nil))

   ;; disable key-chord in the following buffers and mods
   (add-hook 'minibuffer-setup-hook #'disable-key-chord-mode)
   (add-hook 'magit-mode-hook #'disable-key-chord-mode)))


(use-package undo-tree
  :defer 1
  :commands undo-tree-mode
  :diminish undo-tree-mode
  :config
  (progn
    (custom-set-variables
     '(undo-tree-visualizer-timestamps t)
     '(undo-tree-visualizer-diff t))
     (global-undo-tree-mode)))


;; cursor config - (only works in gui)
(use-package bar-cursor
  :demand t
  :config
  (bar-cursor-mode 1))


;;; REGEX

(use-package regex-tool
  :ensure t
  :commands regex-tool
  ;; :custom
  ;; (regex-tool-backend 'perl)
  )

;; (use-package pcre2el
;;   :ensure t)

;; (use-package visual-regexp
;;   :defer
;;     :bind (("C-M-r" . vr/isearch-backward)
;;          ("C-M-s" . vr/isearch-forward)
;;          ("C-M-%" . vr/query-replace)))

;; ;; usage: https://github.com/benma/visual-regexp-steroids.el/
;; (use-package visual-regexp-steroids
;;   :ensure
;;   :defer
;;   :bind (("C-M-r" . vr/isearch-backward)
;;          ("C-M-s" . vr/isearch-forward)
;;          ("C-M-%" . vr/query-replace)))

(use-package anzu
  :config
  (progn
    (global-anzu-mode +1)
    ;; (global-set-key (kbd "M-%") 'anzu-query-replace)
    ;; FIXME: C-M only works in gui
    ;; (global-set-key (kbd "C-M-%") 'anzu-query-replace-regexp)

    (custom-set-variables
     '(anzu-mode-lighter "")
     '(anzu-deactivate-region t)
     '(anzu-search-threshold 1000)
     '(anzu-replace-threshold 50)
     '(anzu-replace-to-string-separator " => "))

    (define-key isearch-mode-map [remap isearch-query-replace]  #'anzu-isearch-query-replace)
    (define-key isearch-mode-map [remap isearch-query-replace-regexp] #'anzu-isearch-query-replace-regexp)
    ))


;;; TERMINAL

;; zsh and multi-term: http://rawsyntax.com/blog/learn-emacs-zsh-and-multi-term/
(use-package multi-term
  :bind (("C-c t" . multi-term)
         ("C-c \"" . multi-term-dedicated-toggle))
  :config
  (setq multi-term-program (getenv "SHELL")
        multi-term-buffer-name "term"
        multi-term-dedicated-select-after-open-p t)
  (setenv "ZSH_THEME" "simple")
  (setenv "TERM" "xterm-256color")
  (add-hook 'term-mode-hook
            (lambda ()
              ;; scroll an entire screen

              ;; page-UP/page-DOWN (prior/next) to scroll an entire screen
              (add-to-list 'term-bind-key-alist '("<next>" . scroll-up))
              (add-to-list 'term-bind-key-alist '("<prior>" . scroll-down))


              ;; scroll just 1 line

              ;; binding C-<up>/C-<down> to scroll 1 line in terminals
              (add-to-list 'term-bind-key-alist '("C-<down>" . scroll-up-line))
              (add-to-list 'term-bind-key-alist '("C-<up>". scroll-down-line))

              ;; also binding M-<up>/M-<down> (M == ESC in MacOS) to scroll 1 line in terminals
              (add-to-list 'term-bind-key-alist '("ESC <down>" . scroll-up-line))
              (add-to-list 'term-bind-key-alist '("ESC <up>" . scroll-down-line))

              ;; also binding M-<up>/M-<down> explicitly for tmux sessions to scroll 1 line
              (add-to-list 'term-bind-key-alist '("M-<down>" . scroll-up-line))
              (add-to-list 'term-bind-key-alist '("M-<up>". scroll-down-line))

              ;; TODO: in tmux mouse scrolling works as expected, get it working in native

              ;; conflict with yasnippet
              (yas-minor-mode -1)
              (company-mode -1)

	      ))
  (add-hook 'term-mode-hook
            (lambda ()
              (define-key term-raw-map (kbd "C-y") 'term-paste)))
  (add-hook 'term-mode-hook
          (lambda ()
            (setq term-buffer-maximum-size 10000))))


;;; FILE SYSTEM

;; ranger
;; NOTE: keep this section commented because causes weirdness with dired+
;; ;; https://github.com/ralesi/ranger.el#standard-ranger-bindings
;; (use-package ranger
;;   :defer 1
;;   :config
;;   (progn
;;     (define-key ranger-normal-mode-map (kbd "+") #'dired-create-directory)

;;     ;;  (add-to-list 'ranger-prefer-regexp "^\\.DS_Store$")
;;     (defcustom ranger-hidden-filter
;;       "^\.|\.(?:pyc|pyo|DS_Store|bak|swp)$|^lost\+found$|^__(py)?cache__$"
;;       "Regexp of filenames to hide."
;;       :group 'ranger
;;       :type 'string)
;;     (setq ranger-max-preview-size 10)
;;     (setq ranger-excluded-extensions '("dat" "p" "iso" "pb" "fb"))
;;     (setq ranger-dont-show-binary t)
;;     (setq ranger-show-hidden t)
;;     (setq ranger-preview-file t)
;;     (setq ranger-dont-show-binary t)
;;     (setq ranger-show-literal nil)
;;     ;; (setq ranger-cleanup-on-disable t)
;;     (setq ranger-cleanup-eagerly t)
;;     (setq ranger-dont-show-binary t)

;;     ;; ;; default ranger over dired
;;     ;; (setq ranger-override-dired 'ranger)
;;     ;; (ranger-override-dired-mode t)

;;     ;; allow dired to stand without ranger
;;     (setq ranger-override-dired nil)
;;     (ranger-override-dired-mode nil)
;;     ))


;; NOTE: we use ranger in place of dired
(use-package dired
  :ensure nil
  :demand t
  :config
  ;; dired - reuse current buffer by pressing 'a'
  (put 'dired-find-alternate-file 'disabled nil)

  ;; always delete and copy recursively
  (setq dired-recursive-deletes 'always)
  (setq dired-recursive-copies 'always)

  ;; if there is a dired buffer displayed in the next window, use its
  ;; current subdir, instead of the current subdir of this dired buffer
  (setq dired-dwim-target t)

  ;; enable some really cool extensions like C-x C-j (dired-jump)
  (require 'dired-x))


;; dired+ adds some features to standard dired (like reusing buffers)

(use-package dired+
  :ensure nil
  ;; :quelpa (dired+ :fetcher url :url "https://www.emacswiki.org/emacs/download/dired+.el")
  ;; :quelpa (dired+ :fetcher wiki)
  :quelpa (dired+ :fetcher github :repo "emacsmirror/dired-plus")
  :defer 1
  :init
  (setq diredp-hide-details-initially-flag nil)
  (setq diredp-hide-details-propagate-flag nil)
  :config
  (diredp-toggle-find-file-reuse-dir 1)
  ;; This binding messes with the arrow keys, for some reason.
  (unbind-key "M-O" dired-mode-map))


;;preview files in dired
(use-package peep-dired
  :disabled t
  :defer 1
  ;; don't access `dired-mode-map' until `peep-dired' is loaded
  :bind (:map dired-mode-map
              ("P" . peep-dired))
  :config
  (setq peep-dired-enable-on-directories t
        peep-dired-cleanup-on-disable t))


;; DESKTOPS/PERSPECTIVES

;; (use-package desktop
;;   :hook
;;   (after-init . desktop-read)
;;   (after-init . desktop-save-mode))

;; ;; remap these config keys
;; (use-package eyebrowse
;;   :bind
;;   ("<f5>" . eyebrowse-switch-to-window-config-1)
;;   ("<f6>" . eyebrowse-switch-to-window-config-2)
;;   ("<f7>" . eyebrowse-switch-to-window-config-3)
;;   ("<f8>" . eyebrowse-switch-to-window-config-4)
;;   :hook
;;   (after-init . eyebrowse-mode)
;;   :custom
;;   (eyebrowse-new-workspace t))

(use-package persp-mode-projectile-bridge
  :config
  (with-eval-after-load "persp-mode-projectile-bridge-autoloads"
      (add-hook 'persp-mode-projectile-bridge-mode-hook
                #'(lambda ()
                    (if persp-mode-projectile-bridge-mode
                        (persp-mode-projectile-bridge-find-perspectives-for-all-buffers)
                      (persp-mode-projectile-bridge-kill-perspectives))))
      (add-hook 'after-init-hook
                #'(lambda ()
                    (persp-mode-projectile-bridge-mode 1))
                t)))

;; NOTE: demanded in order to recover last perspective on init
;; TODO: try using transients for perspective management: https://pastebin.com/ykPpPXqw
(use-package persp-mode
  :demand t
  :init
  (setq-default
   persp-auto-resume-time 0.1)
  :config
  (progn
    (persp-mode 1)
    (setq-default
     persp-auto-save-fname ".persp"
     persp-auto-save-num-of-backups 1
     persp-autokill-buffer-on-remove nil)


    ;; register the copy-log with persp-mode by name
    (setq persp-save-buffer-functions (cons
                                       (lambda (b)
                                         (with-current-buffer b
                                           (when (string= (buffer-name) "*copy-log*")
                                             `(def-copy-log-buffer ,(buffer-name)))))
                                       persp-save-buffer-functions))


    ;; filter out buffers we don't want picked up
    (add-hook 'persp-common-buffer-filter-functions
              ;; there is also `persp-add-buffer-on-after-change-major-mode-filter-functions'
              #'(lambda (b)
                  (or
                   ;; filter out ephemeral buffers
                   (string-prefix-p "*" (buffer-name b))

                   ;; filter out magit buffers
                   (string-prefix-p "magit" (buffer-name b))

                   ;; filter out dired-based buffers (included ranger)
                   (with-current-buffer b
                      (derived-mode-p 'dired-mode)))))
    ))


;; (use-package persp-projectile
;;   :after (perspective)
;;   :bind
;;   ("C-x p" . hydra-persp/body)
;;   :config
;;   (defhydra hydra-persp (:columns 4
;;                          :color blue)
;;     "Perspective"
;;     ("a" persp-add-buffer "Add Buffer")
;;     ("i" persp-import "Import")
;;     ("c" persp-kill "Close")
;;     ("n" persp-next "Next")
;;     ("p" persp-prev "Prev")
;;     ("k" persp-remove-buffer "Kill Buffer")
;;     ("r" persp-rename "Rename")
;;     ("A" persp-set-buffer "Set Buffer")
;;     ("s" persp-switch "Switch")
;;     ("C-x" persp-switch-last "Switch Last")
;;     ("b" persp-switch-to-buffer "Switch to Buffer")
;;     ("P" projectile-persp-switch-project "Switch Project")
;;     ("q" nil "Quit")))


;; ;; ;; TODO: create rules for shackle
(use-package shackle
  :hook
  (after-init . shackle-mode)
  :custom
  (shackle-rules '((help-mode :inhibit-window-quit t :same t)))
  (shackle-select-reused-windows t)
  :config
  (progn
    ;(setq helm-display-function 'pop-to-buffer) ; make helm play nice
    ;;(setq shackle-rules '(("^\\Swoop\\b" :regexp t :align t :size 0.4)))

    ;; make magit always pop to the right at 40% of frame
    (setq shackle-rules '(("\\`\\*magit.*?\\*\\'" :regexp t :align t :size 0.4)))
    ))


;; BUFFER SWITCHING

;; ;; quickly flip forward/backward through open buffers
;; ;; TODO: only works in gui mode? .. add magit buffers to regex list
;; (use-package buffer-flip
;;   :ensure t
;;   :bind  (("M-<tab>" . buffer-flip)
;;           :map buffer-flip-map
;;           ( "M-<tab>" .   buffer-flip-forward)
;;           ( "M-S-<tab>" . buffer-flip-backward)
;;           ( "M-ESC" .     buffer-flip-abort))
;;   :config
;;   (setq buffer-flip-skip-patterns
;;         '("^\\*helm\\b"
;;           "^\\*swiper\\*$")))


;; MOUSE CONFIG

(use-package mwheel
  :ensure nil
  :custom
  (mouse-wheel-progressive-speed nil)
  (mouse-wheel-scroll-amount '(1 ((control) . 5))))

(global-set-key (kbd "C-x m") 'xterm-mouse-mode)

;; enabling mouse by default now
(xterm-mouse-mode 1)


;; SHELL

;; this ensures that the gui path is the same for gui as tui
(use-package exec-path-from-shell
  :demand t
  :if (memq window-system '(mac ns))
  :hook (after-init . exec-path-from-shell-initialize))

(when (f-exists? "~/bin")
  (add-to-list 'exec-path "~/bin"))

(when (f-exists? "~/.local/bin")
  (add-to-list 'exec-path "~/.local/bin"))

(use-package executable
  :ensure nil
  :hook
  ((after-save .
    executable-make-buffer-file-executable-if-script-p)))


;;; REGULAR PACKAGES


;; TODO: investigate hungery delete as a mechanism to prevent kill word from being so aggressive at BOL
(use-package hungry-delete
  :disabled t
  :diminish t
  :hook (after-init . global-hungry-delete-mode)
  :config (setq-default hungry-delete-chars-to-skip " \t\f\v"))

;; (use-package which-key
;;   :diminish which-key-mode
;;   :hook (after-init . which-key-mode))

(use-package which-func
  :defer t
  :hook (prog-mode . which-function-mode))

(use-package restclient)

;; TODO: add keys to the top desc
(use-package highlight-symbol
  :bind
  (:map prog-mode-map
  ("M-o h" . highlight-symbol)
  ("M-p" . highlight-symbol-prev)
  ("M-n" . highlight-symbol-next))
  :config
  (custom-set-faces
   ;; custom-set-faces was added by Custom.
   ;; If you edit it by hand, you could mess it up, so be careful.
   ;; Your init file should contain only one such instance.
   ;; If there is more than one, they won't work right.
   ;; TODO: fix the curretly selected symbol face
   '(highlight-symbol-face
     ((t (:background "color-17" :foreground "black"
                      :weight bold :height 1.3 :family "Sans Serif"))))
  ))


;; nav marks easily - need to map term chars below
(use-package backward-forward
  :disabled t
  ;; :bind
  ;; ("C-," . backward-forward-previous-location)
  ;; ("C-." . backward-forward-next-location)
  :custom
  (mark-ring-max 60)
  :config
  (backward-forward-mode t))


;; turned off for now, because they can annoy me
(use-package highlight-indent-guides
  :disabled t
  :diminish
  :hook
  ((prog-mode yaml-mode) . highlight-indent-guides-mode)
  :custom
  (highlight-indent-guides-auto-enabled t)
  (highlight-indent-guides-responsive t)
  (highlight-indent-guides-method 'character)) ; column

;; better version of linum-mode
(use-package display-line-numbers
  :hook
  ((prog-mode yaml-mode systemd-mode) . display-line-numbers-mode))

;; show vertical line at  80 chars
(use-package fill-column-indicator
  :hook
  ((markdown-mode
    git-commit-mode) . fci-mode))

;; FIXME: neotree vc or somthing in init is overriding color
;; neotree
(use-package neotree
  :bind ("<f8>" . neotree-toggle)
  :demand t
  :init
  (progn
    (defun neotree-project-dir ()
    "Open NeoTree using the git root."
    (interactive)
    (let ((project-dir (projectile-project-root))
          (file-name (buffer-file-name)))
      (neotree-toggle)
      (if project-dir
          (if (neo-global--window-exists-p)
              (progn
                (neotree-dir project-dir)
                (neotree-find file-name)))
        (neotree-toggle))))

    (setq neo-show-updir-line t)
    (setq neo-show-hidden-files t)
    ;; (setq neo-click-changes-root t)
    (setq neo-hide-cursor t)
    ;; every time when the neotree window is opened, it will try to find current
    ;; file and jump to node.
    (setq-default neo-smart-open t)

    ;; set to desired width
    (setq neo-window-width 40)
    ;; (setq neo-window-fixed-size nil)

    (setq neo-theme 'nerd) ; 'classic, 'nerd, 'ascii, 'arrow

    ;; automatically indent to arrow of nerd theme
    (custom-set-variables
     '(neo-auto-indent-point t))

      ;; enable source control state tracking
      (progn
        (setq neo-vc-integration '(face char))

        ;; patch to fix vc integration
        (defun neo-vc-for-node (node)
          (let* ((backend (vc-backend node))
        	 (vc-state (when backend (vc-state node backend))))
            ;; (message "%s %s %s" node backend vc-state)
            ;; set faces for states
            (cons (cdr (assoc vc-state neo-vc-state-char-alist))
        	  (cl-case vc-state
        	    (up-to-date       neo-vc-up-to-date-face)
        	    (edited           neo-vc-edited-face)
        	    (needs-update     neo-vc-needs-update-face)
        	    (needs-merge      neo-vc-needs-merge-face)
        	    (unlocked-changes neo-vc-unlocked-changes-face)
        	    (added            neo-vc-added-face)
        	    (removed          neo-vc-removed-face)
        	    (conflict         neo-vc-conflict-face)
        	    (missing          neo-vc-missing-face)
        	    (ignored          neo-vc-ignored-face)
        	    (unregistered     neo-vc-unregistered-face)
        	    (user             neo-vc-user-face)
        	    (t                neo-vc-default-face))))))))


;; treemacs
(use-package treemacs
  :init
  (with-eval-after-load 'winum
    (define-key winum-keymap (kbd "M-0") #'treemacs-select-window))
  :config
  (progn
    (setq treemacs-be-evil                       nil
          treemacs-collapse-dirs                 (if (executable-find "python3") 3 0)
          treemacs-deferred-git-apply-delay      0.2
          treemacs-display-in-side-window        t
          treemacs-eldoc-display                 t
          treemacs-file-event-delay              1000
          treemacs-max-git-entries               10000
          treemacs-file-follow-delay             10 ;; seconds - slowed down
          treemacs-follow-after-init             t
          treemacs-follow-recenter-distance      0.5
          treemacs-git-command-pipe              ""
          treemacs-goto-tag-strategy             'refetch-index
          treemacs-indentation                   2
          treemacs-indentation-string            " "
          treemacs-is-never-other-window         nil
          treemacs-max-git-entries               5000
          treemacs-missing-project-action        'ask
          treemacs-no-png-images                 nil
          treemacs-no-delete-other-windows       t
          treemacs-project-follow-cleanup        nil
          treemacs-persist-file                  (expand-file-name ".cache/treemacs-persist" user-emacs-directory)
          treemacs-recenter-distance             0.1
          treemacs-recenter-after-file-follow    t
          treemacs-recenter-after-tag-follow     t
          treemacs-recenter-after-project-jump   'always
          treemacs-recenter-after-project-expand 'always
          treemacs-show-cursor                   nil
          treemacs-show-hidden-files             t
          treemacs-silent-filewatch              t
          treemacs-silent-refresh                t
          treemacs-sorting                       'alphabetic-asc ; 'alphabetic-desc
          treemacs-space-between-root-nodes      t
          treemacs-tag-follow-cleanup            t
          treemacs-tag-follow-delay              1.5
          treemacs-width                         35)

    ;; The default width and height of the icons is 22 pixels. If you are
    ;; using a Hi-DPI display, uncomment this to double the icon size.
    (treemacs-resize-icons 16)

    ;; TODO: doesn't seem to support regex. check out.
    ;; (defvar treemacs-custom-dockerfile-icon (all-the-icons-icon-for-file "Dockerfile"))
    ;; (treemacs-define-custom-icon treemacs-custom-dockerfile-icon "Dockerfile.*\\'")


    ;; (treemacs-follow-mode t)
    (treemacs-follow-mode 'tag)
    (treemacs-filewatch-mode t)

    (setq treemacs-use-scope-type 'Perspectives
          treemacs-use-git-mode 'extended
          treemacs-lock-width nil)

    (treemacs-fringe-indicator-mode t)
    (pcase (cons (not (null (executable-find "git")))
                 (not (null (executable-find "python3"))))
      (`(t . t)
       (treemacs-git-mode 'deferred))
      (`(t . _)
       (treemacs-git-mode 'simple)))

    ;; define a function that can turn off fixed width on demand
    ;; FIXME: requires M-x which-key-mode (to be toggled off becuase of bug)
    ;; should be fixed here: https://github.com/Alexander-Miller/treemacs/issues/376
    (defun treemacs-resize-window-unlock ()
      (interactive)
      (with-selected-window (treemacs-get-local-window)
        (progn
          ;; we have to disable/reenable which-key to do this trick.
          (if (bound-and-true-p which-key-mode)
              (progn
                (which-key-mode -1)
                (setq treemacs--width-is-locked nil
                      window-size-fixed nil)
                (which-key-mode 1))
            (setq treemacs--width-is-locked nil
                  window-size-fixed nil))
          )))


    ;; add an advice that unlocks the treemacs fixed window size
    (advice-add 'treemacs :after (lambda (&optional _arg _pred) (treemacs-resize-window-unlock)))


    ;; advise treemacs from the outside to hide it's cursor
    ;; TODO: create a PR for hide/show cursor
    ;; 1. move this internally into treemacs-mode.el & read from 'treemacs-show-cursor
    ;; 2. make a toggle to show/hide cursor on demand from anywhere targeting the window
    (progn
      (defun maybe-hide-treemacs-cursor (&optional _arg pred)
        (interactive)
        (move-beginning-of-line 1)
        (when (and (not treemacs-show-cursor)
                   (string-match-p (regexp-quote "Treemacs-Framebuffer") (buffer-name)))
          (internal-show-cursor (get-buffer-window) nil)))

      (advice-add 'treemacs :after 'maybe-hide-treemacs-cursor))

    ;; allow normal page up/down behavior in treemacs window
    (define-key treemacs-mode-map (kbd "<prior>")   #'scroll-down-command)
    (define-key treemacs-mode-map (kbd "<next>")   #'scroll-up-command)

    ;; single click to expand/collapse
    (define-key treemacs-mode-map [mouse-1] #'treemacs-single-click-expand-action)


    ;; set fonts in tui fallback icons to my liking
    ;; NOTE: these are originally defined in treemacs-icons.el
    (when *is-in-terminal*
      (treemacs-create-icon :file "dir-closed.png"  :extensions (dir-closed) :fallback (propertize "▸ " 'face 'treemacs-term-node-face))
      (treemacs-create-icon :file "dir-open.png"    :extensions (dir-open)   :fallback (propertize "▾ " 'face 'treemacs-term-node-face))
      (treemacs-create-icon :file "tags-leaf.png"   :extensions (tag-leaf)   :fallback (propertize "• " 'face 'font-lock-constant-face))
      (treemacs-create-icon :file "tags-open.png"   :extensions (tag-open)   :fallback (propertize "▸ " 'face 'font-lock-string-face))
      (treemacs-create-icon :file "tags-closed.png" :extensions (tag-closed) :fallback (propertize "▾ " 'face 'font-lock-string-face)))

    ;; setup any custom icons here
    (defvar treemacs-custom-dockerfile-icon (all-the-icons-icon-for-file "Dockerfile"))
    (treemacs-define-custom-icon treemacs-custom-dockerfile-icon
                                 "DockerfileLocal"
                                 "DockerfileLocalCloud"
                                 "DockerfileWorker"
                                 "DockerfileWorkerLocal"
                                 "DockerfileWorkerLocalCloud")) ;; wildcards don't work only explicit, usually only file extensions
  :bind
  (:map global-map
        ("M-0"       . treemacs-select-window)
        ("C-x t 1"   . treemacs-delete-other-windows)
        ([f9]        . treemacs)
        ("C-x t B"   . treemacs-bookmark)
        ("C-x t C-t" . treemacs-find-file)
        ("C-x t M-t" . treemacs-find-tag)))


(use-package treemacs-projectile
  :after (treemacs projectile))

;; (use-package treemacs-icons-dired
;;   :after (treemacs dired)
;;   :config (treemacs-icons-dired-mode))

(use-package treemacs-magit
  :after (treemacs magit))

;; on the fly syntax checking
(use-package flycheck
  :diminish flycheck-mode
  :config
  (global-flycheck-mode t))

;; snippets and snippet expansion
;; NOTE: creating custom snippets: https://simpleit.rocks/lisp/emacs/adding-custom-snippets-to-yasnippet/
;; (setq yas-snippet-dirs
;;       '("~/.emacs.d/snippets"                    ;; personal snippets
;;         ;; "/path/to/some/collection/"           ;; foo-mode and bar-mode snippet collection
;;         ;; "/path/to/yasnippet/yasmate/snippets" ;; the yasmate collection
;;         ))


(use-package helm-c-yasnippet
  :after (helm yasnippet)
  :bind
  (("C-c y" . helm-yas-complete)))

;; NOTE: read this: https://jaketrent.com/post/code-snippets-spacemacs/
(use-package yasnippet
  :after (no-littering)
  :init
  (add-hook 'prog-mode-hook #'yas-minor-mode)
  :config
  (define-key yas-minor-mode-map "\C-j" 'yas-expand)
  (define-key yas-keymap "\C-j" 'yas-next-field-or-maybe-expand)
  (dolist (keymap (list yas-minor-mode-map yas-keymap))
    (define-key keymap (kbd "TAB") nil)
    (define-key keymap [(tab)] nil)))

(use-package yasnippet-snippets
  :after yasnippet)



;; https://github.com/abo-abo/hydra/blob/master/hydra-examples.el
(use-package hydra
  :config
  (progn
    "Hydra helpers"

    ;; resize windows

    (defun hydra-move-splitter-left (arg)
      "Move window splitter left."
      (interactive "p")
      (if (let ((windmove-wrap-around))
            (windmove-find-other-window 'right))
          (shrink-window-horizontally arg)
        (enlarge-window-horizontally arg)))

    (defun hydra-move-splitter-right (arg)
      "Move window splitter right."
      (interactive "p")
      (if (let ((windmove-wrap-around))
            (windmove-find-other-window 'right))
          (enlarge-window-horizontally arg)
        (shrink-window-horizontally arg)))

    (defun hydra-move-splitter-up (arg)
      "Move window splitter up."
      (interactive "p")
      (if (let ((windmove-wrap-around))
            (windmove-find-other-window 'up))
          (enlarge-window arg)
        (shrink-window arg)))

    (defun hydra-move-splitter-down (arg)
      "Move window splitter down."
      (interactive "p")
      (if (let ((windmove-wrap-around))
            (windmove-find-other-window 'up))
          (shrink-window arg)
        (enlarge-window arg))))

  (with-eval-after-load 'hydra

    ;; window size management
    (defhydra hydra-splitter (global-map "C-M-s")
      "Splitter adjustment for current window"
      ("h" hydra-move-splitter-left "Shrink horizontally")
      ("j" hydra-move-splitter-down "Grow vertically")
      ("k" hydra-move-splitter-up "Shrink vertically")
      ("l" hydra-move-splitter-right "Grow horizontally"))

    ;; better window management - maybe replace above
    (defhydra hydra-cycle-windows
      (global-map "C-x o")
      "Window Management"
      ("n" (other-window 1) "Next")
      ("p" (other-window -1) "Previous")
      ("]" enlarge-window-horizontally "Enlarge horizontal")
      ("[" shrink-window-horizontally "Shrink horizontal")
      ("=" enlarge-window "Enlarge vertival")
      ("-" shrink-window "Shrink vertical")
      ("b" balance-windows "Balance windows")
      ("c" delete-window "Close window")
      ("d" delete-other-windows "Maximize window")
      ("q" nil "quit"))))


(use-package multiple-cursors
  :defer 2
  :config
  (progn

    ;; TODO: try using mouse to place arbitrary cursors
    ;; https://stackoverflow.com/questions/39882624/setting-arbitrary-cursor-positions-with-multiple-cursors-in-emacs
    ;; (global-set-key (kbd "C-S-<mouse-1>") 'mc/add-cursor-on-click)

    ;; From active region to multiple cursors:
    (general-define-key
     :prefix "C-c m"
     "r" 'set-rectangular-region-anchor
     "c" 'mc/edit-lines
     "e" 'mc/edit-ends-of-lines
     "a" 'mc/edit-beginnings-of-lines))
    )



;; simpler usage of mc
;; (use-package multiple-cursors
;;   :bind
;;   (("C-c n" . mc/mark-next-like-this)
;;    ("C-c p" . mc/mark-previous-like-this)))

;; TODO: rework this.  get a better solution for MC
;; (use-package multiple-cursors
;;   :functions hydra-multiple-cursors
;;   :bind (("M-u" . hydra-multiple-cursors/body)
;;          ;; we are overriding the mail default creation command here
;;          ("C-x m" . mc/edit-lines)
;;          ;; use the mouse to place a cursor - only works in gui bode because C-S
;;          ("C-S-<mouse-1>" . mc/add-cursor-on-click))
;;   :config
;;   (with-eval-after-load 'hydra
;;     ;; https://ladicle.com/post/config/
;;     (defhydra hydra-multiple-cursors (:color pink :hint nil)
;;       "
;;                                                                         ╔════════╗
;;     Point^^^^^^             Misc^^            Insert                            ║ Cursor ║
;;   ──────────────────────────────────────────────────────────────────────╨────────╜
;;      _k_    _K_    _M-k_    [_l_] edit lines  [_a_] letters
;;      ^↑^    ^↑^     ^↑^     [_m_] mark all    [_n_] numbers
;;     mark^^ skip^^^ un-mk^   [_s_] search
;;      ^↓^    ^↓^     ^↓^
;;      _j_    _J_    _M-j_
;;   ╭──────────────────────────────────────────────────────────────────────────────╯
;;                            [_q_]: quit, [Click]: point
;; "
;;       ("l" mc/edit-lines :exit t)
;;       ("m" mc/mark-all-like-this :exit t)
;;       ("j" mc/mark-next-like-this)
;;       ("J" mc/skip-to-next-like-this)
;;       ("M-j" mc/unmark-next-like-this)
;;       ("k" mc/mark-previous-like-this)
;;       ("K" mc/skip-to-previous-like-this)
;;       ("M-k" mc/unmark-previous-like-this)
;;       ("s" mc/mark-all-in-region-regexp :exit t)
;;       ("a" mc/insert-letters :exit t)
;;       ("n" mc/insert-numbers :exit t)
;;       ("<mouse-1>" mc/add-cursor-on-click)
;;       ;; Help with click recognition in this hydra
;;       ("<down-mouse-1>" ignore)
;;       ("<drag-mouse-1>" ignore)
;;       ("q" nil))))


;; NOTE: for emacs in iterm2 add this escape sequence keyboard binding - ^; => [59;5u
(use-package iedit
  :commands (iedit-mode)
  :bind* (("C-;" . iedit-mode))
  ;; set the face manually since my current theme doesn't show well
  :custom-face
  (iedit-occurrence-face ((t (:background "magenta" :foreground "black")))))

;; move where i mean
(use-package mwim
  :bind
  ("C-a" . mwim-beginning-of-code-or-line)
  ("C-e" . mwim-end-of-code-or-line))

(use-package ws-butler
  :hook (prog-mode . ws-butler-mode)
  :diminish ws-butler-mode)

(use-package subword
  :init
  (progn
    (global-subword-mode)))

(use-package volatile-highlights
  :hook
  (after-init . volatile-highlights-mode))

(use-package smex
  :config
  (smex-initialize))

(use-package flx-ido)

;;;; vlf
;; View Large Files
(use-package vlf
  :init
  (setq vlf-application 'dont-ask)   ;just do it
  (setq vlf-batch-size 8192))        ;a bit more text per batch please

(use-package highlight-indentation
  :disabled t
  :hook
  (prog-mode . highlight-indentation-current-column-mode))


;;; IVY - demanded in order to work quickly

(use-package ivy
  :demand t
  :diminish ivy-mode
  :config
  (progn
    ;; Create and delete a view
    ;; (global-set-key (kbd "C-c v") 'ivy-push-view)
    ;; (global-set-key (kbd "C-c V") 'ivy-pop-view)

    (ivy-mode 1))
  :init
  (progn
    ;; enable switching to the previously opened file buffers
    (setq ivy-use-virtual-buffers t)

    (setq ivy-display-style 'fancy
          ivy-count-format "(%d/%d) " ; this is what you want
          ivy-initial-inputs-alist nil ; remove initial ^ input.
          ivy-extra-directories nil) ; remove . and .. directory.

    (setq ivy-virtual-abbreviate 'abbreviate)
    (setq ivy-count-format "(%d/%d) ")
    (setq ivy-use-selectable-prompt t)))


(use-package ivy-xref
  :defer 1
  :init
  (setq xref-show-xrefs-function #'ivy-xref-show-xrefs))

(use-package counsel
  :defer 1
  :config
  (progn
    (global-set-key (kbd "M-x") 'counsel-M-x)
    ;; (global-set-key (kbd "C-x C-d") 'counsel-dired) ;; doing this because can't figure out how to set `dired-jump' to default action for `list-directories'
    ;; (global-set-key (kbd "C-x C-f") 'counsel-find-file)
    ))

(use-package swiper
  :defer 1
  :config
  (progn

    ;; default to thing-at-point when swiping
    ;; https://github.com/abo-abo/swiper/issues/1068
    (defun ivy-with-thing-at-point (cmd)
      (let ((ivy-initial-inputs-alist
             (list
              (cons cmd (thing-at-point 'symbol)))))
        (funcall cmd)))

    (defun swiper-thing-at-point ()
      (interactive)
      (ivy-with-thing-at-point 'swiper))

    ;; NOTE: swap with helm-swoop/helm-resume commands by commenting out these and commenting in helm's
    ;; (global-set-key "\C-s" 'swiper-thing-at-point)
    ;; ;; using ivy-resume, which will bring up last swipe session
    ;; (global-set-key (kbd "C-r") 'ivy-resume)

    (ivy-mode 1))


  ;; extend swiper to insert word/symbol at point
  (define-key swiper-map (kbd "C-.")
    (lambda () (interactive) (insert (format "\\<%s\\>" (with-ivy-window (thing-at-point 'symbol))))))
  (define-key swiper-map (kbd "M-.")
    (lambda () (interactive) (insert (format "\\<%s\\>" (with-ivy-window (thing-at-point 'word)))))))

(use-package projectile
  :demand t ;; because tabbar loads after - speed up
  :delight '(:eval (concat " " (projectile-project-name)))
  :config
  (progn

    (projectile-global-mode)

    ;; NOTE: moved to helm section, since controlling it there
    ;; (projectile-switch-project-action 'projectile-find-file)
    ;; (setq projectile-switch-project-action 'projectile-find-dir)

    ;; TODO: reorder these to not use eval for speed
    (with-eval-after-load 'ivy
      '(ivy-format-functions-alist))

    (add-hook 'prog-mode-hook 'projectile-mode)
    (setq projectile-enable-caching t)
    ;; (setq projectile-completion-system 'ivy)

    ;; (setq projectile-switch-project-action 'neotree-projectile-action)
    ;; search superordinate dirs until root
    ;; (setq projectile-find-dir-includes-top-level t)
    (define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)

    ;; use ag instread of grep for helm-projectile-grep (REQUIRES AG)
    ;; (define-advice helm-projectile-grep (:override (&optional dir) ag)
    ;;   (helm-do-ag (or dir (projectile-project-root))))

    ;; globally ignore certain directory and file patterns

    (add-to-list 'projectile-globally-ignored-directories "**/_build/")
    (add-to-list 'projectile-globally-ignored-directories "**/deps/")
    (add-to-list 'projectile-globally-ignored-directories "**/node_modules/")
    (add-to-list 'projectile-globally-ignored-directories "**/thirdparty/")
    (add-to-list 'projectile-globally-ignored-directories "**/build/")
    (add-to-list 'projectile-globally-ignored-directories "**/log/")
    (add-to-list 'projectile-globally-ignored-directories "**/logs/")

    (add-to-list 'projectile-globally-ignored-files "*.min.js")

    ;; ignore certain directory and file patterns during grepping

    (add-to-list 'grep-find-ignored-directories "**/_build/")
    (add-to-list 'grep-find-ignored-directories "**/deps/")
    (add-to-list 'grep-find-ignored-directories "**/node_modules/")
    (add-to-list 'grep-find-ignored-directories "**/build/")
    (add-to-list 'grep-find-ignored-directories "**/thirdparty/")
    (add-to-list 'grep-find-ignored-directories "**/log/")
    (add-to-list 'grep-find-ignored-directories "**/logs/")
    (add-to-list 'grep-find-ignored-files "**/*.log")))


;; C-c p h to switch to it
(use-package helm-projectile
  :defer 1
  :after (helm projectile)
  :config
  (progn
    (setq projectile-completion-system 'helm)

    ;; NOTE: this causes all C-c p s [r, g, s] commands to use helm
    (helm-projectile-on)))

;; ELASTICSEARCH
(use-package es-mode
  :ensure t
  :mode ("\\.es\\'" . es-mode)
  :bind (("C-c C-f" . json-pretty-print)))

(use-package ob-elasticsearch
  :ensure nil
  :after (org es-mode)
  :init
  (add-to-list 'org-babel-load-languages '(elasticsearch . t)))

;; JQ
;; requires jq binary to be installed and in $PATH
(use-package jq-mode
  :mode "\\.jq\\'")

;;; GIT

(use-package gitconfig-mode)
(use-package gitignore-mode)
(use-package gitattributes-mode)
(use-package git-link)

;; requires github acct
(use-package gist)

(use-package git-timemachine
  :bind ("M-g t" . git-timemachine-toggle))


;; Show a small popup with the blame for the current line only.
;; https://github.com/syohex/emacs-git-messenger
(use-package git-messenger
  :ensure t
  ;; :bind ("C-x v p" . git-messenger:popup-message)
  :bind (:map vc-prefix-map
         ("p" . git-messenger:popup-message))
  :init
  (setq git-messenger:show-detail t
        git-messenger:use-magit-popup t)
  :config
  (progn
    (define-key git-messenger-map (kbd "RET") 'git-messenger:popup-close)))

(use-package magit-filenotify
  :after (magit)
  :defer 1
  :ensure t
  :config
  (progn
    (when (fboundp 'file-notify-add-watch)
      ;; NOTE: in the magit :config we disable this when killing magit buffers
      (add-hook 'magit-status-mode-hook 'magit-filenotify-mode))))


;; TODO: get gud by reading this: http://endlessparentheses.com/it-s-magit-and-you-re-the-magician.html
(use-package magit
  :bind ("C-x g" . magit-status)
  :config (progn
     (defun run-projectile-invalidate-cache (&rest _args)
       ;; We ignore the args to `magit-checkout'.
       (projectile-invalidate-cache nil))
     (advice-add 'magit-checkout
       :after #'run-projectile-invalidate-cache)
     (advice-add 'magit-branch-and-checkout ; This is `b c'.
       :after #'run-projectile-invalidate-cache)
     (setq magit-completing-read-function 'ivy-completing-read)

     ;; clean up all magit buffers after using it
     (defun my-magit-kill-buffers ()
       "Restore window configuration and kill all Magit buffers."
       (interactive)
       (let ((buffers (magit-mode-get-buffers)))
         (magit-filenotify-mode -1) ; turn off magit-filenotify before closing the buffer
         (magit-restore-window-configuration)
         (mapc #'kill-buffer buffers)))

     (bind-key "q" #'my-magit-kill-buffers magit-status-mode-map)

     ;; possible performance issues with this hook.
     ;; be on the lookout with magit for performance issues
     (add-hook 'after-save-hook 'magit-after-save-refresh-status t)


     ;; gerrit extension to push to frequently used refspec
     (defun magit-push-to-gerrit ()
       (interactive)
       (magit-push-refspecs "origin" "HEAD:refs/for/master" '()))

     ;; add a menu item transient to the push context menu
     (transient-append-suffix
      'magit-push
      "m"
      '("!" "Push to Gerrit" magit-push-to-gerrit))

     ;; git submodule update --init --recursive
     (defun magit-submodule-update-all ()
       "Update all submodules"
       :description "Update All (git submodule update --init --recursive)"
       (interactive)
       (magit-with-toplevel
         (magit-run-git-async "submodule" "update" "--init" "--recursive")))

     ;; git submodule update -f --init --recursive
     (defun magit-submodule-update-all-force ()
       "Update all submodules"
       :description "Update All (git submodule update -f --init --recursive)"
       (interactive)
       (magit-with-toplevel
         (magit-run-git-async "submodule" "update" "-f" "--init" "--recursive")))

     ;; add a menu item transient to submodule context menu
     (transient-append-suffix
      'magit-submodule
      "u"
      '("U" "submodule update recursive" magit-submodule-update-all-force))


     (custom-set-faces
      ;; NOTE: use M-x describe-face to see the style of any face
      ;; other faces
      '(magit-diff-hunk-heading ((((type tty)) (:foreground "magenta"))))
      '(magit-diff-hunk-heading-highlight ((((type tty)) (:background "blue" :foreground "brightyellow"))))
      '(magit-diff-hunk-heading-selection ((((type tty)) (:background "blue" :foreground "magenta"))))
      '(magit-diff-added ((((type tty)) (:foreground "green"))))
      '(magit-diff-added-highlight ((((type tty)) (:foreground "brightgreen"))))
      '(magit-diff-context-highlight ((((type tty)) (:foreground "default"))))
      '(magit-diff-file-heading ((((type tty)) nil)))
      '(magit-diff-removed ((((type tty)) (:background nil :foreground "red"))))
      '(magit-diff-removed-highlight ((((type tty)) (:background nil :foreground "brightred"))))
      '(magit-section-highlight ((((type tty)) nil))))))

;; NOTE: using for helm buffers right now
;; ;; TODO: have cycle within current group
;; (use-package nswbuff
;;   :defer 1
;;   :after (projectile)
;;   :commands (nswbuff-switch-to-previous-buffer
;;              nswbuff-switch-to-next-buffer)
;;   :config
;;   (progn
;;     (my/global-map-and-set-key "C-TAB" 'nswbuff-switch-to-previous-buffer)
;;     (my/global-map-and-set-key "C-S-TAB" 'nswbuff-switch-to-next-buffer))
;;   :init
;;   (setq nswbuff-clear-delay 0 ; set this to 2 (seconds), if you want to see the status window
;;         nswbuff-recent-buffers-first t
;;         nswbuff-display-intermediate-buffers t
;;         nswbuff-exclude-buffer-regexps '("^ "
;;                                          "^\*.*\*"
;;                                          "^magit.*:.+")
;;         nswbuff-include-buffer-regexps '("^*Org Src")
;;         nswbuff-start-with-current-centered t
;;         nswbuff-buffer-list-function '(lambda ()
;;                                         (interactive)
;;                                         (if (projectile-project-p)
;;                                             (nswbuff-projectile-buffer-list)
;;                                           (buffer-list)))))


;; ;; view, stage and revert Git changes straight from the buffer.
;; (use-package git-gutter+
;;   :defer 1
;;   :config
;;   (progn
;;     (when (display-graphic-p)
;;       (use-package git-gutter-fringe
;;         :ensure t))

;;     (global-git-gutter+-mode t)
;;     (setq-default fringes-outside-margins t)
;;     (setq indicate-empty-lines nil)
;;     (setq git-gutter+-lighter ""
;;           git-gutter+-handled-backends '(git hg bzr svn))

;;     ;; (set-face-foreground 'git-gutter+-added    "green")
;;     ;; (set-face-foreground 'git-gutter+-deleted  "black")
;;     ;; (set-face-background 'git-gutter+-modified "purple")
;;     ))

;; NOTE: preferring plain old git-gutter for now because just want state reflected
(use-package git-gutter
  :defer 1
  :config
  (progn
    (if (display-graphic-p)
        (use-package git-gutter-fringe
          :ensure t))

    (global-git-gutter-mode t)
    (setq-default fringes-outside-margins t)
    (setq indicate-empty-lines nil)
    (setq git-gutter:lighter ""
          git-gutter:handled-backends '(git hg bzr svn))
    (set-face-foreground 'git-gutter:modified "purple")
    (set-face-foreground 'git-gutter:added "green")
    (set-face-foreground 'git-gutter:deleted "red")

    ;; update view state when magit refreshes
    ;; e.g. stage/unstage
    (add-hook 'magit-post-refresh-hook
              #'git-gutter:update-all-windows)

    (with-eval-after-load 'hydra

      (defhydra hydra-git-gutter (global-map "C-c")
	"
Git gutter:
  _j_: next hunk        _s_tage hunk     _q_uit
  _k_: previous hunk    _r_evert hunk    _Q_uit and deactivate git-gutter
  ^ ^                   _p_opup hunk
  _h_: first hunk
  _l_: last hunk        set start _R_evision
"
	("j" git-gutter:next-hunk)
	("k" git-gutter:previous-hunk)
	("h" (progn (goto-char (point-min))
		    (git-gutter:next-hunk 1)))
	("l" (progn (goto-char (point-min))
		    (git-gutter:previous-hunk 1)))
	("s" git-gutter:stage-hunk)
	("r" git-gutter:revert-hunk)
	("p" git-gutter:popup-hunk)
	("R" git-gutter:set-start-revision)
	("q" nil :color blue)
	("Q" (progn (git-gutter-mode -1)
		    ;; git-gutter-fringe doesn't seem to
		    ;; clear the markup right away
		    (sit-for 0.1)
		    (git-gutter:clear))
	 :color blue))))
  )


;; smerge
;; TODO: create a binding for smerge hydra
(use-package smerge-mode
  :preface
  (with-eval-after-load 'hydra
    (defhydra smerge-hydra
      (:color pink :hint nil :post (smerge-auto-leave))
      "
^Move^       ^Keep^               ^Diff^                 ^Other^
^^-----------^^-------------------^^---------------------^^-------
_n_ext       _b_ase               _<_: upper/base        _C_ombine
_p_rev       _u_pper              _=_: upper/lower       _r_esolve
^^           _l_ower              _>_: base/lower        _k_ill current
^^           _a_ll                _R_efine
^^           _RET_: current       _E_diff
"
      ("n" smerge-next)
      ("p" smerge-prev)
      ("b" smerge-keep-base)
      ("u" smerge-keep-upper)
      ("l" smerge-keep-lower)
      ("a" smerge-keep-all)
      ("RET" smerge-keep-current)
      ("\C-m" smerge-keep-current)
      ("<" smerge-diff-base-upper)
      ("=" smerge-diff-upper-lower)
      (">" smerge-diff-base-lower)
      ("R" smerge-refine)
      ("E" smerge-ediff)
      ("C" smerge-combine-with-next)
      ("r" smerge-resolve)
      ("k" smerge-kill-current)
      ("ZZ" (lambda ()
              (interactive)
              (save-buffer)
              (bury-buffer))
       "Save and bury buffer" :color blue)
      ("q" nil "cancel" :color blue)))
  :hook ((find-file . (lambda ()
                        (save-excursion
                          (goto-char (point-min))
                          (when (re-search-forward "^<<<<<<< " nil t)
                            (smerge-mode 1)))))
         (magit-diff-visit-file . (lambda ()
                                    (when smerge-mode
                                      (smerge-hydra/body))))))


(use-package ediff-wind
  :ensure nil
  :custom
  (ediff-split-window-function #'split-window-horizontally)
  (ediff-window-setup-function #'ediff-setup-windows-plain))

;;; GREPPERY

;; requires silversearcher install: https://github.com/ggreer/the_silver_searcher#insta
(use-package ag
  :init
  (setq ag-arguments (list "--word-regexp" "--smart-case")))


;; requires ripgrep install: apt-get install ripgrep
(use-package deadgrep
  :bind ("<f5>" . deadgrep))

;; nec for nulear editing
(use-package wgrep)

(use-package ripgrep)

;;; HELM
;; TODO: convert to use tabs like ivy for completions
(use-package helm
  ;; :after (helm-swoop)
  :init
  (progn
    (require 'helm)
    (require 'helm-config)
    (require 'helm-mode)

    ;; these vars allow helm-swoop to not take entire window when used with neotree or treemacs
    (setq helm-swoop-split-with-multiple-windows t
          helm-swoop-split-direction 'split-window-vertically
          helm-swoop-split-window-function 'helm-default-display-buffer))
  :config
  (progn
    ;; open helm buffer inside current window, not occupy whole other window
    (setq helm-split-window-in-side-p t)
    ;; set helm buffer filename column width wide enough to see long filenames without ...
    ;; use nil to use the longest buffername found or something like 40 to constrain
    (setq helm-buffer-max-length nil)
    (defun spacemacs//helm-hide-minibuffer-maybe ()
      (when (with-helm-buffer helm-echo-input-in-header-line)
	(let ((ov (make-overlay (point-min) (point-max) nil nil t)))
	  (overlay-put ov 'window (selected-window))
	  (overlay-put ov 'face
		       (let ((bg-color (face-background 'default nil)))
			 `(:background ,bg-color :foreground ,bg-color)))
	  (setq-local cursor-type nil))))

    ;; set helm height
    (add-hook 'helm-minibuffer-set-up-hook
	      'spacemacs//helm-hide-minibuffer-maybe)
    (setq helm-autoresize-max-height 0)
    (setq helm-autoresize-min-height 40)
    (helm-autoresize-mode 1)




    ;; (require 'helm-config)
    (setq helm-candidate-number-limit 100)
    ;; From https://gist.github.com/antifuchs/9238468
    (setq helm-idle-delay 0.0 ; update fast sources immediately (doesn't).
          helm-input-idle-delay 0.01  ; this actually updates things
                                        ; reeeelatively quickly.
          helm-yas-display-key-on-candidate t
          helm-quick-update t
          helm-M-x-requires-pattern nil
          helm-ff-skip-boring-files t)

    ;; what sources will we show in helm-mini
    (setq helm-mini-default-sources
	  '(helm-source-buffers-list
	    ;; helm-source-bookmarks
	    helm-source-recentf
	    helm-source-buffer-not-found))

    ;; suppress headerline
    (setq helm-display-header-line nil) ;; t by default
    (setq helm-echo-input-in-header-line t)

    ;; suppress modline while in helm
    (progn
      ;; 1. Collect bottom buffers
      (defvar bottom-buffers nil
    	"List of bottom buffers before helm session.
         Its element is a pair of `buffer-name' and `mode-line-format'.")

      (defun bottom-buffers-init ()
    	(when bottom-buffers
    	  (bottom-buffers-show-mode-line))
    	(setq bottom-buffers
    	      (cl-loop for w in (window-list)
    		       when (window-at-side-p w 'bottom)
    		       collect (with-current-buffer (window-buffer w)
    				 (cons (buffer-name) mode-line-format)))))

      (add-hook 'helm-before-initialize-hook #'bottom-buffers-init)
      ;; 2. Hide mode line
      (defun bottom-buffers-hide-mode-line ()
    	(mapc (lambda (elt)
    		(with-current-buffer (car elt)
    		  (setq-local mode-line-format nil)))
    	      bottom-buffers))

      (add-hook 'helm-after-initialize-hook #'bottom-buffers-hide-mode-line)

      ;; 3. Restore mode line
      (defun bottom-buffers-show-mode-line ()
    	(when bottom-buffers
    	  (mapc (lambda (elt)
    		  (with-current-buffer (car elt)
    		    (setq-local mode-line-format (cdr elt))))
    		bottom-buffers)
    	  (setq bottom-buffers nil)))

      (add-hook 'helm-exit-minibuffer-hook #'bottom-buffers-show-mode-line)

      (defun helm-keyboard-quit-advice (orig-func &rest args)
    	(bottom-buffers-show-mode-line)
    	(apply orig-func args))

      (advice-add 'helm-keyboard-quit :around #'helm-keyboard-quit-advice))


    ;; make TAB behave like completion key for helm by
    ;; defining reversed keymaps for tab while in helm
    (define-key helm-map (kbd "TAB") #'helm-execute-persistent-action)
    (define-key helm-map (kbd "<tab>") #'helm-execute-persistent-action)
    (define-key helm-map (kbd "C-SPC") #'helm-select-action)

    ;; define a wrapper to use so helm can be project aware when finding files
    (defun smart-helm-for-files ()
      "Call `helm-projectile' if `projectile-project-p', otherwise fallback to `helm-for-files'."
      (interactive)
      (if (projectile-project-p)
          (helm-projectile-find-file-dwim)
        (helm-find-files)))

    ;; define the preferred order files are shown in
    (setq helm-for-files-preferred-list
          '(
            helm-source-projectile-files-list
            ;; helm-source-projectile-buffers-list
            helm-source-projectile-recentf-list
            helm-source-buffers-list
            helm-source-recentf
            helm-source-projectile-directories-list
            helm-source-projectile-projects
            ;; helm-source-bookmarks
            ;; helm-source-file-cache
            helm-source-files-in-current-dir
            helm-source-moccur
            helm-source-locate
            ))

    ;; use helm to find files for projects instead
    (define-key global-map [remap projectile-find-file] 'helm-projectile-find-file-dwim)

    ;; use helm-projectile when switching into a projectile project
    (setq projectile-switch-project-action 'helm-projectile)

    ;; helm view toggle functions

    (defun my-toggle-helm (helm-view-fn &optional use-fast-selection)
      "Toggles any view from helm. The passed callback must be interactive."
      (if helm-alive-p
          (if use-fast-selection
              ;; select without explicit RET
              (progn
                ;; makes it sticky, only enter of C-g will quit
                ;; (setq quit-flag t)
                ;; allows toggle but selection can only be done via RET
                ;; (setq helm-quit t)

                ;; these make it fully dynamic
                (helm-kill-async-processes)
                (helm-restore-position-on-quit)
                (helm-exit-minibuffer))
            ;; standard selection behavior
            ;; same as setting helm-quit t
            (helm-keyboard-quit))
          (funcall helm-view-fn)))


    ;; NOTE: this fn ensures that helm doesn't change the cursor when we exit
    (defun helm-keyboard-quit-advice (orig-func &rest args)
      "Reset the cursor-type back to what it was after quitting helm"
      (interactive)
      (deferred:$
        (deferred:wait 3) ; msec
        (deferred:nextc it
          (lambda (x)
            (when (boundp 'cursor-type)
              (setq cursor-type cursor-type)))))
      (apply orig-func args))

    ;; advising all these functions ensures we cover all cases
    (advice-add 'helm-cleanup :around #'helm-keyboard-quit-advice)
    (advice-add 'keyboard-escape-quit :around #'helm-keyboard-quit-advice)
    (advice-add 'keyboard-quit :around #'helm-keyboard-quit-advice)



    (defun my-toggle-helm-buffers-list ()
      "Toggles helm-buffers-list"
      (interactive)
      (my-toggle-helm 'helm-buffers-list nil))

    ;; NOTE: using C-TAB for helm-buffers for now
    (if *is-in-terminal*
        (my/global-map-and-set-key "C-TAB" 'my-toggle-helm-buffers-list)
      (progn (define-key helm-map (kbd "C-<tab>") 'my-toggle-helm-buffers-list)
       (bind-key* (kbd "C-<tab>") 'my-toggle-helm-buffers-list)))


    ;; Note: THIS IS WHAT ALLOWS HELM TO OVERRIDE IVY FOR FIND-FILE
    ;; it must be loaded after (ivy-mode 1)
    (helm-mode 1)

    )


  :bind (("C-c h" . helm-mini)
         ("C-h a" . helm-apropos)
         ("C-x C-f" . helm-find-files) ;; this gets used by ivy in my config
         ;; ("C-x C-f" . smart-helm-for-files) ;; use find files in project if in project context
         ("C-x c m" . helm-mini)
         ("C-x c o" . helm-imenu)

         ;; handle dired use case
         ;; ("C-x C-d" . ranger)
         ("C-x C-d" . dired)

         ("C-x C-b" . helm-buffers-list)
         ;;("C-x b" . helm-buffers-list) ;; belongs to ivy
         ("M-y" . helm-show-kill-ring)
         ;; ("M-x" . helm-M-x) ;; belongs to counsel
         ("M-s o" . helm-occur)

         ;; NOTE: swap with swiper/ivy-resume commands by commenting out these and commenting in swiper's
         ;; swoop pre input: https://github.com/emacsorphanage/helm-swoop#configure-pre-input-search-query
         ("C-s" . helm-swoop-without-pre-input)
         ;; ("C-S-s" . helm-swoop)
         ("C-r" . helm-resume)

         ("C-c y" . helm-yas-complete)
         ("C-c Y" . helm-yas-create-snippet-on-region)
         ("C-h SPC" . helm-all-mark-rings)
         ("M-r"   . helm-recentf)))

;; (use-package swiper-helm
;;   :ensure t
;;   :bind ("C-s" . swiper-helm))

(use-package helm-descbinds
  :after (helm)
  :commands helm-descbinds
  :config
  (global-set-key (kbd "C-h b") 'helm-descbinds))

(use-package helm-make
  :bind
  (("C-c K" . helm-make)))

(use-package helm-flx
  :after helm
  :config
  (helm-flx-mode +1))

(use-package helm-fuzzier
  :after helm
  :config
  (helm-fuzzier-mode 1))



;; causes xref-find-references and xref-find-definitions to use helm
;; force load!
(use-package helm-xref
  :defer 1
  :config
  (progn

    (if (>= emacs-major-version 27)
        (progn
          (setq xref-show-definitions-function #'helm-xref-show-defs-27)
          (setq xref-show-xrefs-function 'helm-xref-show-xrefs-27)) ; NOTE: this fn name may change soon
      (progn
        (setq xref-show-xrefs-function 'helm-xref-show-xrefs)
        (setq xref-show-definitions-function 'helm-xref-show-xrefs)))

    ;; (setq helm-xref-candidate-formatting-function 'helm-xref-format-candidate-long)

    (defun helm-xref-format-candidate-project (file line summary)
      "Build short form of candidate format with FILE, LINE, and SUMMARY."
      (concat
       (propertize

        ;; strategy 1: strip off prefix from root to project root
        (if (projectile-project-p)
            (replace-regexp-in-string (regexp-quote (projectile-project-root)) ""
                                      file)
          file)

        ;; strategy 2: infer based on prior project file, e.g. Jenkinsfile, .gitignore
        ;; (let ((fname (buffer-file-name)))
        ;;   (file-relative-name fname (locate-dominating-file fname "README.md")))

        'font-lock-face 'helm-xref-file-name)
       (when (string= "integer" (type-of line))
         (concat
          ":"
          (propertize (int-to-string line)
                      'font-lock-face 'helm-xref-line-number)))
       ":"
       summary))

    (setq helm-xref-candidate-formatting-function 'helm-xref-format-candidate-project)

    ))


(use-package helm-swoop
  :defer 1
  :config
  (progn
    ;; jump back to place we were before swooping
    (global-set-key (kbd "M-i") 'helm-swoop-back-to-last-point)

    ;; NOTE: we are setting a mark in the xref ring from swooping
    ;; if this behavior becomes problematic, just disable this line

    ;; get a marker ref for current point
    ;; (xref-push-marker-stack (copy-marker (point)))

    (add-hook 'helm-swoop-before-goto-line-action-hook 'xref-push-marker-stack)
))

(use-package ag
  :ensure-system-package ag)

(use-package helm-ag
  :after (helm)
  :defer 1
  :config
  (progn
    (setq
     helm-ag-use-agignore t
     ;; fixex ansi codes in helm-do-ag output
     helm-ag-base-command "ag --nocolor --nogroup -p ~/.agignore"
     ;; helm-ag-command-option "--all-text"
     helm-ag-insert-at-point 'symbol
     helm-ag-ignore-buffer-patterns '("\\.log\\'" "\\.pem\\'"))

    ;; use ~/.agignore in home directory when filtering results.
    (setq helm-ag-command-option " -U" )))


(use-package rg
  :ensure-system-package rg)


;; using my modified version that shows files relative to projectile project
(use-package helm-rg
  :after helm
  :ensure nil
  :defer 1
  :quelpa (helm-rg :fetcher github :repo "jclosure/helm-rg"))


;; dumb-jump uses ag for jumps if avail, else grep
;; must be latest ver for newer lang support
(use-package dumb-jump
  ;; smart-jump owns these..
  ;; :bind (("M-." . dumb-jump-go)
  ;;        ("M-," . dumb-jump-back))
  :defer 1 ;; force load
  :config
  (progn
    (setq
     ;; comment out to use popup (like company-mode) as selector
     dumb-jump-selector 'helm

     dumb-jump-aggressive nil

     ;; NOTE: I have modified the following to work for root build marker
     ;; set it to your needs or unset for defaults:
     ;; https://github.com/jacktasia/dumb-jump/blob/master/dumb-jump.el#L1450
     dumb-jump-project-denoters '("Jenkinsfile"))

    ;; NOTE: overriding internal function in order to format to my liking
    ;; TODO: create a PR with pluggable formatting function for dumb-jump
    (defun dumb-jump--format-result (proj result)
      (let ((path (s-replace proj "" (plist-get result :path)))
            (line (plist-get result :line))
            (context (s-trim (plist-get result :context))))
        (format "%s:%s: %s"
                (if (projectile-project-p)
                    (progn
                      (replace-regexp-in-string "\\./" ""
                      (replace-regexp-in-string "\\.\\./" "" path)))
                  path)
                line
                context)))))

;; NOTE: make elisp jumps also work
;; TODO: keep an eye on this and make sure of the following:
;; no negative impact on performance with jump ops
;; no strange behaviors in different major modes
(use-package smart-jump
  :after dumb-jump
  :config
  (progn
    ;; go ahead and allow smart jump to be smart

    (defun smart-jump-find-references-with-helm-xref ()
      "Use `helm-xref-show-xrefs' to find references."
      (interactive)
      (if (featurep 'helm-xref)
          (helm-xref-show-xrefs
           (cond ((use-region-p)
                  (buffer-substring-no-properties (region-beginning)
                                                  (region-end)))
                 ((symbol-at-point)
                  (substring-no-properties
                   (symbol-name (symbol-at-point))))))
        (message
         "Install the emacs package helm-xref to use\
 `smart-jump-simple-find-references-with-helm-xref'.")))

    ;; set smart-jump's final fallback to my own function
    (setq smart-jump-find-references-fallback-function
          'smart-jump-find-references-with-helm-xref)

    ;; disable smart-jump's final fallback
    ;; (setq smart-jump-find-references-fallback-function
    ;;       nil)


    (smart-jump-setup-default-registers)
    )
  )

;; use this if you want to see where you've exceeded column width
(use-package column-enforce-mode
  :disabled t
  :diminish column-enforce-mode
  :init
  (setq column-enforce-column 80)
  :config
  (progn
    (add-hook 'prog-mode-hook 'column-enforce-mode)))

(use-package origami
  :commands (origami-toggle-node))

(use-package beacon
  :disabled t
  :diminish beacon-mode
  :config
  (beacon-mode 1)
  (setq beacon-dont-blink-commands nil) ;; always blink
    ;; (setq beacon-lighter '"Λ") -
  (add-to-list 'beacon-dont-blink-major-modes 'man-mode)
  (add-to-list 'beacon-dont-blink-major-modes 'shell-mode))


;; ;; LSP SUPPORT
;; ;; TODO: this is the full best documentation of lsp, read every bit carefully
;; ;; https://emacs-lsp.github.io/lsp-mode/lsp-mode.html
;; ;; NOTE: currently disabled because elixir-lsp is slow on large code bases
;; ;; TODO: implement fallback to dumbjump (see doom's impl)
;; ;; NOTE: `language_server.sh' must be setup!!!
;; ;; NOTE: to add or remove workspace forlders use M-x
;; ;; lsp-workspace-folders-add
;; ;; lsp-workspace-folders-remove
;; ;; lsp-workspace-blacklist-remove
;; ;; LSP
;; (use-package lsp-mode
;;   :commands lsp
;;   :hook ((elixir-mode . lsp))
;;   :init
;;   (setq lsp-prefer-flymake nil)
;;   (setq lsp-enable-completion-at-point t)
;;   (setq lsp-enable-xref t)
;;   :config
;;   ;; TODO: promote to a documentation
;;   (add-to-list 'exec-path "~/projects/elixir-lsp/elixir-ls/release")) ;; Uncomment for lsp-mode



;;; LSP PYTHON CONFIG
;; NOTE: requires pyles: pip install python-language-server[all]
;; NOTE: has issuex with helm-xref and ivy-xref display, may need to comment them out for testing
;; (use-package lsp-mode
;;   :demand t
;;   :init
;;   (progn
;;     (setq lsp-prefer-flymake nil)
;;     (setq lsp-pyls-plugins-pycodestyle-enabled nil)
;;     (setq lsp-pyls-plugins-pylint-enabled nil)
;;     ;; (setq lsp-highlight-symbol-at-point nil)
;;     (setq lsp-ui-doc-enable nil)
;;     (setq lsp-symbol-highlighting-skip-current t)) ; disable symbol highlighting
;;   :hook (python-mode . lsp) ; enable with python
;;   :config
;;   (progn
;;     ;; NOTE: these are currently disabled because (setq lsp-symbol-highlighting-skip-current t)
;;     (set-face-background 'lsp-face-highlight-read    (face-attribute 'default :background))
;;     (set-face-background 'lsp-face-highlight-write   (face-attribute 'default :background))
;;     (set-face-background 'lsp-face-highlight-textual (face-attribute 'default :background))))

;; (use-package lsp-ui)

;; (use-package lsp-ui
;;   :after (lsp-mode)
;;   :hook (lsp-mode . lsp-ui-mode)
;;   :init
;;   ;;(setq lsp-ui-sideline-enable nil)
;;   ;;(setq lsp-ui-sideline-ignore-duplicate t)
;;   (setq lsp-ui-doc-max-width 50)
;;   (setq lsp-ui-doc-max-hight 20)
;;   (setq lsp-ui-doc-include-signature t)
;;   (setq lsp-ui-peek-always-show t)
;;   (setq lsp-ui-doc-enable nil)
;;   :commands lsp-ui-mode
;;   :config
;;   (progn
;;     ;; (define-key lsp-ui-mode-map [remap xref-find-definitions] #'lsp-ui-peek-find-definitions)
;;     ;; (define-key lsp-ui-mode-map [remap xref-find-references] #'lsp-ui-peek-find-references)
;;     ))

;; (use-package company-lsp)


;; HELP CUSTOMIZATION

(use-package help-fns+
  :ensure nil
  ;; :quelpa (help-fns+ :fetcher url :url "https://www.emacswiki.org/emacs/download/help-fns+.el")
  ;; :quelpa (help-fns+ :fetcher wiki)
  :quelpa (help-fns+ :fetcher github :repo "emacsmirror/help-fns-plus")
  :defer 2)


(use-package abbrev
  :ensure nil
  :config
  (setq save-abbrevs 'silently)
  (setq-default abbrev-mode t))


(use-package helpful
  :bind
  (("C-h f" . helpful-callable)
   ("C-h v" . helpful-variable)
   ("C-h k" . helpful-key)
   ("C-c C-d" . helpful-at-point)
   ("C-h F" . helpful-function)
   ("C-h C" . helpful-command)))

;; SYNTAX CHECKING

;; https://github.com/flycheck/flycheck-pos-tip
(use-package flycheck-pos-tip
  :after flycheck
  :config
  (flycheck-pos-tip-mode))


;; helpful examples added to elisp help system
(use-package elisp-demos
  :defer 5
  :after (helpful)
  :config
  ;; basic help system's advice
  ;; (advice-add 'describe-function-1 :after #'elisp-demos-advice-describe-function-1)
  ;; advice specific to the `helpful' package
  (advice-add 'helpful-update :after #'elisp-demos-advice-helpful-update)
  )

(use-package macrostep
  :commands (macrostep-expand
             macrostep-mode))

(use-package auto-compile
  :config
  (auto-compile-on-load-mode)
  (auto-compile-on-save-mode))


;;; PROGRAMMING SUPPORT

;; company
;; TODO: try this: https://github.com/TommyX12/company-tabnine
(use-package company
  :defer 1
  :after (general)
  ;; :bind
  ;; (:map company-active-map
  ;;       ("ESC" . company-abort)) ; side-effects on <up>/<down> in company-active-map breaks: https://emacs.stackexchange.com/questions/37659/oa-and-ob-inserted-by-emacs
  :general
    (:keymaps 'company-active-map
            "C-n" 'company-select-next
            "C-p" 'company-select-previous
            "C-d" 'company-show-doc-buffer)
  :config
  ;;; TODO: add these mode hooks for company instead of global
  ;; latex-mode-hook
  ;; html-mode-hook
  ;; emacs-lisp-mode-hook
  ;; prog-mode-hook
  ;; python-mode-hook
  (progn

    ;; setup our backends
    (add-to-list 'company-backends 'company-capf)

    ;; cycle completion options with tab-and-go
    (company-tng-configure-default)

    ;; allow tab cycling
    (eval-after-load 'company
      '(progn
         (define-key company-active-map (kbd "TAB") 'company-complete-common-or-cycle)
         (define-key company-active-map (kbd "<tab>") 'company-complete-common-or-cycle)))

    (eval-after-load 'company
      '(progn
         (define-key company-active-map (kbd "S-TAB") 'company-select-previous)
         (define-key company-active-map (kbd "<backtab>") 'company-select-previous)))


    ;; ^ wires these company-frontends and keys:
    ;; https://github.com/emacs-evil/evil-collection/issues/41#issuecomment-349500540

    (setq company-idle-delay nil
          company-minimum-prefix-length 0
          company-auto-complete t)

    ;; ;; swap search and filter shortcuts.
    ;; (define-key company-active-map "\C-s" 'company-filter-candidates)
    ;; (define-key company-active-map "\C-\M-s" 'company-search-candidates)


    ;; tab trigger approaches:

    ;; approach 1
    ;; use company provided indent-or-complete.  works well but is not smart
    ;; like approach 2, because expands on unwanted chars, e.g. (){}*, etc...
    ;; (global-set-key (kbd "TAB") #'company-indent-or-complete-common)

    ;; approach 2
    ;; ref: https://www.emacswiki.org/emacs/CompanyMode#toc6
    ;; using this approach for tab+complete control
    ;; it only completes if prior char is a _, -, or regular character class
    ;; including explicit .
    ;; also this allows yasnippet to preempt completion if there's a match.
    ;; the yasnippet expanse  may be black some completions, so it can be disabled if want.
    (progn

      (defun str-char-before-point ()
        (string (char-before (point))))
      (defun str-char-at-point ()
        (string (char-before (+ 1 (point)))))
      (defun str-char-after-point ()
        (string (char-after (point))))

      (defun generally-completable-char-before-point-p ()
        (string-match-p  "[\\-a-zA-Z0-9_\\.]" (str-char-before-point)))

      (defun generic-prog-mode-point-and-after-point-criteria-match-p ()
        "Sufficient for most programming languages, e.g. elisp, python, c"
        (or
         (at-end-of-line-text-p) ; point at the end of visual line
         (string-match-p  "[\s\t]+" (str-char-after-point)) ; space after point
         (string-match-p  "\)" (str-char-at-point)) ; point over )
         (string-match-p  "\(" (str-char-at-point)))) ; point over (

      ;; NOTE: different check-expansions can be created for different
      ;; language modes in order to handle different syntatic needs.
      ;; (setq print-maybe-on t)
      (defun check-expansion ()
        (save-excursion
          (if (and
               ;; character befor point must be
               (generally-completable-char-before-point-p)
               ;; and now what is point looking at and what is after that
               (generic-prog-mode-point-and-after-point-criteria-match-p))
              (print-maybe "match" t) ; return t
            (print-maybe "no match" nil) ; return nil
            )))

      (defun do-yas-expand ()
        (let ((yas/fallback-behavior 'return-nil))
          (yas/expand)))

      ;; NOTE: we are using several internal functions for this.
      ;; check to see if there's a better way to do this
      (defun in-live-snippet-p ()
        "Tells us if point is in a live snippet"
        (interactive)
        (if (and (bound-and-true-p yas/minor-mode) yas--active-field-overlay)
            (let* ((snippet (overlay-get yas--active-field-overlay 'yas--snippet)))
              (yas--snippet-live-p snippet))
          nil))

      ;; no yas expansion attempt included
      (defun my-tab-indent-or-company-complete ()
        "Checks if we're in a snippet, if so TAB advances through fields
         otherwise it completes or indents according to the result of check-expansion"
        (interactive)
        (if (in-live-snippet-p)
            (yas-next-field)
          (if (minibufferp)
              (minibuffer-complete)
            (if (check-expansion)
                (progn
                  ;; (message "found expansion") ; for debugging
                  (company-complete-common))
              (progn
                ;; (message "no expansion found") ; for debugging
                (indent-for-tab-command)
                ;; (c-indent-line-or-region)
                )))))

      ;; ;; includes an attempt to yas expand
      ;; (defun my-tab-indent-or-company-complete ()
      ;;   (interactive)
      ;;   (if (minibufferp)
      ;;       (minibuffer-complete)
      ;;     (if (or (not yas/minor-mode)
      ;;             (null (do-yas-expand)))
      ;;         (if (check-expansion)
      ;;             (company-complete-common)
      ;;           (indent-for-tab-command)))))


      ;; TODO: promote to :keymaps at use-package level
      ;;;; we set the binding to our tab triggering fn
      ;; (global-set-key (kbd "TAB") 'my-tab-indent-or-company-complete)
      (general-define-key
       :keymaps 'prog-mode-map
       "TAB" 'my-tab-indent-or-company-complete
       ;; ...
       )

      ;;;; XOR
      ;;;; toggle company menu w/ C-TAB
      ;; helm buffers this globally
      ;; (my/global-map-and-set-key "C-TAB" 'company-complete-common)


      ;; allow C-TAB to close company menu
      (define-key company-active-map (kbd "C-TAB") #'company-abort)


      ;; use enter to complete explicitly and don't send a CR when in company-active mode
      (define-key company-active-map (kbd "RET") #'company-complete-selection))


    ;; enable company mode everywhere, also serves to prestart it for prog-modes
    (global-company-mode 1)
    ))


(use-package company-flx
  :config
  (company-flx-mode +1))

;; most frequently used to top
(use-package company-statistics
  :config
  (company-statistics-mode))






;; LANGUAGE CUSTOMIZATION

;; XML
;; default indent to be 4
(setq nxml-child-indent 4 nxml-attribute-indent 4)

;; EMACS LISP
(use-package emacs-lisp-mode
  :ensure nil
  :init
  (defun my-elisp-mode-hook ()
    "Hook for `emacs-lisp-mode'"
    (set (make-local-variable 'company-backends)
         '((company-capf company-dabbrev-code company-yasnippet company-files))))
  :mode ("\\.el$" . emacs-lisp-mode)
  :bind (:map emacs-lisp-mode-map
              ("C-c I" . describe-function)
              ("C-c S" . find-function-at-point)))
;; leave these outside of above
(add-hook 'emacs-lisp-mode-hook 'my-elisp-mode-hook)
(add-hook 'emacs-lisp-mode-hook 'company-mode)

;; company-shell
;; to provide company-shell-env only
(use-package company-shell
  :after company
  :defer 3)

;; shell scripting
(use-package sh-script
  :mode (("\\.alias\\'"       . shell-script-mode)
         ("\\..*sh\\'"        . shell-script-mode)
         ("\\.[a-zA-Z]+rc\\'" . shell-script-mode)
         ("crontab.*\\'"     . shell-script-mode))
  :init
  (defun my-shell-script-mode-hook ()
    "Hook for `sh-mode'"
    (set (make-local-variable 'company-backends)
         '((company-capf company-dabbrev-code company-shell-env company-yasnippet company-files))))
  :config
  (progn
    (add-hook 'sh-mode-hook 'my-shell-script-mode-hook)

    ))

;; ;; syntax checking in shell scripts
;; (use-package flymake-shell
;;   :hook (sh-set-shell-hook . flymake-shell-load))


;; R
(use-package ess-site
  :disabled
  :commands R)

;; SQL
(use-package sql
  :init
  ;; (add-hook 'sql-mode-hook 'regtab-mode)
  (add-hook 'sql-interactive-mode-hook 'regtab-mode))

(use-package sqlup-mode
  :init
  (add-hook 'sql-mode-hook 'sqlup-mode)
  (add-hook 'sql-interactive-mode-hook 'sqlup-mode))

(use-package sql-indent
  :defer t
  :init
  (add-hook 'sql-mode-hook 'sqlind-minor-mode)
  (add-hook 'sql-interactive-mode-hook 'sqlind-minor-mode))

(use-package sqlformat
  :ensure t
  ;; :custom (sqlformat-command 'sqlformat)
  :custom (sqlformat-command 'pgformatter) ; brew install pgformatter or apt-get install pgformatter
  )

;; SYMBOLS
(use-package xah-math-input
  :init
  (add-hook 'text-mode-hook 'xah-math-input-mode)
  (add-hook 'org-mode-hook 'xah-math-input-mode))

;; LUA
(use-package lua-mode)

;; C/C++

;; additional cpp file extensions
(add-hook 'after-init-hook
	  (function (lambda()
		      (add-to-list 'auto-mode-alist '("\\.h\\'" . c++-mode))
		      (add-to-list 'auto-mode-alist '("\\.inl\\'" . c++-mode))
		      (add-to-list 'auto-mode-alist '("\\.inc\\'" . c++-mode))
		      (add-to-list 'auto-mode-alist '("\\.cc\\'" . c++-mode))
		      )))

;; TODO: make it so these don't run everytime a c file is loaded.  ordered with company, etc..
(add-hook 'c-mode-common-hook
          (lambda()
            (define-key c-mode-map  (kbd "TAB") 'my-tab-indent-or-company-complete)
            (define-key c++-mode-map  (kbd "TAB") 'my-tab-indent-or-company-complete)
            ))

(use-package cmake-mode)
(use-package flymake-cursor)
(use-package flymake-google-cpplint
  :init
  :config
  (add-hook 'c-mode-hook 'flymake-google-cpplint-load)
  (add-hook 'c++-mode-hook 'flymake-google-cpplint-load)
  (custom-set-variables
   ;; MacOS
   ;; '(flymake-google-cpplint-command "/System/Library/Frameworks/Python.framework/Versions/2.7/bin/cpplint")
   ;; Linux
   '(flymake-google-cpplint-command "/usr/local/bin/cpplint")
   '(flymake-google-cpplint-verbose "--verbose=0")
   ;; Note that I've turned off build rules. May want those on, e.g. +build/include
   '(flymake-google-cpplint-filter "--filter=-whitespace/line_length,-build")))

;; ;; FIXME: straight-fix
(use-package google-c-style
  :ensure t
  :init
  (add-hook 'c-mode-common-hook 'google-set-c-style)
  (add-hook 'c-mode-common-hook 'google-make-newline-indent))



;; PYTHON
;; (use-package python
;;   :mode ("\\.py\\'" . python-mode)
;;   :config
;;   (progn
;;     ;; use ipython for interpreter if it's available
;;     (if (executable-find "ipython")
;;         (setq python-shell-interpreter "ipython"
;;               python-shell-interpreter-args "--simple-prompt -i")
;;       (setq setq python-shell-interpreter "python3"))))

;; swap out with ipython above if no modern ipython (7.x) or higher in env
(use-package python
  :commands python-mode
  :interpreter ("python3" . python-mode)
  :hook
  (python-mode-hook . (lambda ()
                        (highlight-lines-matching-regexp "import ipdb")
                        (highlight-lines-matching-regexp "ipdb.set_trace()")
                        (highlight-lines-matching-regexp "import wdb")
                        (highlight-lines-matching-regexp "wdb.set_trace()")))
  ;; :custom
  ;; (python-environment-virtualenv (quote ("python3" "-m" "venv")))
  :init
  (progn
    "hydra for shifting python code left/right more easily"

    (defun tom/shift-left (start end &optional count)
      "Shift region left and activate hydra."
      (interactive
       (if mark-active
           (list (region-beginning) (region-end) current-prefix-arg)
         (list (line-beginning-position) (line-end-position) current-prefix-arg)))
      (python-indent-shift-left start end count)
      (tom/hydra-move-lines/body))

    (defun tom/shift-right (start end &optional count)
      "Shift region right and activate hydra."
      (interactive
       (if mark-active
           (list (region-beginning) (region-end) current-prefix-arg)
         (list (line-beginning-position) (line-end-position) current-prefix-arg)))
      (python-indent-shift-right start end count)
      (tom/hydra-move-lines/body))

    (defhydra tom/hydra-move-lines ()
      "Move one or multiple lines"
      ("<" python-indent-shift-left "left")
      (">" python-indent-shift-right "right")))

   :bind (:map python-mode-map
              ("C-c <" . tom/shift-left)
              ("C-c >" . tom/shift-right)))

;; see pyvenv config here: https://github.com/EmacsRuPub/emacs-config/blob/master/config.org#python
(use-package pyvenv
  :ensure t
  :config
  (pyvenv-mode 1))

;; (use-package auto-virtualenvwrapper
;;   :ensure t
;;   :commands auto-virtualenvwrapper-activate
;;   :init
;;   (add-hook 'python-mode-hook #'auto-virtualenvwrapper-activate)
;;   (add-hook 'window-configuration-change-hook #'auto-virtualenvwrapper-activate)
;;   (add-hook 'focus-in-hook #'auto-virtualenvwrapper-activate)
;;   :config
;;   (setq auto-virtualenvwrapper-verbose t))


  (use-package company-quickhelp          ; Show help in tooltip
    :ensure t
    :defer t
    :init (with-eval-after-load 'company
            (company-quickhelp-mode)))

;; NOTE: It is very important that all libs that are
;; accessible to jedi be either in the project dir or
;; available on the PYTHONPATH.
(use-package company-jedi
  :ensure t
  :after python
  :defer t
  :bind (:map python-mode-map
         ("M-." . jedi:goto-definition)
         ("M-," . my-jedi:goto-definition-pop-marker)
         ;; ("M-/" . jedi:show-doc)
         ;; ("M-?" . helm-jedi-related-names)
         ; ("C-c C-d" . jedi:show-doc)
         :map comint-mode-map
         ("C-l" . comint-clear-buffer))
  :init
  (setq jedi:complete-on-dot t)
  (add-hook 'python-mode-hook (lambda () (add-to-list 'company-backends 'company-jedi)))
  :config
  (progn
    ;; NOTE: I'm defining my own ver of jedi:goto-definistion-pop-marker fn in oder to make
    ;; it fallback to use (xref-pop-marker-stack), so the helm-projectile-rg advice xref find behaves
    ;; the same in python with jedi: marker mgmnt functions
    (defun my-jedi:goto-definition-pop-marker ()
      "Goto the last point where `jedi:goto-definition' was called."
      (interactive)
      (if (ring-empty-p jedi:goto-definition--marker-ring)
          (xref-pop-marker-stack)
        ;; (error "Jedi marker ring is empty, can't pop")
        (let ((marker (ring-remove jedi:goto-definition--marker-ring 0)))
          (switch-to-buffer (or (marker-buffer marker)
                                (error "Buffer has been deleted")))
          (goto-char (marker-position marker))
          ;; Cleanup the marker so as to avoid them piling up.
          (set-marker marker nil nil))))

    ;; here's another approach, that does not use jedi's builtin marker-ring
    ;; https://txt.arboreus.com/2013/02/21/jedi.el-jump-to-definition-and-back.html

    ;; center the jumped def into the middle of the screen using hook
    (defun my-jedi-goto-definition-center-in-window-hook ()
      (let ((recenter-positions '(middle)))
        (recenter-top-bottom)))

    (add-hook 'jedi:goto-definition-hook
              'my-jedi-goto-definition-center-in-window-hook)

    )

     ;; in this case we're using jedi's internal marker ring for python
     ;; (defun my-python-push-marker-ring ()
     ;;     (jedi:goto-definition-push-marker))
)

;; lets you manage pip packages
(use-package pippel
  :defer t)

;; requires > python3.7
;; need black binary: pip install black
(use-package blacken
  :ensure t)

(use-package yapfify
  :ensure t
  :hook (python-mode . yapf-mode))

;; (use-package py-autopep8
;;   :hook (python-mode-hook . py-autopep8-enable-on-save))

(use-package jinja2-mode
  :mode (("\\.j2$" . jinja2-mode)))

;; PROTOBUF
(use-package protobuf-mode
  :mode "\\.proto$")

;; ELIXIR
(use-package elixir-mode
  :commands elixir-mode
  :mode
  (("\\.exs$" . elixir-mode)
   ("\\.ex$" . elixir-mode))
  :config
  (progn
    ;; TODO: reenable alchemist once elixir-lsp setup
    ;; currently disabled in order to use dumb-jump
    (add-hook 'elixir-mode-hook 'alchemist-mode)
    (add-hook 'elixir-mode-hook  'regtab-mode)

    ;; support functions stolen from spacemacs

    (defun spacemacs//elixir-enable-compilation-checking ()
      "Enable compile checking if `elixir-enable-compilation-checking' is non nil."
      (when (or elixir-enable-compilation-checking)
        (flycheck-mix-setup)
        ;; enable credo only if there are no compilation errors
        (flycheck-add-next-checker 'elixir-mix '(warning . elixir-credo))))

    (defun spacemacs/elixir-annotate-pry ()
      "Highlight breakpoint lines."
      (interactive)
      (highlight-lines-matching-regexp "require IEx; IEx.pry"))

    (defun spacemacs/elixir-toggle-breakpoint ()
      "Add a breakpoint line or clear it if line is already a breakpoint."
      (interactive)
      (let ((trace "require IEx; IEx.pry")
            (line (thing-at-point 'line)))
        (if (and line (string-match trace line))
            (kill-whole-line)
          (progn
            (back-to-indentation)
            (insert trace)
            (newline-and-indent)))))

  ))

(use-package alchemist
  :commands alchemist-mode
  :init
  (defun my-put-iex ()
    ;; Automatically add pry so debugging is easy
    (interactive)
    (insert "require IEx; IEx.pry"))
  :config
  ;; override iex key sequence with my version
  (bind-keys :map alchemist-mode-map
             ( "C-c a p i" . my-put-iex))

  ;; compiler/recompile this buffer, e.g. this module
  (bind-keys :map alchemist-mode-map
             ("C-c C-l" . (lambda () (interactive)
                            (save-buffer)
                            (alchemist-iex-compile-this-buffer))))

  ;; make elixir evalute a single line similar to elisp
  (bind-keys :map alchemist-mode-map
             ("C-x C-e" . alchemist-iex-send-current-line))

  ;; overriding jump keys for alchemist mode map to make jumping work in monorepos
  ;; using dumb-jump directly because smart-jump has alchemist jump registered inline
  ;; before dumb-jump
  (bind-keys :map alchemist-mode-map
             ("M-." . dumb-jump-go))
  (bind-keys :map alchemist-mode-map
            ("M-," . dumb-jump-back))
  )

(use-package elixir-yasnippets)

(use-package flymake-elixir
  :hook
  (elixir-mode-hook . flymake-elixir-load))

;; C#
;; NOTE: requires: M-x
;; omnisharp-install-server
;; omnisharp-start-omnisharp-server
(use-package omnisharp
  :after csharp-mode
  :init
  (defun my-csharp-mode-hook ()
    "Hook for `emacs-lisp-mode'"
    (set (make-local-variable 'company-backends)
         '((company-capf company-dabbrev-code company-omnisharp company-yasnippet company-files)))
    (local-set-key (kbd "C-c C-c") #'recompile))
  :bind (:map omnisharp-mode-map
              ("C-c r" . omnisharp-run-code-action-refactoring)
              ("M-." . omnisharp-go-to-definition)
              ;; ("M-." . omnisharp-find-implementations)
              ("M-?" . omnisharp-find-usages))
  :hook ((csharp-mode . omnisharp-mode)))


;; JAVASCRIPT
(use-package js2-mode
  :mode ("\\.js$" . js2-mode)
  :init
  (setq js2-strict-missing-semi-warning nil))

(use-package nodejs-repl
  :commands (nodejs-repl
             nodejs-repl-send-buffer
             nodejs-repl-switch-to-repl
             nodejs-repl-send-region
             nodejs-repl-send-last-expression
             nodejs-repl-execute
             nodejs-repl-load-file))

(use-package nvm
  :commands (nvm-use
             nvm-use-for))

;; JSON
(use-package json-mode
  :mode "\\.json$")

(use-package json-snatcher
  :commands (jsons-print-path))



;; Prettier for JS
(use-package prettier-js
  :ensure t)


;; TYPESCRIPT

;; TypeScript completion
(use-package tide
  :demand t
  :ensure t
  :after (typescript-mode web-mode company flycheck)
  :hook ((typescript-mode . tide-setup)
	 (typescript-mode . tide-hl-identifier-mode)
	 (web-mode . (lambda ()
		       (when (string-equal "tsx" (file-name-extension
						  buffer-file-name))
			 (tide-setup))))
	 (web-mode . (lambda ()
		       (when (string-equal "tsx" (file-name-extension
						  buffer-file-name))
			 (tide-hl-identifier-mode)))))
  :config (flycheck-add-mode 'typescript-tslint 'web-mode))

;; TypeScript highlighting
(use-package typescript-mode
  :demand t
  :ensure t
  :mode (("\\.ts\\'" . typescript-mode))
  :custom
  (typescript-indent-level 2 "TypeScript indent size"))

;; J/TSX highlighting
(use-package web-mode
  :demand t
  :ensure t
  :mode (("\\.tsx\\'" . web-mode)
	 ("\\.jsx\\'" . web-mode))
  :custom
  (web-mode-markup-indent-offset 2 "Web mode indent size"))




;; YAML
(use-package yaml-mode
  :mode ("\\.yaml\\'" "\\.yml\\'")
  :custom-face
  (font-lock-variable-name-face ((t (:foreground "violet")))))

;; CSV
(use-package csv-mode
  :mode "\\.csv\\'")

;; ELF
(use-package elf-mode
  :magic ("ELF" . elf-mode))

;; MARKDOWN
(use-package markdown-mode
  :mode ("\\.md\\'" . markdown-mode))

;; GRAPHVIZ
(use-package graphviz-dot-mode
  :ensure t
  :mode ("\\.dot\\'" . graphviz-dot-mode))

;; HEX
(use-package hexl-mode
  :ensure nil
  :mode (("\\.bin\\'" . hexl-mode)
         ("\\.a\\'" . hexl-mode)
         ("\\.o\\'" . hexl-mode)
         ("\\.so\\'" . hexl-mode)
         ("\\.so.*\\'" . hexl-mode)
         ("\\.exe\\'" . hexl-mode)
         ("\\.dll\\'" . hexl-mode)))

;; GROOVY
(use-package groovy-mode
  :mode (("/Jenkinsfile\\'" . groovy-mode)))

;; (use-package gradle-mode)

;; COMPILATION AND DEBUGGING
(setq compilation-finish-functions
      (lambda (buf str)
        (if (null (string-match ".*exited abnormally.*" str))
            ;;no errors, make the compilation window go away in a few seconds
            (progn
              (run-at-time "0.4 sec" nil
                           (lambda ()
                             (select-window (get-buffer-window (get-buffer-create "*compilation*")))
                             (switch-to-buffer nil)))
              (message "No Compilation Errors!")))))

(use-package realgud
  :commands (realgud:gdb
             realgud:ipdb
             realgud:pdb))

(use-package quickrun
  :commands (quickrun
             quickrun-region
             quickrun-with-arg
             quickrun-shell
             quickrun-compile-only
             quickrun-replace-region))

;; clang-format can be triggered using C-c C-f
;; Install clang-format: $ apt-get install clang-format-3.9
;;                       $ ln -s `which clang-format-3.9` ~/bin/clang-format
;; Create clang-format file using google style
;; clang-format -style=google -dump-config > .clang-format
(use-package clang-format
  :bind ("C-c C-f" . 'clang-format-region))




;;; INFRASTRUCTURE


;; DOCKER
(use-package docker
  :ensure t)

(use-package dockerfile-mode
  :ensure t
  :mode "Dockerfile.*\\'")

;; NGNIX
(use-package nginx-mode)

;; TERRAFORM
(use-package terraform-mode
  :ensure t
  :init
  (setq terraform-indent-level 2))

(use-package company-terraform
  :ensure t
  :config
  (company-terraform-init))

;; SSH
(use-package ssh-config-mode
  :mode ((".ssh/config\\'"       . ssh-config-mode)
         ("sshd?_config\\'"      . ssh-config-mode)
         ("known_hosts\\'"       . ssh-known-hosts-mode)
         ("authorized_keys\\'" . ssh-authorized-keys-mode)))


;; TODO: document this at top
(use-package windresize
  :ensure t
  :bind ("C-c r" . windresize))


;; TODO: document this at top
;; contract with C-- C-= - - -
;; ^=    ^[[61;5u
;; (use-package expand-region
;;   :ensure t
;;   :bind (("C-=" . er/expand-region))
;;   :config
;;   (progn
;;     ;; (defadvice er/expand-region (around fill-out-region activate)
;;     ;;   (if (or (not (region-active-p))
;;     ;;           (eq last-command 'er/expand-region))
;;     ;;       ad-do-it
;;     ;;     (if (< (point) (mark))
;;     ;;         (let ((beg (point)))
;;     ;;           (goto-char (mark))
;;     ;;           (end-of-line)
;;     ;;           (forward-char 1)
;;     ;;           (push-mark)
;;     ;;           (goto-char beg)
;;     ;;           (beginning-of-line))
;;     ;;       (let ((end (point)))
;;     ;;         (goto-char (mark))
;;     ;;         (beginning-of-line)
;;     ;;         (push-mark)
;;     ;;         (goto-char end)
;;     ;;         (end-of-line)
;;     ;;         (forward-char 1)))))

;;     ;; (defhydra hydra-expand (:hint nil :columns 1 :body-pre (er/expand-region 1))
;;     ;;   ("c" er/contract-region "Contract")
;;     ;;   ("e" er/expand-region "Expand"))
;;     )
;;   )

;; contract with C-=, =, =, -, -
;; ^=    ^[[61;5u
(use-package expand-region
  :commands er/expand-region
  :config (setq expand-region-contract-fast-key "-")
  :bind (("C-= =" . er/expand-region)))

;; TODO: move this section above language modes and remove demand

;;; INDENTATION

;; NOTE: disabling this for now.  python-mode seems to behave better
(use-package regtab
  :demand t
  :ensure nil
  :config
  (progn
    ;; generate a global minor mode for regtab and activate it
    ;; NOTE: this may cause issues with tabbing BE AWARE!!!
    (define-globalized-minor-mode my-global-regtab-mode regtab-mode
      (lambda () (regtab-mode 1)))
    ;;(my-global-regtab-mode 1)
    ))


;; ;; clean auto-indent and backspace unindent
;; ;; https://github.com/pmarinov/clean-aindent-mode
;; (use-package clean-aindent-mode
;;   :ensure t
;;   :defer 1
;;   :config
;;   (add-hook 'prog-mode-hook 'clean-aindent-mode))


;; a minor mode that guesses the indentation offset originally used for
;; creating source code files and transparently adjusts the corresponding
;; settings in Emacs, making it more convenient to edit foreign files
;; https://github.com/jscheid/dtrt-indent
(use-package dtrt-indent
  :ensure t
  :hook ((sh-mode . dtrt-indent-mode)))


;;;  SCROLLING

(use-package smooth-scrolling
  :defer 1
  :init
  (setq scroll-step 1
        scroll-conservatively 10000
        smooth-scroll-margin 2
        scroll-preserve-screen-position t)
  :config
  (smooth-scrolling-mode 1))


(defun scroll-lock-previous-line-fixed-position (arg)
  (interactive "p")
  (when (not (pos-visible-in-window-p (point-min)))
    (scroll-down arg)) ; scroll, before moving cursor
  (previous-line arg) ; keep the cursor position fixed relative to the window
  (setq this-command 'previous-line))  ; make temporary-goal-column work correctly
(global-set-key  [(control shift up)] 'scroll-lock-previous-line-fixed-position)

(defun scroll-lock-next-line-fixed-position (arg)
  (interactive "p")
  (when (not (pos-visible-in-window-p (point-max)))
    (scroll-up arg)) ; scroll, before moving cursor
  (next-line arg) ; keep the cursor position fixed relative to the window
  (setq this-command 'next-line))  ; make temporary-goal-column work correctly
(global-set-key  [(control shift down)] 'scroll-lock-next-line-fixed-position)


;; minimap
;; (use-package sublimity
;;   :ensure t
;;   :config
;;   (sublimity-mode 1))

;; ;; keep a live running log of all keystrokes and fns they run
;; (use-package interaction-log
;;   :defer 1
;;   :config
;;   (progn
;;     (interaction-log-mode +1)
;;     (defun open-interaction-log ()
;;       (interactive)
;;       (display-buffer ilog-buffer-name))
;;     (bind-key "C-h C-l" 'open-interaction-log)))


;; PARENS

;; documentation: https://smartparens.readthedocs.io/en/latest/
;; TODO: integrate the following into my workflow
;; sp-forward-slurp-sexp
;; sp-forward-barf-sexp

;; (use-package smartparens
;;   :hook ((elixir-mode . smartparens-mode))
;;   :config
;;   (progn
;;     ;; enable visual
;;     (show-smartparens-global-mode)

;;     ;; load support for elixir
;;     (require 'smartparens-elixir)

;;     ;; chords for slurping and barfing
;;     ;; NOTE: this is experimental. it will conflict with elixir binary literals
;;     (key-chord-define-global "<<" 'sp-forward-slurp-sexp)
;;     (key-chord-define-global ">>" 'sp-forward-barf-sexp)

;;     ))




(use-package smartparens
  :defer 2
  :config
  (progn
    ;; enable visual
    (show-smartparens-global-mode)

    (sp-pair "\"" "\"") ;; adds `' as a local pair in emacs-lisp-mode

    ;; ;; chords for slurping and barfing
    ;; ;; NOTE: this is experimental. it will conflict with elixir binary literals
    ;; (key-chord-define-global "<<" 'sp-forward-slurp-sexp)
    ;; (key-chord-define-global ">>" 'sp-forward-barf-sexp)


    ;; bindings for slurping/barfing
    (global-set-key  (kbd "C-0") 'sp-forward-barf-sexp) ; [48;5u
    (global-set-key  (kbd "C-9") 'sp-forward-slurp-sexp) ; [57;5u

    ))


;; RECENTER AFTER THE FOLLOWING JUMP EVENTS

(let ((after-fn (lambda (&rest _) (recenter nil))))
  (advice-add 'goto-line :after after-fn)
 ;;  (advice-add 'pop-mark :after after-fn)
  (advice-add 'xref-goto-xref :after after-fn)
  (advice-add 'smart-jump-go :after after-fn)
  (advice-add 'dumb-jump-go :after after-fn)
  (advice-add 'helm-goto-source :after after-fn)
  (advice-add 'helm-goto-line :after after-fn)
  (advice-add 'helm-goto-next-file :after after-fn)
  (advice-add 'helm-xref-goto-xref-item :after after-fn)
  (advice-add 'helm-find-files :after after-fn)
  ;; etc...
  )

;; the scratch buffer will persist between runs
(use-package persistent-scratch
  :ensure t
  :defer 1
  :config
  (progn

    (setq scratch-buffers '("*scratch*" "*copy-log*"))

    ;; prevent killing of scratch buffers
    (deferred:$
      (deferred:wait 5000) ; msec
      (deferred:nextc it
        (lambda (x)
          (mapcar (lambda (name)
            (when (get-buffer name)
            (prevent-kill-buffer name)))
            scratch-buffers))))


    ;; override from persistent-scratch.el
    ;; add more scratch buffers to be persisted
    (defun persistent-scratch-default-scratch-buffer-p ()
      "Return non-nil iff the current buffer's name is *scratch*."
      (member (buffer-name) scratch-buffers))

    (persistent-scratch-setup-default)
    (persistent-scratch-autosave-mode)))


(use-package pip-requirements
  :ensure t
  :mode (("requirements.*\\.txt" . pip-requirements-mode)))

(use-package paradox
  :ensure t
  :after async
  :defer 3
  :commands paradox-list-packages
  :config
  (setq paradox-automatically-star t
        paradox-execute-asynchronously t))

(use-package company-ansible
  :ensure t
  :after (company)
  :config
  (add-to-list 'company-backends 'company-ansible))

(use-package poly-ansible
  :ensure t)

;;; WRITING

(use-package writeroom-mode
  :defer 3
  :preface
  (define-minor-mode prose-mode
    "Set up a buffer for prose editing.
This enables or modifies a number of settings so that the
experience of editing prose is a little more like that of a
typical word processor."
    nil " Prose" nil
    (if prose-mode
        (progn
          (when (fboundp 'writeroom-mode)
            (writeroom-mode 1))
          (setq truncate-lines nil)
          (setq word-wrap t)
          (setq cursor-type 'bar)
          (when (eq major-mode 'org)
            (kill-local-variable 'buffer-face-mode-face))
          (buffer-face-mode 1)
          ;;(delete-selection-mode 1)
          (set (make-local-variable 'blink-cursor-interval) 0.6)
          (set (make-local-variable 'show-trailing-whitespace) nil)
          (set (make-local-variable 'line-spacing) 0.2)
          (set (make-local-variable 'electric-pair-mode) nil)
          (ignore-errors (flyspell-mode 1))
          (visual-line-mode 1))
      (kill-local-variable 'truncate-lines)
      (kill-local-variable 'word-wrap)
      (kill-local-variable 'cursor-type)
      (kill-local-variable 'show-trailing-whitespace)
      (kill-local-variable 'line-spacing)
      (kill-local-variable 'electric-pair-mode)
      (buffer-face-mode -1)
      ;; (delete-selection-mode -1)
      (flyspell-mode -1)
      (visual-line-mode -1)
      (when (fboundp 'writeroom-mode)
        (writeroom-mode 0)))))

;;; TMUX

(use-package emamux
  :ensure t
  :general
  (:prefix "<f12>"
           "n" 'emamux:new-window
           "s" 'emamux:send-region
           "r" 'emamux:run-command))


(use-package paradox
  :ensure t
  :defer 1
  :config
  (paradox-enable))


(use-package try
  :ensure t
  :defer t)


;; TOP FOR EMACS
;; monitor all functions run in emacs from outside shell w/:
;; step 1: in a separate shell:
;; socat -u UNIX-RECV:/tmp/explain-pause.socket STDOUT | grep -v tabbar-ruler-mouse-movement
;; step 2:
;; (start-explain-pause-logging)
;; (setq use-package-ensure-function 'quelpa)
(use-package explain-pause-mode
  :ensure nil
  :quelpa (explain-pause-mode :fetcher github :repo "lastquestion/explain-pause-mode")
  :defer 1
  :config
  (defun start-explain-pause-logging ()
    (explain-pause-log-to-socket "/tmp/explain-pause.socket")
    (explain-pause-mode t)))



;; ICONS
;; NOTE: IMPORTANT! MUST RUN (all-the-icons-install-fonts) AT LEAST ONCE PER INSTALL
;; REF: https://www.youtube.com/watch?v=Buo-KLOevxo
(when (not *is-in-terminal*)

  (use-package all-the-icons
    :ensure t
    :init ;; don't install if already installed. not this can be updated by manually running: (all-the-icons-install-fonts t)
    (unless (member "all-the-icons" (font-family-list))
      (all-the-icons-install-fonts t)))

  ;; not working
  (use-package all-the-icons-dired
    :ensure t
    :if window-system
    :hook ((dired-mode-hook . all-the-icons-dired-mode)))

  (use-package all-the-icons-ivy
    :ensure t
    :if window-system
    :hook (())
    :init (all-the-icons-ivy-setup))

  (use-package all-the-icons-ibuffer
    :ensure t
    :if window-system
    :init (all-the-icons-ibuffer-mode 1))


  ;; NOTE: loaded by el-get currently
  ;; TODO: move to melpa when package becomes avail
  ;; NOTE: having todo some janky timing here, because race with el-get async load
  ;; TODO: also switch to all-the-icons instead of treemacs icons when implemented
  ;; REF: https://github.com/yyoncho/helm-icons
  (with-eval-after-load 'helm
    (defun my-load-helm-icons ()
      (deferred:$
        (deferred:wait 4000) ; msec
        (deferred:nextc it
          (lambda (x)
            (message "loading helm-icons!!!!")
            (when (and (not *is-in-terminal*)
                       (fboundp 'helm-icons-enable))
              (helm-icons-enable)))
          )))
    (my-load-helm-icons)))


;; MENU SYSTEM

(defhydra hydra-undo-tree (:color yellow
                                  :hint nil
                                  )
  "
  _p_: undo  _n_: redo _s_: save _l_: load   "
  ("p"   undo-tree-undo)
  ("n"   undo-tree-redo)
  ("s"   undo-tree-save-history)
  ("l"   undo-tree-load-history)
  ("u"   undo-tree-visualize "visualize" :color blue)
  ("q"   nil "quit" :color blue))


(general-define-key
 :keymaps '(normal visual emacs motion esc-map)
 :prefix "SPC"
 :non-normal-prefix "C-SPC"
 "" nil

 "SPC" 'helm-M-x

 "c" '(:ignore t :which-key "comment")
 "cl" 'comment-line
 "cr" 'comment-region

 "f" '(:ignore t :which-key "file")
 "ff" 'helm-find-files
 "fr" 'helm-recentf
 "fl" 'helm-locate
 "fs" 'save-buffer

 "/"   'helm-do-ag-project-root
 "TAB" 'switch-to-prev-buffer
 "SPC" 'helm-M-x
 "ff"  'helm-find-files
 "fed" 'edit-dotfile
 "fei" 'edit-init-file
 "fez" 'edit-zsh-dotfile
 "feR" 'reload-config-files
 "qq"  'save-buffers-kill-terminal
 ;; Magit
 "gs" 'magit-status
 ;; Buffers
 "bb"  'helm-buffers-list
 "bm"  'switch-to-messages
 "bs"  'switch-to-scratch
 "bdb" 'switch-to-dashboard
 "bcl" 'switch-to-copy-log
 "bk"  'kill-this-buffer
 ;; etc.
 "q"   'send-keyboard-quit
 "u"   'hydra-undo-tree/body
 )


;; GLOBAL ERGO SHORTCUTS

(define-key key-translation-map (kbd "C-q") (kbd "C-g"))
(define-key key-translation-map (kbd "M-q") (kbd "C-g"))

;; DASHBOARD

(use-package dashboard                  ;
  :ensure t
  :bind (
         ;; NOTE: addresses random chars when clicking in dashboard
         ;;; https://github.com/emacs-dashboard/emacs-dashboard/issues/45
	       :map dashboard-mode-map
	       ("<down-mouse-1>" . nil)
	       ("<mouse-1>" . widget-button-click)
	       ("<mouse-2>" . widget-button-click))
  :hook (after-init . dashboard-setup-startup-hook)
  :config
  (progn
    ;; content is not centered by default. To center, set
    (setq dashboard-center-content nil)

    ;; to disable shortcut "jump" indicators for each section, set
    (setq dashboard-show-shortcuts nil)

    ;; (setq show-week-agenda-p t)

    (setq dashboard-items '((recents  . 10)
                            (projects . 10)
                            (bookmarks . 4)
                            ;; (agenda . 5)
                            ;; (registers . 2)
                            ))

    ;; (require 'all-the-icons)
    ;; (setq dashboard-set-heading-icons t)
    ;; (setq dashboard-set-file-icons t)

    (setq dashboard-set-navigator t)))


;; BOOKMARKING

;; ;; builtin bookmarks control
;; (use-package bookmark
;;   :ensure nil
;;   :init
;;   :bind
;;   (("C-c b s" . bookmark-set)
;;    ("C-c b S" . bookmark-save)
;;    ("C-c b j" . bookmark-jump)
;;    ("C-c b l" . bookmark-bmenu-list)))

;; Visibile bookmarks in buffer
(use-package bm
  :ensure t
  :demand t
  :bind (("C-M-;" . bm-toggle)
         ("C-M-," . bm-previous)
         ("C-M-." . bm-next))
  :init
  ;; restore on load (even before you require bm)
  (setq bm-restore-repository-on-load t)

  :config

  ;; custom keybinding support for Iterm2 keymappings
  (if *is-in-terminal*
      (my/global-map-and-set-key "C-M-;" 'bm-toggle)
    (my/global-map-and-set-key "C-M-," 'bm-previous)
    (my/global-map-and-set-key "C-M-." 'bm-next)
    )

  ;; Allow cross-buffer 'next'
  (setq bm-cycle-all-buffers t)

  ;; where to store persistant files
  (setq bm-repository-file "~/.emacs.d/var/bm-repository.el")

  ;; save bookmarks
  (setq-default bm-buffer-persistence t)

  ;; Loading the repository from file when on start up.
  (add-hook 'after-init-hook 'bm-repository-load)

  ;; Saving bookmarks
  (add-hook 'kill-buffer-hook #'bm-buffer-save)

  ;; Saving the repository to file when on exit.
  ;; kill-buffer-hook is not called when Emacs is killed, so we
  ;; must save all bookmarks first.
  (add-hook 'kill-emacs-hook #'(lambda nil
                                 (bm-buffer-save-all)
                                 (bm-repository-save)))

  ;; The `after-save-hook' is not necessary to use to achieve persistence,
  ;; but it makes the bookmark data in repository more in sync with the file
  ;; state.
  (add-hook 'after-save-hook #'bm-buffer-save)

  ;; Restoring bookmarks
  (add-hook 'find-file-hooks   #'bm-buffer-restore)
  (add-hook 'after-revert-hook #'bm-buffer-restore)

  ;; The `after-revert-hook' is not necessary to use to achieve persistence,
  ;; but it makes the bookmark data in repository more in sync with the file
  ;; state. This hook might cause trouble when using packages
  ;; that automatically reverts the buffer (like vc after a check-in).
  ;; This can easily be avoided if the package provides a hook that is
  ;; called before the buffer is reverted (like `vc-before-checkin-hook').
  ;; Then new bookmarks can be saved before the buffer is reverted.
  ;; Make sure bookmarks is saved before check-in (and revert-buffer)
  (add-hook 'vc-before-checkin-hook #'bm-buffer-save)

  )

;; MACOS-SPECIFIC FEATURES

;; Reveal in finder for current bufferz
(use-package reveal-in-osx-finder
  :bind ("C-c z" . reveal-in-osx-finder)
  :ensure t
  :if (eq system-type 'darwin))


(use-package password-generator
  :ensure t)

;; install qrencode
;; brew install qrencode
;; sudo apt-get install qrencode
(defun qr-code-region (start end)
  "Show a QR code of the region.
A simple way to transfer text to the phone..."
  (interactive "r")
  (let ((buf (get-buffer-create "*QR*"))
	(inhibit-read-only t))
    (with-current-buffer buf
      (erase-buffer))
    (let ((coding-system-for-read 'raw-text))
      (shell-command-on-region start end "qrencode -o -" buf))
    (switch-to-buffer buf)
    (image-mode)))


;; * Programming & Writing
;; comment-tags -- Tags comments
(use-package comment-tags
  :ensure t
  :init
  (setq comment-tags-keymap-prefix (kbd "C-c t"))
  (with-eval-after-load "comment-tags"
    (setq comment-tags-keywords
	  '("TODO"
	    "FIXME"
	    "BUG"
	    "HACK"
	    "KLUDGE"
	    "INFO"
	    "DONE"))
    (setq comment-tags-require-colon nil)
    (setq comment-tags-keyword-faces
          `(("TODO" .   ,(list :weight 'bold :foreground "#28ABE3"))
            ("FIXME" .  ,(list :weight 'bold :foreground "#DB3340"))
            ("BUG" .    ,(list :weight 'bold :foreground "#DB3340"))
            ("HACK" .   ,(list :weight 'bold :foreground "#E8B71A"))
            ("KLUDGE" . ,(list :weight 'bold :foreground "#E8B71A"))
            ("INFO" .   ,(list :weight 'bold :foreground "#F7EAC8"))
            ("DONE" .   ,(list :weight 'bold :foreground "#1FDA9A")))))
  (setq comment-tags-comment-start-only t
        comment-tags-require-colon nil
        comment-tags-case-sensitive t
        comment-tags-show-faces t
        comment-tags-lighter nil)
  :hook
  (prog-mode . comment-tags-mode)
  (conf-mode . comment-tags-mode))


;; EXPERIMENTAL


;; (use-package thingatpt+
;;   :ensure nil
;;   :quelpa (thingatpt+ :fetcher wiki))

;; (progn
;;   (require 'mouse)
;;   (xterm-mouse-mode t)
;;   (defun track-mouse (e))
;;   (setq mouse-sel-mode t))



;; (use-package yascroll
;;   :demand t
;;   :config
;;   (global-yascroll-bar-mode 1))



;; ;; not used for ff because helm owning minibuffer
;; (use-package zlc
;;   :defer 1
;;   :config
;;   (let ((map minibuffer-local-map))
;;   ;;; like menu select
;;     (define-key map (kbd "<down>")  'zlc-select-next-vertical)
;;     (define-key map (kbd "<up>")    'zlc-select-previous-vertical)
;;     (define-key map (kbd "<right>") 'zlc-select-next)
;;     (define-key map (kbd "<left>")  'zlc-select-previous)
;;   ;;; reset selection
;;     (define-key map (kbd "C-c") 'zlc-reset)))


;; TODO: TRY THESE ORG CONFIGS
;; https://git.sr.ht/~nsh/emacs.d/tree/ad7780b33993b59afa901c00d447d00bb3457055/init.el#L342
;; TODO: do full org config
;; indent src blocks according to lang type
(use-package org
  :ensure org-plus-contrib
  :pin org
  :config
  (setq org-src-tab-acts-natively t)
  (require 'org-tempo)
  ;; (setq company-global-modes '(not org-mode))
  )



;; Pin your eyes to the centre of the screen (except when you don't)
(use-package centered-cursor-mode
  :ensure t
  :diminish centered-cursor-mode
  :init
  (setq ccm-recenter-at-end-of-file t)
  :config
  (define-global-minor-mode my-global-centered-cursor-mode centered-cursor-mode
    (lambda ()
      (when (not (memq major-mode (list 'compilation-mode
                                        'dired-mode
                                        'ein:notebook-multilang-mode
                                        'eshell-mode
                                        'eww-mode
                                        'inferior-python-mode
                                        'inferior-sml-mode
                                        'magit-mode
                                        'messages-buffer-mode
                                        'org-mode
                                        'sql-interactive-mode
                                        'w3m-mode)))
        (centered-cursor-mode))))
  (my-global-centered-cursor-mode 1))

;; Sometimes the best fix is turning it off and on again
(use-package restart-emacs
  :ensure t
  :defer t
  :bind (("C-x M-r" . restart-emacs))
  :init
  (setq restart-emacs-restore-frames t))

;; (use-package indent-shift
;;   :ensure nil
;;   :bind (("C-c <" . indent-shift-left)
;;          ("C-c >" . indent-shift-right)))


(use-package free-keys
  :commands free-keys)

(use-package sqlplus
  :ensure nil
  ;; :quelpa (sqlplus :fetcher wiki)
  :quelpa (sqlplus :fetcher github :repo "emacsmirror/sqlplus")
  :defer 1
  :config
  ())


;; ;; lojban https://dustinlacewell.github.io/sutysisku.el/
;; (use-package sutysisku
;;   :defer 1
;;   :quelpa (sutysisku :fetcher github :repo "dustinlacewell/sutysisku.el"))


;; Zoom window like tmux
(use-package zoom-window
  :defer t
  :init
  (global-set-key [S-return] 'zoom-window-zoom))




;; HOW TO USE use-package to load a package from local dir (not already in load-path)
;; (use-package some-feature-package
;;   :load-path some-other-dir)


(setq enable-experimental-packages t)


;;  NOTE: be sure this isn't causing problems before adopting
(use-package aggressive-indent
  :ensure t
  :if enable-experimental-packages
  :config
  (global-aggressive-indent-mode 1)
  (add-to-list 'aggressive-indent-excluded-modes 'html-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'slime-repl-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'haskell-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'lisp-mode)
  (add-to-list 'aggressive-indent-excluded-modes 'emacs-lisp-mode)
  (delete 'lisp-mode aggressive-indent-modes-to-prefer-defun)
  (delete 'emacs-lisp-mode aggressive-indent-modes-to-prefer-defun)
  (add-to-list 'aggressive-indent-dont-indent-if
               '(not (null (string-match (rx (zero-or-more space) (syntax comment-start) (zero-or-more anything)) (thing-at-point 'line))))))


(use-package scroll-restore

  :defer 2
  :if enable-experimental-packages
  :config
  (defun enable-scroll-restore ()
    (interactive)
    (require 'scroll-restore)
    (scroll-restore-mode 1)
    ;; Allow scroll-restore to modify the cursor face
    (setq scroll-restore-handle-cursor t)
    ;; Make the cursor invisible while POINT is off-screen
    (setq scroll-restore-cursor-type nil)
    ;; Jump back to the original cursor position after scrolling
    (setq scroll-restore-jump-back t)
    ;; Toggle scroll-restore-mode with the Scroll Lock key
    ;; (global-set-key (kbd "<Scroll_Lock>") 'scroll-restore-mode)
    (scroll-restore-mode)))


;;; #############################################################
;;; Dash docsets requires gnutls support in emacs
;;; #############################################################
;; (use-package counsel-dash
;;   :ensure t
;;   :defer 3
;;   :init
;;   (setq counsel-dash-docsets-path "~/.emacs.d/docsets"
;;         counsel-dash-docsets-url "https://api.github.com/repos/Kapeli/feeds/contents"
;;         counsel-dash-browser-func 'browse-url)
;;   :config
;;   (counsel-dash-install-docset "Emacs Lisp"))
;;
;; ;; get docsets: https://kapeli.com/docsets#dashdocsetfeed
;; (use-package helm-dash
;;   :config
;;   (progn
;;     (setq helm-dash-browser-func 'eww)
;;     (setq helm-dash-docsets-path (expand-file-name "~/.emacs.d/docsets")
;;     )
;;     ;;(helm-dash-activate-docset "Go")
;;     ;; (helm-dash-activate-docset "Python 3")
;;     ;; (helm-dash-activate-docset "CMake")
;;     ;; (helm-dash-activate-docset "Bash")
;;     ;; (helm-dash-activate-docset "Zsh")
;;     ;; (helm-dash-activate-docset "Django")
;;     ;; (helm-dash-activate-docset "Redis")
;;     ;; (helm-dash-activate-docset "Emacs Lisp")
;;     ))
;;
;; (use-package dash-at-point
;;   :ensure t)
;;
;;; #############################################################
;;; End Dash docsets
;;; #############################################################


(use-package erc
  :init (progn
          (setq erc-nick "jclosure"
                erc-autojoin-channels-alist '(("freenode.net" . ("#emacs"))))))

(use-package transmission
  :config (progn
            (defun transmission-add-magnet (url)
              "Like `transmission-add', but with no file completion."
              (interactive "sMagnet url: ")
              (transmission-add url))))

  ;; (use-package helm-dash
  ;;   :init
  ;;   (global-set-key (kbd "C-c d") 'helm-dash-at-point)
  ;;   (defun c-doc ()
  ;;     (setq helm-dash-docsets '("C")))
  ;;   (defun c++-doc ()
  ;;     (setq helm-dash-docsets '("C" "C++")))
  ;;   (add-hook 'c-mode-hook 'c-doc)
  ;;   (add-hook 'c++-mode-hook 'c++-doc))

;; ++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++
;; Cma / C ++ IDE Setup
;; ++++++++++++++++++++++++++++++++++++++++++++++++ ++++++++++++

(use-package disaster
  :commands (disaster))

(use-package gdb-mi
  :init
  (setq
   ;; use gdb-many-windows by default when `Mx gdb '
   gdb-many-windows t
   ;; Non-nil means display source file containing the main routine at startup
   gdb-show-main t))



;; try this..missing from melpa?
;; (use-package auto-mark
;;   :config (progn (setq auto-mark-command-class-alist
;;                        '((helm . helm)
;;                          (goto-line . jump)
;;                          (indent-for-tab-command . ignore)
;;                          (undo . ignore)))
;;                  (setq auto-mark-command-classifiers
;;                        (list (lambda (command)
;;                                (if (and (eq command 'self-insert-command)
;;                                         (eq last-command-char ? ))
;;                                    'ignore))))
;;                  (global-auto-mark-mode 1)))


;; vc-dir setup section
(progn
"
ref: https://joppot.info/en/2018/02/04/4144
There's this config and a full walkthrough on using vc-git/vc-dir
"

  (require 'vc-dir)
  ;; In vc-git and vc-dir for git buffers,
  ;; make (C-x v)
  ;; - a run git add,
  ;; - u run git reset,
  ;; - r run git reset and checkout from head

  (defun my-vc-git-command (verb fn)
    (let* ((fileset-arg (or vc-fileset (vc-deduce-fileset nil t)))
           (backend (car fileset-arg))
           (files (nth 1 fileset-arg)))
      (if (eq backend 'Git)
          (progn (funcall fn files)
                 (message (concat verb " " (number-to-string (length files))
                                  " file(s).")))
        (message "Not in a vc git buffer."))))

  (defun my-vc-git-add (&optional revision vc-fileset comment)
    (interactive "P")
    (my-vc-git-command "Staged" 'vc-git-register))

  (defun my-vc-git-reset (&optional revision vc-fileset comment)
    (interactive "P")
    (my-vc-git-command "Unstaged"
                       (lambda (files) (vc-git-command nil 0 files "reset" "-q" "–"))))

  (eval-after-load "vc-dir"
    '(progn
       (define-key vc-prefix-map [(r)] 'vc-revert-buffer)
       (define-key vc-dir-mode-map [(r)] 'vc-revert-buffer)
       (define-key vc-prefix-map [(a)] 'my-vc-git-add)
       (define-key vc-dir-mode-map [(a)] 'my-vc-git-add)
       (define-key vc-prefix-map [(u)] 'my-vc-git-reset)
       (define-key vc-dir-mode-map [(u)] 'my-vc-git-reset)

       ;; hide up to date files after refreshing in vc-dir
       (define-key vc-dir-mode-map [(g)]
         (lambda () (interactive) (vc-dir-refresh) (vc-dir-hide-up-to-date)))
       )))




;; TODO: use lsp and clang setup from:
;; https://emacs.nasy.moe/
;; Language Server Protocol & Debug Adapter Protocol
;; lsp-mode

(defun imalison:colorize-compilation-buffer ()
  (let ((was-read-only buffer-read-only))
    (unwind-protect
        (progn
          (when was-read-only
            (read-only-mode -1))
          (ansi-color-apply-on-region (point-min) (point-max)))
      (when was-read-only
        (read-only-mode +1)))))

(add-hook 'compilation-filter-hook 'imalison:colorize-compilation-buffer)


;;; ; http://lists.gnu.org/archive/html/help-gnu-emacs/2007-05/msg00975.html

;; (defvar sticky-buffer-previous-header-line-format)
;; (define-minor-mode sticky-buffer-mode
;;   "Make the current window always display this buffer."
;;   nil " sticky" nil
;;   (if sticky-buffer-mode
;;       (progn
;;         (set (make-local-variable 'sticky-buffer-previous-header-line-format)
;;              header-line-format)
;;         (set-window-dedicated-p (selected-window) sticky-buffer-mode))
;;     (set-window-dedicated-p (selected-window) sticky-buffer-mode)
;;     (setq header-line-format sticky-buffer-previous-header-line-format)))


;; (use-package helm-systemd
;;   :commands helm-systemd)


;; NOTE: show last 2 marks
;; (defface visible-mark-active ;; put this before (use-package visible-mark)
;;   '((((type tty) (class mono)))
;;     (t (:background "magenta"))) "")

;; (use-package visible-mark
;;   :init (global-visible-mark-mode 1)
;;   :config
;;   (progn
;;     (setq visible-mark-max 2)
;;     (setq visible-mark-faces `(visible-mark-face1 visible-mark-face2))))





;; (use-package auto-mark
;;   :defer 1
;;   :ensure nil
;;   :config
;;   (progn
;;     (setq auto-mark-command-class-alist
;;           '((anything . anything)
;;             (goto-line . jump)
;;             (undo . ignore)))
;;     (setq auto-mark-command-classifiers
;;           (list (lambda (command)
;;                   (if (and (eq command 'self-insert-command)
;;                            (eq last-command-char ? ))
;;                       'ignore))))
;;     (global-auto-mark-mode 1)
;;     ))


;; indentation all 2 - this may be problematic
(setq-default
 indent-tabs-mode nil
 tab-width 2
 sh-basic-offset 2
 sh-indentation 2
 c-basic-offset 2)


;; comment the line if no region selected
(defun smf/comment-dwim-line (&optional arg)
  (interactive "*P")
  (comment-normalize-vars)
  (if (and (not (region-active-p)) (not (looking-at "[ \t]*$")))
      (comment-or-uncomment-region (line-beginning-position) (line-end-position))
    (comment-dwim arg)))
(global-set-key (kbd "M-;") #'smf/comment-dwim-line)


;; ;; compilation buffers should be colored
;; ;; NOTE: test this fully
;; (use-package ansi-color
;;   :init
;;   (setq compilation-scroll-output 't)   ; yes plz

;;   (defun smf/colorize-compilation ()
;;     "Colorize from `compilation-filter-start' to `point'."
;;     (let ((inhibit-read-only t))
;;       (ansi-color-apply-on-region
;;        compilation-filter-start (point))))

;;   (add-hook 'compilation-filter-hook 'smf/colorize-compilation))


;; ;; solaire-mode is an aesthetic plugin that helps visually distinguish file-visiting windows
;; ;; from other types of windows (like popups or sidebars) by giving them a slightly different
;; ;; – often brighter – background.
;; (use-package solaire-mode
;;   :ensure t
;;   :demand t
;;   :config
;;   (setq solaire-mode-remap-modeline nil)

;;   ;; brighten buffers (that represent real files)
;;   (add-hook 'after-change-major-mode-hook #'turn-on-solaire-mode)
;;   ;; To enable solaire-mode unconditionally for certain modes:
;;   (add-hook 'ediff-prepare-buffer-hook #'solaire-mode)

;;   ;; ...if you use auto-revert-mode, this prevents solaire-mode from turning
;;   ;; itself off every time Emacs reverts the file
;;   (add-hook 'after-revert-hook #'turn-on-solaire-mode)

;;   ;; highlight the minibuffer when it is activated:
;;   (add-hook 'minibuffer-setup-hook #'solaire-mode-in-minibuffer))


 (with-eval-after-load 'hydra

   ;; https://www.reddit.com/r/emacs/comments/78j51h/worlds_greatest_hydra_needs_your_help/
   (defhydra hydra-nav (:hint nil)
     "
  _b__f__k__j_ ←→↑↓   _h_/_l_ word   _a_/_e_ b/eol   _d_/_r_ del
  _o_ther-frame-or-window   _y_ank        _u_/_U_ndo/redo
  _k_/_w_/_H_/_J_ kill line/region/lw/rw      _j_/_k_ move-line-or-region
  _=_/_-_ expand/contract     _SPC_ mark    _D_/_W_ dup/copy line/region
---------------------------------------------------------------
  _m_ark-ring    _s_wiper     _g_oto-word   _q_u_i_t
      "
     ("f"   forward-char)
     ("F"   forward-sexp)
     ("b"   backward-char)
     ("B"   backward-sexp)
     ("k"   previous-line)
     ("j"   next-line)
     ("a"   xah-beginning-of-line-or-block)
     ("e"   xah-end-of-line-or-block)
     ("A"   beginning-of-defun)
     ("E"   end-of-defun)
     ("h"   left-word)
     ("l"   right-word)
     ("H"   backward-kill-word)
     ("J"   kill-word)
     ("w"   kill-region)
     ("W"   kill-ring-save)
     ("u"   undo-tree-undo)
     ("U"   undo-tree-redo)
     ("d"   delete-char)
     ("o"   other-window-or-frame)
     ("y"   yank)
     ("t"   transpose-chars)
     ;; ("k"   kill-line)
     ("C"   comment-or-uncomment-region-or-line)
     ("P"   elpy-nav-move-line-or-region-up)
     ("N"   elpy-nav-move-line-or-region-down)
     ("D"   duplicate-current-line-or-region)
     ("z"   zap-to-char)
     ("q"   nil)
     ("x"   quit-and-send-C-x :color blue)
     ("c"   quit-and-send-C-c :color blue)
     ("r"   backward-delete-char-untabify)
     ;; ("C-l"   recenter-top-bottom)
     ("v"   scroll-up-command)
     ("V"   scroll-down-command)
     (","   set-mark-and-deactive)
     ("."   exchange-point-and-mark)
     ("="   er/expand-region)
     ("-"   er/contract-region)
     ("SPC" set-mark-command)
     ("g"   avy-goto-word-1 :color blue)
     ("i"   nil :color blue)
     ("s"   swiper :color blue) ;; blue is sadly needed!
     ("S"   save-buffer)
     ("K"   volatile-kill-buffer)
     ("m"   counsel-mark-ring :color blue)
     ))


;; ;; easy-kill (similar to expand-region)
;; ;; https://emacsredux.com/blog/2018/11/09/an-easy-kill/
;; (use-package easy-kill
;;   :ensure t
;;   :defer 1
;;   :config
;;   (global-set-key [remap kill-ring-save] #'easy-kill)
;;   (global-set-key [remap mark-sexp] #'easy-mark))


;; ;; http://blog.lujun9972.win/emacs-document/blog/2019/03/14/evil-guide/
;; (use-package evil
;;   :defer 1
;;   :init
;;   (setq evil-want-integration t) ;; This is optional since it's already set to t by default.
;;   (setq evil-want-keybinding nil)
;;   :config
;;   (progn
;;    (define-key evil-emacs-state-map [escape] 'evil-normal-state)
;;    (evil-mode 1)))

;; (use-package evil-collection
;;   :after evil
;;   :ensure t
;;   :config
;;   (evil-collection-init))

;; (use-package general
;;   :ensure t
;;   :init
;;   (setq general-override-states '(insert
;;                                   emacs
;;                                   hybrid
;;                                   normal
;;                                   visual
;;                                   motion
;;                                   operator
;;                                   replace))
;;   :config
;;   (general-define-key
;;    :states '(normal visual motion)
;;    :keymaps 'override
;;    "SPC" 'hydra-space/body))
;;    ;; Replace 'hydra-space/body with your leader function.


;; TODO: its interactive, have it prompt for key sequence
(defun my-lookup-key (key)
  "Search for KEY in all known keymaps."
  (interactive)
  (mapatoms (lambda (ob) (when (and (boundp ob) (keymapp (symbol-value ob)))
                      (when (functionp (lookup-key (symbol-value ob) key))
                        (message "%S" ob))))
            obarray))


;; https://matthewbauer.us/bauer/
(use-package diff-hl
  :disabled
  :bind (:package diff-hl
         :map diff-hl-mode-map
         ("<left-fringe> <mouse-1>" . diff-hl-diff-goto-hunk))
  :hook ((prog-mode . diff-hl-mode)
         (vc-dir-mode . diff-hl-mode)
         (dired-mode . diff-hl-dir-mode)
         (magit-post-refresh . diff-hl-magit-post-refresh)
         (org-mode . diff-hl-mode)))

;; ;; https://github.com/DamienCassou/auth-password-store
;; ;; research
;; (use-package auth-source-pass
;;   :ensure nil
;;   :after auth-source
;;   :init
;;   (progn
;;     (setq auth-sources '(password-store))))


;; ;; bash-completion
;; ;; sudo apt-get install bash-completion
;; (use-package bash-completion
;;   :hook (shell-dynamic-complete-functions . bash-completion-dynamic-complete))


;; ;; TODO: get this working
;; ;; brew install transmission
;; ;; sudo apt-get install transmission
;; (use-package transmission
;;   :config (progn
;;             (defun transmission-add-magnet (url)
;;               "Like `transmission-add', but with no file completion."
;;               (interactive "sMagnet url: ")
;;               (transmission-add url))))

;; abbrevs and dabbrevs


(use-package abbrev
  :ensure nil
  :config
  (setq save-abbrevs 'silently)
  (setq-default abbrev-mode t))


;; (use-package company-dabbrev
;;   :config (progn
;;             (setq dabbrev-check-all-buffers t)
;;             (setq company-dabbrev-ignore-case t)
;;             (setq company-dabbrev-downcase nil)))







;;   ;; TODO: this stops on non-word syntax
;;   (defun dwim-backward-kill-word ()
;;     "DWIM kill characters backward until encountering the beginning of a
;; word or non-word."
;;     (interactive)
;;     (if (thing-at-point 'word) (backward-kill-word 1)
;;       (let* ((orig-point              (point))
;;              (orig-line               (line-number-at-pos))
;;              (backward-word-point     (progn (backward-word) (point)))
;;              (backward-non-word-point (progn (goto-char orig-point) (backward-non-word) (point)))
;;              (min-point               (max backward-word-point backward-non-word-point)))

;;         (if (< (line-number-at-pos min-point) orig-line) (progn (goto-char min-point) (end-of-line) (delete-horizontal-space))
;;           (delete-region min-point orig-point)
;;           (goto-char min-point))
;;         )))

;;   (defun backward-non-word ()
;;     "Move backward until encountering the beginning of a non-word."
;;     (interactive)
;;     (search-backward-regexp "[^a-zA-Z0-9\s\n]")
;;     (while (looking-at "[^a-zA-Z0-9\s\n]")
;;       (backward-char))
;;     (forward-char))

;;   (define-key global-map [remap backward-kill-word] 'dwim-backward-kill-word)



;; XAH versions of these functions

;;   (defun my-delete-word (arg)
;;     "Delete characters forward until encountering the end of a word.
;; With argument, do this that many times.
;; This command does not push text to `kill-ring'."
;;     (interactive "p")
;;     (delete-region
;;      (point)
;;      (progn
;;        (forward-word arg)
;;        (point))))

;;   (defun my-backward-delete-word (arg)
;;     "Delete characters backward until encountering the beginning of a word.
;; With argument, do this that many times.
;; This command does not push text to `kill-ring'."
;;     (interactive "p")
;;     (my-delete-word (- arg)))

;;   ;; bind them to emacs's default shortcut keys:
;;   (global-set-key (kbd "M-d") 'my-delete-word)
;;   (global-set-key (kbd "<M-backspace>") 'my-backward-delete-word)







;; JUMP XREF FIND REFERENCES SPEC AND ADVICE

;; NOTE: currently not used because we have helm-xref using helm for all xref-find-* fns
(progn
  ;; set from: http://bryan-murdock.blogspot.com/2018/03/fixing-xref-find-references.html
  ;; to lookup references of words with word boundary in projectile projects.
  ;; this should be replaced if it causes problems with lsp or other xref-find-references
  ;; packages!!!
  ;; M-? is bound by default to xref-find-references

  ;; ootb ag for projects
  ;;(define-key global-map [remap xref-find-references] 'ag-project)

  ;; best helm-ag for projects
  ;; (define-key global-map [remap xref-find-references] 'helm-ag-project-root)

  ;; second best helm-ag for projects
  ;;(define-key global-map [remap xref-find-references] 'helm-projectile-ag)

  ;; preferring helm-projectile-rg
  ;; rip grep honors .gitignores and is faster than ag.
  (define-key global-map [remap xref-find-references] 'helm-projectile-rg)

  ;; ;; EAGER mark insertion - jump back from xref-finds with M-,
  (defun my-xref-find-add-mark-before-advice (&rest args)
    "Advice for `xref-find-references' or `xref-find-definitions' that adds a mark first."

    ;; (cond ((eq major-mode 'python-mode)
    ;;        ;; use python-mode specific marker ring fn
    ;;        (my-python-push-marker-ring))
    ;;       ;; otherwise use default global marker ring
    ;;       ;; TODO: look into using a specific one for elixir
    ;;       (t (xref-push-marker-stack (point-marker))))

    ;; deprecated shows using a generic approach to ring insertion
    ;; (ring-insert find-tag-marker-ring (point-marker))
    (xref-push-marker-stack (point-marker))
    )


        ;; ;; NOTE: apply this advice to the remapped xref-find-references fn above
  (advice-add 'helm-projectile-rg :before #'my-xref-find-add-mark-before-advice)

  ;; TODO: investigate LAZY mark insertion: https://github.com/emacs-helm/helm/issues/1927
  )





;; FACE CUSTOMIZATION

;; style the window divider

;; solid gray looks good but fat (theme overrides)
(set-face-background 'vertical-border "gray20")
(set-face-foreground 'vertical-border (face-background 'vertical-border))


;; set base highlight style, so that hightlight and all inherited faces look good
(set-face-foreground 'highlight nil) ;; remove forground so syntax highlighting still works
(set-face-background 'highlight "#1c1c1c")

;; ;; region selected color (black)
;; (set-face-attribute 'region nil :background "color-236")

;; comment keyword highlighting
;; TODO: possibly replace with comment-tags-mode in the future. currently doesn't support regex.
(defface my-comment-keywords-face
  '((t :foreground "magenta"
       :weight bold))
  "Face for specific keywords that appear in comments."
  :group 'my-faces)

(defun my-highlight-comment-keywords ()
  "minimal face change for comment keywords."
  (interactive)
  (font-lock-add-keywords nil '(("\\b\\(FIXME[\\(\\)a-zA-Z0-9]*:\\|TODO[\\(\\)a-zA-Z0-9]*:\\|BUG[\\(\\)a-zA-Z0-9]*:\\|NOTE[\\(\\)a-zA-Z0-9]*:\\)" 1 'my-comment-keywords-face t))))


;; add hooks where comment keywords should be highlighted
(mapc (lambda (hook) (add-hook hook 'my-highlight-comment-keywords))
      '(
	text-mode-hook
	latex-mode-hook
	html-mode-hook
	emacs-lisp-mode-hook
	prog-mode-hook
	python-mode-hook
	))



;; set the face for volatile highlights
;; timing issues with gui mode setting face, so using an idle timer to set it
(progn
  ;; variable for the timer object
  (defvar idle-timer-vhl-timer nil)

  ;; callback function
  (defun idle-timer-vhl-callback ()
    (progn
      ;; we can always all make face before setting in incase it's not there yet
      (make-face 'vhl/default-face)
      (set-face-attribute 'vhl/default-face nil
                          :background "blue4")))

  ;; start functions
  (defun idle-timer-vhl-run-once ()
    (interactive)
    (when (timerp idle-timer-vhl-timer)
      (cancel-timer idle-timer-vhl-timer))
    (setq idle-timer-vhl-timer
          (run-with-idle-timer 1 nil #'idle-timer-vhl-callback)))

  (idle-timer-vhl-run-once))



;; hadcoded colors for terminal emacs.
;; TODO: hoist these up into the use-packages to which they apply
(when *is-in-terminal*
  (progn

    (use-package terminal-focus-reporting
      :hook (after-init . terminal-focus-reporting-mode))

    ;; to see all current faces colors, run
    ;; list-faces-display


    ;; ;; set inactive windows' background(s) to a slightly lighter color to visually distingush
    ;; (defun highlight-selected-window ()
    ;;   "Highlight selected window with a different background color."
    ;;   (walk-windows (lambda (w)
    ;;                   (unless (eq w (selected-window))
    ;;                     (with-current-buffer (window-buffer w)
    ;;                       (buffer-face-set '(:background "#111"))))))
    ;;   (buffer-face-set 'default))
    ;; (add-hook 'buffer-list-update-hook 'highlight-selected-window)



    ;; TODO: remove from custom-set-faces
    ;; tweak these to be more inline with doom theme
    ;; set the default comment color to light grey
    (custom-set-faces
     ;; custom-set-faces was added by Custom.
     ;; If you edit it by hand, you could mess it up, so be careful.
     ;; Your init file should contain only one such instance.
     ;; If there is more than one, they won't work right.
     '(font-lock-variable-name-face ((t (:foreground "violet"))))
     '(magit-diff-added ((((type tty)) (:foreground "green"))))
     '(magit-diff-added-highlight ((((type tty)) (:foreground "brightgreen"))))
     '(magit-diff-context-highlight ((((type tty)) (:foreground "default"))))
     '(magit-diff-file-heading ((((type tty)) nil)))
     '(magit-diff-hunk-heading ((((type tty)) (:foreground "magenta"))))
     '(magit-diff-hunk-heading-highlight ((((type tty)) (:foreground "brightyellow"))))
     '(magit-diff-removed ((((type tty)) (:foreground "red"))))
     '(magit-diff-removed-highlight ((((type tty)) (:foreground "brightred"))))
     '(magit-section-highlight ((((type tty)) nil))))


    ;; TODO: do we still want this?
    ;; set highlight face for dark screens
    (set-face-background 'highlight "darkblue")
    (set-face-background 'secondary-selection "darkblue")

    ;;; ivy
    (with-eval-after-load 'ivy
      (progn
        ;; the background color of selection line in ivy
        (set-face-attribute 'ivy-current-match nil
                            :background "blue")))

    ;;; helm

    (with-eval-after-load 'helm
      (progn

        (set-face-attribute 'helm-source-header nil
                            :background "color-17" :foreground "black" :weight 'bold :height 1.3 :family "Sans Serif" )

        ;; the background color of selection line in helm
        (set-face-attribute 'helm-selection nil
                            :background "blue")
        ))


    ;; ;;; show-paren-mode
    ;; ;; NOTE: letting theme own this for now
    ;; (if (face-exists-p 'show-paren-match)
    ;;     (set-face-attribute 'show-paren-match t
    ;;                         :background "light sky blue"))


    ;; ;;; neotree
    ;; ;; NOTE: the 'neo-vc-default-face is not visible on black backgrounds
    ;; ;; setting it explicitly here or must set:
    ;; ;; (setq frame-background-mode 'light) before neo loads
    ;; ;; otherwise, you can override like this
    ;; (with-eval-after-load 'neotree
    ;;   (progn
    ;;     (setq default-neo-tree-file-color "color-244")
    ;;     (set-face-attribute 'neo-vc-default-face t
    ;;                         :foreground default-neo-tree-file-color)
    ;;     (set-face-attribute 'neo-file-link-face t
    ;;                         :foreground default-neo-tree-file-color)))
    ))
(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ediff-split-window-function (quote split-window-horizontally) t)
 '(ediff-window-setup-function (quote ediff-setup-windows-plain) t)
 '(flymake-google-cpplint-command "/usr/local/bin/cpplint")
 '(flymake-google-cpplint-filter "--filter=-whitespace/line_length,-build")
 '(flymake-google-cpplint-verbose "--verbose=0")
 '(mouse-wheel-progressive-speed nil)
 '(mouse-wheel-scroll-amount (quote (1 ((control) . 5))))
 '(neo-auto-indent-point t)
 '(package-selected-packages
   (quote
    (use-package-el-get el-get-use-package google-c-style terminal-focus-reporting auto-dim-other-buffers terraform-mode nginx-mode dockerfile-mode company-lsp company-flx clang-format quickrun realgud groovy-mode markdown-mode yaml-mode json-mode nvm nodejs-repl js2-mode flymake-elixir elixir-yasnippets alchemist elixir-mode protobuf-mode jinja2-mode py-autopep8 flymake-google-cpplint flymake-cursor cmake-mode lua-mode auto-compile macrostep elisp-demos flycheck-pos-tip helpful origami dumb-jump helm-ag helm-fuzzier helm-flx wgrep deadgrep ag git-timemachine gist git-link gitattributes-mode gitignore-mode gitconfig-mode helm-projectile counsel ivy-xref ivy vlf flx-ido smex volatile-highlights ws-butler mwim iedit multiple-cursors yasnippet-snippets flycheck treemacs-magit treemacs-icons-dired treemacs-projectile treemacs which-key visual-regexp-steroids use-package undo-tree tabbar smooth-scrolling restclient ranger pfuture persp-projectile no-littering neotree multi-term key-chord hydra ht highlight-symbol fill-column-indicator exec-path-from-shell doom-themes doom-modeline diminish crux bar-cursor backup-walker anzu ace-window)))
 '(tabbar-separator (quote (0.5)))
 '(undo-tree-visualizer-diff t)
 '(undo-tree-visualizer-timestamps t))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(auto-dim-other-buffers-face ((t (:background "#111"))))
 '(font-lock-variable-name-face ((t (:foreground "violet"))))
 '(magit-diff-added ((((type tty)) (:foreground "green"))))
 '(magit-diff-added-highlight ((((type tty)) (:foreground "brightgreen"))))
 '(magit-diff-context-highlight ((((type tty)) (:foreground "default"))))
 '(magit-diff-file-heading ((((type tty)) nil)))
 '(magit-diff-hunk-heading ((((type tty)) (:foreground "magenta"))))
 '(magit-diff-hunk-heading-highlight ((((type tty)) (:foreground "brightyellow"))))
 '(magit-diff-removed ((((type tty)) (:foreground "red"))))
 '(magit-diff-removed-highlight ((((type tty)) (:foreground "brightred"))))
 '(magit-section-highlight ((((type tty)) nil))))



(message "Initialized dotfiles")

(provide 'init-dotfiles)

;;; init-dotfiles.el ends here
