#! /bin/bash

# if [ ! `command -v "zsh"` ] && { echo "Please install zsh before proceeding" ; exit 1; }

if [ -z "$IN_ZSH" ]; then
  export IN_ZSH=1
  exec zsh -f "$0" "$@"
fi

echo installing

# install the dotfiles
mydir=${0:a:h}

# hard link the .emacs bootstrap file
unlink $HOME/.emacs.d
ln -s $mydir/.emacs.d $HOME/

# my .tmux.conf.local fixes colors in terminal and sets some better styles
# NOTE: WARNING: this may become deprecated as new oh-my-tmux rels continue
# tween and update in the future
unlink $HOME/.tmux.conf.local
ln -s $mydir/tmux/.tmux.conf.local ~/.tmux.conf.local
